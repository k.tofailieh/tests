
WITH existingRAW_MATERIAL AS (
SELECT m_id,m_ref, cust_id,marguage as code FROM fiche WHERE tp ='RAW_MATERIAL'
AND split_part(ns,'/',1) = 'Havea'
AND active <> 0)
UPDATE import.import_line
SET internal_id = m_id
from existingRAW_MATERIAL
WHERE imp_ref = 'ImportRawMaterial_6a56d67d-5c91-4104-90dc-509ab9c1fe72_OtherInformation'||'_CERTIFICATION'
and (import_line.cust_id = existingRAW_MATERIAL.cust_id or
       (coalesce(import_line.value_txt10, '') != '' and import_line.value_txt10 = existingRAW_MATERIAL.code and import_line.value13 = existingRAW_MATERIAL.m_ref) or
       (coalesce(import_line.value_txt10, '') = '' and coalesce(existingRAW_MATERIAL.code, '') = '' and import_line.value13 = existingRAW_MATERIAL.m_ref))
AND NOT(COALESCE(imp_err, FALSE));





/*crete new ids for new certifications*/
with
	distinct_new_Certifications_to_be_inserted as (select distinct value30 as certification from import.import_line where value_id4 is null and imp_ref='ImportRawMaterial_6a56d67d-5c91-4104-90dc-509ab9c1fe72_OtherInformation'||'_CERTIFICATION' AND NOT(COALESCE(imp_err, FALSE)))
,	new_Certifications_with_new_ids as (select -nextval('seq_lov_id') as newId ,certification from distinct_new_Certifications_to_be_inserted)
update import.import_line
set value_id4 = newId
from new_Certifications_with_new_ids
where imp_ref='ImportRawMaterial_6a56d67d-5c91-4104-90dc-509ab9c1fe72_OtherInformation'||'_CERTIFICATION'
and COALESCE(value30,'')=certification
;

insert into lov(lov_id,
                tp,
                ke,
                ns)
select
    abs(value_id4) ,
    'RAW_MATERIAL_CERTIFICATION' as tp,
    value30 as ke,
    'Havea'

from import.import_line
where imp_ref='ImportRawMaterial_6a56d67d-5c91-4104-90dc-509ab9c1fe72_OtherInformation'||'_CERTIFICATION'						/* Current upload */
and coalesce(imp_err,false) = false             /* No errors */
and COALESCE(value_id4,0)<0                    /*new certification*/
group by value_id4,value30
;



/* update current Certification*/
update entr_fiche_lov
set entr_dst_id=abs(value_id4)
from import.import_line
where exists(select 1 from entr_fiche_lov efl
    where efl.entr_src_id=abs(import_line.internal_id)
    and efl.entr_tp='RAW_MATERIAL_CERTIFICATION'
    )

and imp_ref='ImportRawMaterial_6a56d67d-5c91-4104-90dc-509ab9c1fe72_OtherInformation'||'_CERTIFICATION'						/* Current upload */
and coalesce(imp_err,false) = false
and entr_tp='RAW_MATERIAL_CERTIFICATION'
and entr_src_id=abs(import_line.internal_id)
and coalesce(value_id4,0)<0
;



 /*link between RM and Certification*/


insert into entr_fiche_lov(
    entr_id,
	entr_src_id,
	entr_dst_id,
	entr_tp
)(select	nextval('seq_entr_fiche_lov_id') as entr_id,
	abs(internal_id) as entr_src_id,
	abs(value_id4) as entr_dst_id,
	'RAW_MATERIAL_CERTIFICATION' as entr_tp
from import.import_line
where not exists(select 1 from entr_fiche_lov efl
    where efl.entr_src_id=abs(import_line.internal_id)
    and efl.entr_tp='RAW_MATERIAL_CERTIFICATION'
    )
and imp_ref=:imp_ref					/* Current upload */
and coalesce(imp_err,false) = false
and coalesce(value_id4,0)<0
group by internal_id,value_id4  )
;
