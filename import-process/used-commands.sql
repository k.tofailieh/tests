SELECT *
FROM import.import_line
ORDER BY imp_id DESC;

UPDATE import.import_line
SET value_id1   = NULL,
    imp_err_msg = NULL,
    imp_err     = NULL
WHERE imp_ref = :imp_ref;

DELETE
FROM import.import_matching
WHERE TRUE



SELECT il.value10
FROM import.import_matching
         INNER JOIN import.import_line il USING (imp_id)
WHERE imp_ref = :imp_ref;


SELECT *
FROM anx.inci
--          inner join anx.entr_substances_inci ON inci.inci_id = entr_substances_inci.entr_dst_id
--          inner join substances s ON entr_substances_inci.entr_src_id = s.sub_id
WHERE upper(inciname) = 'LIMONENE';


SELECT *
FROM anx.inci
--          inner join anx.entr_substances_inci ON inci.inci_id = entr_substances_inci.entr_dst_id
--          inner join substances s ON entr_substances_inci.entr_src_id = s.sub_id
WHERE upper(cas) = '78-70-6'


SELECT value10, value35
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND NOT exists(SELECT 1
                 FROM import.import_matching
                 WHERE import_matching.imp_id = import_line.imp_id
                   AND entr_tp LIKE 'INCI_CONCERNED%');

SELECT DISTINCT *
FROM de
WHERE de_ke = 'DERMAL_ABSORBTION';



INSERT INTO de(de_id, de_ke, de_tp, de_li, de_st)
VALUES (nextval('seq_de_id'), 'DERMAL_ABSORBTION', 'ENDPT_CAT', 'Dermal absorbtion', 'VALID')



SELECT *
FROM (SELECT study.study_id, study.purpose_flag, efp.entr_src_id, efp.entr_tp, study.conclusion
      FROM entr_fiche_profile efp
               INNER JOIN profile ON efp.entr_dst_id = profile.profile_id AND profile.tptp = 'Registration'
               INNER JOIN entr_profile_study eps
                          ON profile.profile_id = eps.entr_src_id AND eps.entr_tp = 'REGULATORY_PROFILE_DATA'
               INNER JOIN (SELECT max(study_id) OVER (PARTITION BY purpose_flag) AS id FROM study) latesStudy
               INNER JOIN study ON study_id = latesStudy.id
                          ON eps.entr_dst_id = study.study_id AND study.category = 'Registration'
      WHERE study.purpose_flag = 'CN') studies -- on raw_material.m_id = studies.entr_src_id and studies.entr_tp='REGULATORY_PROFILE'



SELECT study_id, study_dt, max(study_dt) OVER (PARTITION BY purpose_flag) AS id FROM study