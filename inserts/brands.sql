insert into import.import_line(imp_ref, value1, value2)
values ('brand_test_1','new product', E''),
       ('brand_test_1','new product', E'L\'oréal'),
       ('brand_test_1','new product', E'L\'oréal'),
       ('brand_test_1','new product2', E'L\'oréal'),
       ('brand_test_1','new product',  E'Khaled'),
       ('brand_test_1','new product',  E'Khaled'),
       ('brand_test_1','new product 1',  E'Khaled');

/* Check Brands Count For Product. */
with brands_count as (select trim(value1) as value1, coalesce(cust_id,'-') as product_code, count(distinct value2) as nb_brands from import.import_line where imp_ref= :imp_ref group by trim(value1), coalesce(cust_id,'-'))
     ,products_with_multiple_brands as (select imp_id, il.value1, value2 from brands_count inner join import.import_line il on trim(il.value1) = trim(brands_count.value1) and product_code = coalesce(il.cust_id, '-')  where nb_brands > 1 and il.imp_ref = :imp_ref)
update import.import_line
    set imp_warn = true, imp_warn_msg = coalesce(import_line.imp_warn_msg, '') || ' | This Product Has Many Brands And We Will Take The First One.'
from products_with_multiple_brands
where products_with_multiple_brands.imp_id = import_line.imp_id
and import_line.imp_ref = :imp_ref
;

/* Get the brand_id from DB or generate a new one. */
with brands_to_be_inserted as (select distinct value2 as brand_name from import.import_line where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value2, '') != ''),
     brand_ids as (select brand_name, coalesce(lov_id, -nextval('seq_lov_id')) as brand_id from brands_to_be_inserted left join lov on upper(trim(brand_name)) = upper(trim(ke)))
update import.import_line
    set value_id7 = brand_id
    from brand_ids
where value2 = brand_name
    and not coalesce(imp_err, false)
    and imp_ref = :imp_ref
;

/* Flag master-brand record to avoid creating multiple lov records for one brand. */
with master_brand_record as (select imp_id, value_id7 as brand_id, row_number() over (partition by value_id7) as rn from import.import_line where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value_id7, 0) != 0)
update import.import_line set value35 = 'Master Brand'
    from master_brand_record
where master_brand_record.imp_id = import_line.imp_id
    and rn = 1                      /* First one in the partition. */
    and not coalesce(imp_err, false)/* No error. */
    and imp_ref = :imp_ref
;

/* Insert new brands */
insert into lov(lov_id, ke, ns, co)
select abs(value_id7), trim(value2), :user_ns, 'Automatically Generated From Import-Cosmetic-Product.'
    from import.import_line
where coalesce(value_id7, 0) < 0
    and value35 = 'Master Brand'     /* The Master record. */
    and not coalesce(imp_err, false)
    and imp_ref = :imp_ref
;

/* Flag to insert the relation entr-fiche-lov */
with master_brand_for_product as (select imp_id, value_id7 as brand_id, row_number() over (partition by value1) as product_rn
                                  from import.import_line
                                  where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value_id7, 0) !=0)
update import.import_line il set value36 = 'Master Product Brand'
from master_brand_for_product
where il.imp_id = master_brand_for_product.imp_id
and product_rn = 1
;


/* Create the link product-brand */
insert into entr_fiche_lov(entr_id, entr_src_id, entr_dst_id, entr_tp)
select nextval('seq_entr_fiche_lov_id'), abs(internal_id), abs(value_id7), 'COSMETIC_PRODUCT_BRAND'
    from import.import_line
where  value36 = 'Master Product Brand'     /* The Master record. */
    and not coalesce(imp_err, false)
    and internal_id is not null
    and value_id7 is not null
    and imp_ref = :imp_ref
on conflict (entr_src_id, entr_tp, entr_dst_id) do nothing
;





select imp_id, internal_id, value_id7 from import.import_line where  value35 = 'Master Brand'

update import.import_line
set imp_err = null
where imp_ref = :imp_ref;




with brands_count as (select trim(value1), coalesce(cust_id,'-') as product_code, value2, count(value2) over (partition by value1, cust_id) as nb_brands from import.import_line where imp_ref= :imp_ref)
select * from brands_count


with master_brand_for_product as (select imp_id, value_id7 as brand_id, row_number() over (partition by value1, value_id7) as brand_rn, row_number() over (partition by value1 order by value2 nulls last ) as product_rn from import.import_line where imp_ref = :imp_ref and not coalesce(imp_err, false))
select * from master_brand_for_product;
--where product_rn =1


insert into import.import_line(imp_ref, value2)
values ('brand_test', E'new Brand')

update import.import_line set internal_id = 598173  where imp_ref = :imp_ref and value1 = 'new product 1';

select value1, value2, imp_warn, imp_warn_msg, value_id7, value35, value36, internal_id  from import.import_line
where imp_ref = :imp_ref;

select distinct lov.tp from fiche prod inner join entr_fiche_lov on prod.m_id = entr_fiche_lov.entr_src_id
         inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id
             where prod.tp = 'PRODUCT'


select * from import.import_line order by imp_id desc;










-- /* Get the brand_id from DB or generate a new one. */
-- with brands_to_be_inserted as (select distinct value2 as brand_name from import.import_line where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value2, '') != ''),
--      brand_ids as (select brand_name, coalesce(lov_id, -nextval('seq_lov_id')) as brand_id from brands_to_be_inserted left join lov on upper(trim(brand_name)) = upper(trim(ke)))
-- update import.import_line
--     set value_id7 = brand_id
--     from brand_ids
-- where value2 = brand_name
--     and not coalesce(imp_err, false)
--     and imp_ref = :imp_ref
-- ;
--
-- /* Flag master-brand record to avoid creating multiple lov records for one brand. */
-- with master_brand_record as (select imp_id, value_id7 as brand_id, row_number() over (partition by value_id7) as rn from import.import_line where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value_id7, 0) != 0)
-- update import.import_line set value35 = 'Master Brand'
--     from master_brand_record
-- where master_brand_record.imp_id = import_line.imp_id
--     and rn = 1                      /* First one in the partition. */
--     and not coalesce(imp_err, false)/* No error. */
--     and imp_ref = :imp_ref
-- ;
--
-- /* Insert new brands */
-- insert into lov(lov_id, ke, ns, co)
-- select abs(value_id7), trim(value2), :user_ns, 'Automatically Generated From Import-Cosmetic-Product.'
--     from import.import_line
-- where coalesce(value_id7, 0) < 0
--     and value35 = 'Master Brand'     /* The Master record. */
--     and not coalesce(imp_err, false)
--     and imp_ref = :imp_ref
-- ;
--
-- /* Flag to insert the relation entr-fiche-lov */
-- with master_brand_for_product as (select imp_id, value_id7 as brand_id, row_number() over (partition by value1) as product_rn
--                                   from import.import_line
--                                   where imp_ref = :imp_ref and not coalesce(imp_err, false) and coalesce(value_id7, 0) !=0)
-- update import.import_line il set value36 = 'Master Product Brand'
-- from master_brand_for_product
-- where il.imp_id = master_brand_for_product.imp_id
-- and product_rn = 1
-- ;
--
--
-- /* Create the link product-brand */
-- insert into entr_fiche_lov(entr_id, entr_src_id, entr_dst_id, entr_tp)
-- select nextval('seq_entr_fiche_lov_id'), abs(internal_id), abs(value_id7), 'COSMETIC_PRODUCT_BRAND'
--     from import.import_line
-- where  value36 = 'Master Product Brand'     /* The Master record. */
--     and not coalesce(imp_err, false)
--     and internal_id is not null
--     and value_id7 is not null
--     and imp_ref = :imp_ref
-- on conflict (entr_src_id, entr_tp, entr_dst_id) do nothing
-- ;


