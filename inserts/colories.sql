select * from import.import_line where imp_ref = 'inci-colories-import-1';

with get_siplitted_translations as (
    select *, coalesce((nullif(split_part(value3, '/', 1), '') ), '') || '/' || trim(regexp_split_to_table(split_part(value3, '/', 2),  '_')) as new_translation from import.import_line where imp_ref = 'inci-colories-import-1'
), get_second_siplitted_translations as (
    select *, trim(regexp_split_to_table(coalesce(nullif(new_translation, '/'), left(new_translation,length(new_translation)-1)), '_'))  as new_translation_1 from get_siplitted_translations
)
,
get_partitioned_translations as (
    select imp_id, value1, value2,value_id2,  value3, value_id1, value25,
           case when right(new_translation_1, 1) = '/' then left(new_translation_1, length(new_translation_1)-1) else new_translation_1 end ,  row_number() over (partition by value_id1,value25 ) from get_second_siplitted_translations
)
select * from get_partitioned_translations
