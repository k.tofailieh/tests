/* ADD New Product Documents On Kyoto */
with documents (de_tp, de_nt, de_ke, de_li, de_st, de_num1, de_char1)
         as (values ('NOAEL_TTC_VALUE', 'Noael TTC value', 'TTC_CLASS_I', 'TTC class I', 'VALID', 23, 'μg/kg/d'),
                    ('NOAEL_TTC_VALUE', 'Noael TTC value', 'TTC_CLASS_III', 'TTC_CLASS_III', 'VALID', 1.15, 'μg/kg/d'),
                    ('NOAEL_TTC_VALUE', 'Noael TTC value', 'TTC_CLASS_BOTANICAL', 'TTC BOTANICAL', 'VALID',  5.5525, 'μg/kg/d')
)
insert
into de(de_id, de_tp, de_nt, de_ke,de_li, de_st, de_num1, de_char1, de_co)
select coalesce(de_id, nextval('seq_de_id')) as de_id,
       documents.de_tp,
       documents.de_nt,
       documents.de_ke,
       documents.de_li,
       documents.de_st,
       documents.de_num1,
       documents.de_char1,
       'TTC Insert test.'
from documents
     left join de on trim(upper(documents.de_ke)) = trim(upper(de.de_ke))
        and trim(upper(documents.de_nt)) = trim(upper(de.de_nt))
        and trim(upper(documents.de_tp)) = trim(upper(de.de_tp))
-- if they are already exists set de_st = 'VALID' if it is null.
on conflict (de_id) do update set de_st = 'VALID'
where de.de_st is null
returning de_id;

