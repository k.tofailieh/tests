
/* update for NATURAL_ORIGIN_CONTENT */
with rm_regulatory_restrictions as
(select distinct internal_id as rm_id,
    coalesce(endpt_id, -nextval('seq_endpt_id')) as endpt_id,
    de_id
from import.import_line
inner join fiche f on f.m_id =  abs(internal_id)
left join entr_fiche_endpt efe on efe.entr_src_id = f.m_id and entr_tp = 'ISO_16128_CONTENT'
left join endpt e on e.endpt_id = efe.entr_dst_id
left join der_endpt on der_endpt.der_src_id = e.endpt_id and der_tp = 'ENDPT_DESC'
left join de on de_tp = 'ENDPT' and de.de_ke = 'ISO_16128_NATURAL_ORIGIN_CONTENT'
where imp_ref = :imp_ref and internal_id is not null
)

--- update the import_line table with the rawmaterial disposal conditions data ---
update import.import_line
set
 value_id1 = endpt_id, /* endpt_id in value_id1 */
  value_id2 = de_id /* de_id in value_id2 */
from rm_regulatory_restrictions
where imp_ref =  :imp_ref and abs(internal_id) = abs(rm_id);

/* update for NATURAL_CONTENT */
with rm_regulatory_restrictions as
(select distinct internal_id as rm_id,
    coalesce(endpt_id, -nextval('seq_endpt_id')) as endpt_id,
    de_id
from import.import_line
inner join fiche f on f.m_id =  abs(internal_id)
left join entr_fiche_endpt efe on efe.entr_src_id = f.m_id and entr_tp = 'ISO_16128_CONTENT'
left join endpt e on e.endpt_id = efe.entr_dst_id
left join der_endpt on der_endpt.der_src_id = e.endpt_id and der_tp = 'ENDPT_DESC'
left join de on de_tp = 'ENDPT' and de.de_ke = 'ISO_16128_NATURAL_CONTENT'
where imp_ref = :imp_ref and internal_id is not null
)

--- update the import_line table with the rawmaterial disposal conditions data ---
update import.import_line
set
 value_id3 = endpt_id, /* endpt_id in value_id3 */
  value_id4 = de_id /* de_id in value_id4 */
from rm_regulatory_restrictions
where imp_ref =  :imp_ref and abs(internal_id) = abs(rm_id);



/* update for ORGANIC_ORIGIN_CONTENT */
with rm_regulatory_restrictions as
(select distinct internal_id as rm_id,
    coalesce(endpt_id, -nextval('seq_endpt_id')) as endpt_id,
    de_id
from import.import_line
inner join fiche f on f.m_id =  abs(internal_id)
left join entr_fiche_endpt efe on efe.entr_src_id = f.m_id and entr_tp = 'ISO_16128_CONTENT'
left join endpt e on e.endpt_id = efe.entr_dst_id
left join der_endpt on der_endpt.der_src_id = e.endpt_id and der_tp = 'ENDPT_DESC'
left join de on de_tp = 'ENDPT' and de.de_ke = 'ISO_16128_ORGANIC_ORIGIN_CONTENT'
where imp_ref = :imp_ref and internal_id is not null
)

--- update the import_line table with the rawmaterial disposal conditions data ---
update import.import_line
set
 value_id5 = endpt_id, /* endpt_id in value_id5 */
 value_id6 = de_id /* de_id in value_id6 */
from rm_regulatory_restrictions
where imp_ref =  :imp_ref and abs(internal_id) = abs(rm_id);

/* update for ORGANIC_CONTENT */
with rm_regulatory_restrictions as
(select distinct internal_id as rm_id,
    coalesce(endpt_id, -nextval('seq_endpt_id')) as endpt_id,
    de_id
from import.import_line
inner join fiche f on f.m_id =  abs(internal_id)
left join entr_fiche_endpt efe on efe.entr_src_id = f.m_id and entr_tp = 'ISO_16128_CONTENT'
left join endpt e on e.endpt_id = efe.entr_dst_id
left join der_endpt on der_endpt.der_src_id = e.endpt_id and der_tp = 'ENDPT_DESC'
left join de on de_tp = 'ENDPT' and de.de_ke = 'ISO_16128_ORGANIC_CONTENT'
where imp_ref = :imp_ref and internal_id is not null
)

--- update the import_line table with the rawmaterial disposal conditions data ---
update import.import_line
set
 value_id7 = endpt_id, /* endpt_id in value_id7 */
  value_id8 = de_id /* de_id in value_id8 */
from rm_regulatory_restrictions
where imp_ref =  :imp_ref and abs(internal_id) = abs(rm_id);


--- insert new endpoint containing the disposal condition if it doesn't already exist ---
insert into endpt (endpt_id, endpt_tp, val, endpt_co,ns)
select distinct abs(value_id1),
 'RAW_MATERIAL_ELIMINATION',
 /* if the storage condition is too long for the val column, get the substring and stock the text in endpt_co */
  /* if the value can be filled in the val column, directly stock it in the val */
  CASE
    WHEN length(value22) > 125 THEN concat(substring(value22, 1, 125), ' ...')
    ELSE value22
  END,
  CASE
    WHEN length(value22) > 125 THEN value22
    ELSE null
  END,
  :user_ns
  from import.import_line
where imp_ref =  :imp_ref and coalesce(value_id1, 0) < 0;

--- insert new relation between the de and the endpoint if it doesn't already exist ---
insert into der_endpt (der_id, der_tp, der_src_id, der_dst_id)
select nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id1), abs(value_id2) from import.import_line
where imp_ref =  :imp_ref  and coalesce(value_id1, 0) <0
group by value_id1, value_id2;

--- insert new relation between the de and the endpoint if it doesn't already exist ---
insert into entr_fiche_endpt (entr_id, entr_tp, entr_src_id, entr_dst_id)
select nextval('seq_entr_fiche_endpt_id'), 'RAW_MATERIAL_ELIMINATION', abs(internal_id), abs(value_id1) from import.import_line
where imp_ref = :imp_ref and coalesce(value_id1, 0) < 0
group by internal_id, value_id1;

--- update the existing endpoint linked to the RM ---
update endpt e
set val = endptVal, endpt_co = endptCo
from (select
  value_id1,
  CASE
    WHEN length(value22) > 125 THEN concat(substring(value22, 1, 125), ' ...')
    ELSE value22
  END endptVal,
  CASE
    WHEN length(value22) > 125 THEN value22
    ELSE null
  END endptCo
  from import.import_line
where imp_ref = :imp_ref and coalesce(value_id1, 0) > 0) existing_rm
where e.endpt_id = existing_rm.value_id1;
