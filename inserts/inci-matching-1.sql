/* Identify the INCI destination */

/* Clean INCI-Names and but the cleaned values in value35 */
/* Delete All Words Between Parthness And Manage The Double Spaces. */
update import.import_line
set value35 = trim(upper(regexp_replace(regexp_replace(value10, '\(([^)(]*)\)', '', 'g'), '(\s+)', ' ', 'g')))
where imp_ref = :imp_ref
  and not coalesce(imp_err, false)
  and coalesce(value10, '') != '';


/* Create the link import_matching based on the given inci_id */
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_ID', 'inci', inci_id
from import.import_line
         inner join anx.inci on inci_id = value_id1
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is not null /* id */
;

/* Create the link import_matching based on the given cleaned inciname and cas */
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE_CAS', 'inci', inci_id
from import.import_line
         inner join anx.inci
                    on ((trim(upper(inciname)) = value35 and cas = value11)
                        or (trim(upper(inciname)) = trim(upper(value10)) and cas = value11))
                        and coalesce(anx_st, 'VALID') = 'VALID'
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value10, '') is not null /* key */
  and not exists(select 1
                 from import.import_matching
                 where import_matching.imp_id = import_line.imp_id
                   and entr_tp like 'INCI_CONCERNED%') /* not already matched */
;

/* Create the link import_matching based on the given inciname (matching by cleaned-inciname or pure-inciname)*/
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE', 'inci', inci_id
from import.import_line
         inner join anx.inci
                    on (trim(upper(inciname)) = value35 or trim(upper(inciname)) = trim(upper(value10)))
                        and coalesce(anx_st, 'VALID') = 'VALID'
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value10, '') is not null /* key */
  and not exists(select 1
                 from import.import_matching
                 where import_matching.imp_id = import_line.imp_id
                   and entr_tp like 'INCI_CONCERNED%') /* not already matched */
;

/* Create the link import_matching based on the given translated INCI_ke */
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE', 'inci', entr_src_id as inci_id
from import.import_line
         inner join anx.incitranslation
                    on (trim(upper(incitranslation.tuv_seg)) = value35 or
                        trim(upper(incitranslation.tuv_seg)) = upper(trim(value10)))
         inner join anx.entr_inci_incitranslation on entr_dst_id = incitranslation_id
         inner join anx.inci on inci_id = entr_src_id and coalesce(anx_st, 'VALID') = 'VALID'
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value35, '') is not null /* key */
  and not exists(select 1
                 from import.import_matching
                 where import_matching.imp_id = import_line.imp_id
                   and entr_tp like 'INCI_CONCERNED%') /* not already matched */
;

/* Create the link import_matching based on INCI Cas */
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_CAS', 'inci', inci_id
from import.import_line
         inner join anx.inci on cas = trim(value11) and coalesce(anx_st, 'VALID') = 'VALID'
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value11, '') is not null /* Cas */
  and not exists(select 1
                 from import.import_matching
                 where import_matching.imp_id = import_line.imp_id
                   and entr_tp like 'INCI_CONCERNED%') /* not already matched */
;

/* Records having a inci_id but no import_matching ⇒ Set error message. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') ||
                  format('The inci with EcoUID "%s" has not been found.', value_id1)
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is not null /* id */
  and 1 <> (select count(1)
            from import.import_matching MATCHING
            where MATCHING.imp_id = import_line.imp_id
              and MATCHING.entr_tp = 'INCI_CONCERNED, BY_ID')
;

/* Records having a inciname but no import_matching. Set error message. Works also for double match. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg || ' | ', '') || format(
            'The inci with name "%s" and cas number "%s" has not been found in any language (or found many times).',
            value10, value11)
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value35, '') is not null /* key */
  and 1 <> (select count(1)
            from import.import_matching MATCHING
            where MATCHING.imp_id = import_line.imp_id
              and MATCHING.entr_tp in ('INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS'))
;

/* Records That Matched by(name,translation, cas) and have multiple matches. */
with matches_count as (select imp_id,
                              string_agg(cast(entr_dst_id as varchar), ',') as matches,
                              count(*)                                         matches_cnt
                       from import.import_matching
                       where entr_tp in ('INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')
                       group by imp_id)
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || '| This INCI Has Multiple Matches. '
from matches_count
where matches_count.imp_id = import_line.imp_id
  and matches_count.matches_cnt > 1
  and imp_ref = :imp_ref
;

/* Warning for records that matched by cas only */
update import.import_line il
set imp_warn     = true,
    imp_warn_msg = coalesce(imp_warn_msg, '') || '| This record is matched by cas only.'
from import.import_matching matching
where il.imp_id = matching.imp_id
  and not coalesce(il.imp_err, false)
  and matching.entr_tp = 'INCI_CONCERNED, BY_CAS'
  and imp_ref = :imp_ref
;

/*check if inci linked to substance create entr_fiche_substance*/
with subIDSelect as (select entr_src_id, entr_dst_id, max(cas), max(ec)
                     from substances
                              inner join anx.ENTR_substances_inci
                                         on substances.sub_id = entr_substances_inci.entr_src_id
                              inner join import.import_line on value_id1 = entr_dst_id
                     where imp_ref = :imp_ref /* Current upload */
                       and coalesce(imp_err, false) = false /* No error */
                       and coalesce(value6, '') != '' /* ingredient */
                       and coalesce(value_id2, 0) < 0
                       and coalesce(value_id1, 0) > 0
                     group by entr_dst_id, entr_src_id)
select *
from subIDSelect;

/* Report inci_id found by inciname and cas number in value_idN. */
update import.import_line
set value_id1 = entr_dst_id
from import.import_matching MATCHING
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value10, '') is not null /* ke */
  and not (coalesce(import_line.imp_err, false)) /*no err */
  and MATCHING.imp_id = import_line.imp_id
  and MATCHING.entr_tp in ('INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')
  and entr_dst_id is not null
;

/*take Benzyl Alcohol as an allergen and not in the composition*/
update import.import_line
set value7       = value6,
    value6       = null,
    imp_warn     = true,
    imp_warn_msg = coalesce(import_line.imp_warn_msg || ' | ', '') ||
                   format('the inci "%s" will be taken as an allergen and not in the composition.', value10)
from import.import_matching MATCHING
where imp_ref = :imp_ref
  and coalesce(value6, '') != ''
  and MATCHING.imp_id = import_line.imp_id
  and MATCHING.entr_tp in
      ('INCI_CONCERNED, BY_ID', 'INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')
  and entr_dst_id is not null
  and 1 = (select count(1)
           from anx.inci
                    inner join anx.entr_substances_inci on entr_dst_id = inci_id and entr_tp = 'INCI_NAME'
           where inciname like 'Benzyl Alcohol%'
             and MATCHING.entr_dst_id = inci_id);

update import.import_line
set value7 = value6,
    value6 = null
from import.import_matching MATCHING
where imp_ref = :imp_ref
  and coalesce(value6, '') != ''
  and MATCHING.imp_id = import_line.imp_id
  and MATCHING.entr_tp in
      ('INCI_CONCERNED, BY_ID', 'INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')
  and entr_dst_id is not null
  and 1 = (select count(1)
           from anx.inci
                    inner join anx.entr_substances_inci on entr_dst_id = inci_id and entr_tp = 'INCI_NAME'
           where entr_src_id in
                 (4405, 4127, 2710, 2974, 2598, 4001, 108650, 3874, 1171, 2920, 2919, 23992, 3067, 1895, 2353, 22729,
                  3069, 2711, 3197, 46223, 2354, 25282, 1030, 3486, 81660, 81659) /*ids of allergen*/
             and MATCHING.entr_dst_id = inci_id);


/*check composition percentage: set error if concentration is not 100% */
update import.import_line
set imp_warn     = true,
    imp_warn_msg = coalesce(imp_warn_msg || ' | ', '') ||
                   format('Error in concentration, Raw Material total concentration is not 100%% but %s%%.',
                          total_composition)
from (select value1                                               as prod_name,
             value2                                               as prod_code,
             imp_ref                                              as prod_imp_ref,
             sum(round(COALESCE(cast(value6 as NUMERIC), 0), 10) +
                 round(COALESCE(cast(value8 as NUMERIC), 0), 10)) as total_composition
      from import.import_line
      where imp_ref = :imp_ref
        and isnumeric(coalesce(value6, '0'))
        and isnumeric(coalesce(value8, '0'))
      group by value1, value2, imp_ref
      having sum(round(COALESCE(cast(value6 as NUMERIC), 0), 10) +
                 round(COALESCE(cast(value8 as NUMERIC), 0), 10)) not in (0, 100)) havingPcPb
where (value1, value2, imp_ref) = (prod_name, prod_code, prod_imp_ref)
;


/*check if inci linked to substance entr_fiche_substance */
/* use dst_table field to store the sub_id string */
with subIDSelect as (select imp_id,
                            entr_dst_id,
                            count(*)                                 as matched_substances_cnt,
                            string_agg(cas, ', ')                    as cas,
                            string_agg(ec, ',')                      as ec,
                            string_agg(cast(sub_id as varchar), ',') as sub_id
                     from substances
                              inner join anx.ENTR_substances_inci
                                         on substances.sub_id = entr_substances_inci.entr_src_id
                              inner join import.import_line on value_id1 = entr_dst_id
                         and coalesce(nullif(value11, ''), substances.cas, '-') = coalesce(substances.cas, '-')
                         and coalesce(nullif(value12, ''), substances.ec, '-') = coalesce(substances.ec, '-')
                     where imp_ref = :imp_ref /* Current upload */
--                        and coalesce(value6, '') != '' /* ingredient */
--                        and coalesce(value_id2, 0) < 0
--                        and coalesce(value_id1, 0) > 0
                     group by imp_id, entr_dst_id)
insert
into import.import_matching(imp_id, dst_table, entr_table, entr_tp, entr_num1, entr_char1, entr_char2)
select imp_id, sub_id, 'entr_substances_inci', 'RELATED_SUBSTANCES', matched_substances_cnt, cas, ec
from subIDSelect
;

/* Error for Records that have multiple substance matches */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(import_line.imp_err_msg, '') ||
                  case
                      when coalesce(value11, '') = '' and coalesce(value12, '') = '' then format('The INCI with %s Id and %s name is related to %s substances with CAS: %s ',value_id1, value10,MATCHING.entr_num1, MATCHING.entr_char1)
                      when coalesce(value11, '') != '' and coalesce(value12, '') = '' then format('The INCI with %s Id and %s name and %s CAS is related to %s substances with EC: %s ',value_id1, value10,value11,MATCHING.entr_num1, MATCHING.entr_char2)
                      else format('The INCI with %s Id and %s name and %s CAS is related to %s substances ',value_id1, value10,value11,MATCHING.entr_num1) end

from import.import_matching MATCHING
where import_line.imp_id = MATCHING.imp_id
  and MATCHING.entr_tp = 'RELATED_SUBSTANCES'
  and MATCHING.entr_num1 > 1
  and imp_ref = :imp_ref
;

/* Report subIds for records that have one matches */
update import.import_line
set value_id7 = cast(dst_table as bigint)
from import.import_matching MATCHING
where MATCHING.imp_id = import_line.imp_id
  and MATCHING.entr_num1 = 1;


/* one substance matched */
insert into import.import_line(imp_ref, value11, value12, value_id1)
values ('substances-matching-test', '', '', 55809),
       ('substances-matching-test', '', '', 50735),
       ('substances-matching-test', '', '', 63897)

update import.import_line
set value11 = '68917-10-2'
where value_id1 = 50735

select *
from import.import_matching where entr_tp = 'RELATED_SUBSTANCES';


/*check if inci linked to substance create entr_fiche_substance*/
WITH subIDSelect AS (
	SELECT max(entr_src_id) entr_src_id, entr_dst_id
	FROM anx.ENTR_substances_inci
	INNER JOIN import.import_line ON value_id1 = entr_dst_id
	WHERE imp_ref = :imp_ref						/* Current upload */
	AND coalesce(imp_err,false) = false				/* No error */
	AND coalesce(value6,'')!= '' 					/* ingredient */
	AND coalesce(value_id2,0) < 0
	AND coalesce(value_id1,0) > 0
	group by entr_dst_id
);

INSERT INTO ENTR_fiche_substances (
	entr_id,
	entr_src_id,
	entr_dst_id,
	entr_tp
)
SELECT
	nextval('seq_entr_fiche_substances_id') as entr_id,
	abs(value_id2) as entr_src_id,
	value_id7 as entr_dst_id,
	'INGREDIENT' as entr_tp
from import.import_line
where imp_ref = :imp_ref						/* Current upload */
and coalesce(imp_err,false) = false				/* No error */
and coalesce(value6,'')!= '' /* ingredient */
and coalesce(value_id2,0) < 0
and coalesce(value_id7, 0)> 0 /* matched substance id */
group by value_id2, value_id7
;

INSERT INTO ENTR_fiche_substances (
	entr_id,
	entr_src_id,
	entr_dst_id,
	entr_tp
)
SELECT
	nextval('seq_entr_fiche_substances_id') as entr_id,
	abs(value_id2) as entr_src_id,
	value_id7 as entr_dst_id,
	'ALLERGEN' as entr_tp
from import.import_line
where imp_ref = :imp_ref						/* Current upload */
and coalesce(imp_err,false) = false				/* No error */
and coalesce(value7,'')!= ''/* allergen */
and coalesce(value_id2,0) < 0
and coalesce(value_id7, 0) > 0 /* matched substance id */
group by value_id2, value_id7
;


select inciname, cas,anx_st, count(*) from anx.inci group by inciname, cas, anx_st having count(*) > 1;

select * from import.import_line order by imp_id desc

delete from import.import_matching where imp_id in (select imp_id from import.import_line where import_matching.imp_id = import_line.imp_id and imp_ref = :imp_ref)

update import.import_line set value_id1 = null where imp_ref = :imp_ref;


select inciname, cas,anx_st from anx.inci where upper(inciname) = 'LIMONENE';


select inciname, cas,anx_st from anx.inci where cas = '5989-27-5';

select inciname, cas,anx_st from anx.inci where inci_id = 43448
select de_ke, de_tp from de


select m_id, sub_id, naml, substances.cas
from substances inner join  entr_fiche_substances on substances.sub_id = entr_fiche_substances.entr_dst_id
inner join fiche on m_id = entr_src_id and split_part(fiche.ns, '/',1) = :user_organisation
	and fiche.tp in ('PERFUME_COMPONENT','IMPURITY')  and fiche.active <> 0  and entr_tp in ('PERFUME_COMPONENT','IMPURITY')


select value10, value11, entr_tp, entr_dst_id from import.import_line
                            inner join import.import_matching on import_line.imp_id = import_matching.imp_id
    where value11 = '5989-27-5'
and imp_ref = 'ImportRawMaterial_3083bf9a-1158-4afc-b7df-4a2febf9d82e_RM';

select * from dir where displayname = 'VANTAGE';

select * from entr_society_dir where entr_dst_id = 2136238;





with matches_count as (select imp_id,
                              entr_tp,
                              string_agg(cast(entr_dst_id as varchar), ',') as matches,
                              count(*)                                         matches_cnt
                       from import.import_matching
                       where entr_tp in ('INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')
                       group by imp_id, entr_tp)
   , min_matches_count as (select distinct imp_id,
                                           matches,
                                           matches_cnt,
                                           entr_tp,
                                           min(matches_cnt) over (partition by imp_id) as min_matches_cnt
                           from matches_count
                           order by imp_id , entr_tp desc)
select * from min_matches_count;

delete from import.import_matching where entr_dst_id in (73917, 73918, 43448);imp_id = 7773; entr_tp in ('INCI_CONCERNED, BY_KE', 'INCI_CONCERNED, BY_KE_CAS', 'INCI_CONCERNED, BY_CAS')

select imp_ref, * from import.import_line order by imp_id desc;

select value_id8, value37 from import.import_line where imp_ref = :imp_ref;

delete from import.import_matching;

update import.import_line set imp_err = null, imp_err_msg = null where imp_ref = :imp_ref;


select entr_char1 from entr_society_dir where entr_id = 74095
