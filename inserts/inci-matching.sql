select regexp_replace('Khaled To (Khalil) Khalel (sss) ', '\(.*\)', 'k', 'g')

select regexp_replace('Khaled To (Khalil) Khalel (sss) (oooo)', '\(([^)(]*)\)', '', 'g')

select regexp_replace(regexp_replace(value1, '\(([^)(]*)\)', '', 'g'), '(\s+)',
                      ' ', 'g')
from import.import_line
where imp_ref = :imp_ref;

select regexp_replace(regexp_replace(value1, '\(([^)(]*)\)', '', 'g'), '(\s+)',
                      ' ', 'g');

insert into import.import_line(imp_ref, value10, value11)
values ('inci_test', '1,4-Dichlorobut-2-Ene', '764-41-0')


/* insert matched incis by inci-name and cas deleting parentheses*/
update import.import_line
set value35 = regexp_replace(regexp_replace(value10, '\(([^)(]*)\)', '', 'g'), '(\s+)', ' ', 'g')
where imp_ref = :imp_ref
  and not coalesce(imp_err, false);

/* Create the link import_matching based on the given inciname, cas */
with matched_inci_count as (select  imp_id,
                                            string_agg(cast(inci_id as varchar), ',') as inci_id,
                                            inciname,
                                            value35,
                                            value10,
                                            count(*) matched_cnt
                            from import.import_line
                                     inner join anx.inci on trim(value35) = trim(inciname) and value11 = cas
                            where imp_ref = :imp_ref
                              and coalesce(value10, '') != ''
                              and coalesce(value11, '') != ''
                            group by imp_id, inciname)
update import.import_line
set imp_err     = case when matched_cnt > 1 then true else imp_err end,
    imp_err_msg = case when matched_cnt > 1 then coalesce(imp_err_msg, '| Matched Multiple INCIs By INCI-Name And CAS') else imp_err_msg end,
    value_id8 =   case when matched_cnt = 1 then cast(matched_inci_count.inci_id as bigint) else value_id8 end
from matched_inci_count
where imp_ref = :imp_ref
  and matched_inci_count.imp_id = import_line.imp_id
;


/* Create the link import_matching based on the given inciname, cas */
with matched_incis_ke_cas as (select  imp_id,
                                             inci_id,
                                            inciname
                            from import.import_line
                                     inner join anx.inci on trim(value35) = trim(inciname) and value11 = cas and coalesce(anx_st, 'VALID') = 'VALID'
                            where imp_ref = :imp_ref
                              and coalesce(value10, '') != ''
                              and coalesce(value11, '') != '')
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE-CAS', 'inci', inci_id
    from matched_incis_ke_cas;


/* Create the link import_matching based on the given inciname  */
with matched_incis_ke as (select  imp_id,
                                             inci_id,
                                            inciname
                            from import.import_line
                                     inner join anx.inci on trim(value35) = trim(inciname) and coalesce(anx_st, 'VALID') = 'VALID'
                            where imp_ref = :imp_ref
                              and coalesce(value10, '') != ''
                              and coalesce(value11, '') != '')
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE', 'inci', inci_id
    from matched_incis_ke
where not exists (select 1 from import.import_matching where import_matching.imp_id = matched_incis_ke.imp_id and entr_tp like 'INCI_CONCERNED%') /* not already matched */
;




update import.import_line
set imp_err     = case when matched_cnt > 1 then true else imp_err end,
    imp_err_msg = case when matched_cnt > 1 then coalesce(imp_err_msg, '| Matched Multiple INCIs By INCI-Name And CAS') else imp_err_msg end,
    value_id8 =   case when matched_cnt = 1 then cast(matched_inci_count.inci_id as bigint) else value_id8 end
from matched_inci_count
where imp_ref = :imp_ref
  and matched_inci_count.imp_id = import_line.imp_id





select value1, value_id8, imp_err, imp_err_msg
from import.import_line where imp_ref = :imp_ref;


insert
into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_inci', 'INCI_CONCERNED, BY_KE', 'inci', inci_id
from import.import_line
         inner join anx.inci on trim(upper(inciname)) = trim(upper(value10)) and coalesce(anx_st, 'VALID') = 'VALID'
where imp_ref = :imp_ref
  and (coalesce(value6, '') != '' or coalesce(value7, '') != '') /*Either ingredient or allergen*/
  and value_id1 is null /* id */
  and nullif(value10, '') is not null /* key */
;



select inciname,
       regexp_replace(regexp_replace(inciname, '\(([^)(]*)\)', '', 'g'), '(\s+)',
                      ' ', 'g')
from anx.inci
where regexp_match(inciname, '\(([^)(]*)\)') is not null;


select entr_src_id, entr_tp, count(entr_id)
from anx.entr_substances_inci
group by entr_src_id, entr_tp
having count(entr_id) > 1

select inciname, cas, count(inci_id)
from anx.inci
where inciname = 'Beta-Carotene'
group by inciname, cas

having count(inci_id) >= 1

select naml, cas, ec, count(sub_id)
from substances
group by naml, cas, ec
having count(sub_id) > 1

select count(*)
from substances
         inner join anx.inci on upper(trim(naml)) = upper(trim(inciname)) and substances.cas = inci.cas
