/* Create the link import_matching based on the given sub_id */
insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id)
select imp_id, 'entr_fiche_substances', 'SUBSTANCE_CONCERNED, BY_ID', 'substances', sub_id
from import.import_line
inner join substances on sub_id = value_id6
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is not null	/* id */
;

/* Create the link import_matching based on the given naml/cas/ec */
insert into import.sub2match(import_id,import_line_ref,cas,ec,naml)
select import_line.imp_id as import_id,import_line.imp_ref as import_line_ref,value11 as cas,value12 as ec,value10 as naml
from import.import_line
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is null		/* Substance Ecouid has not been provided*/
;

insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id, msg)
select v_sub2match_score.import_id, 'entr_fiche_substances', 'SUBSTANCE_CONCERNED, BY_KE', 'substances', sub_id, '('||coalesce(actual_cas,'-')||', '||coalesce(actual_ec,'-')||', '||coalesce(actual_naml,'-')||', '||coalesce(score,'0000')||')' as msg
from import.v_sub2match_score
where import_line_ref = :imp_ref ;

delete from import.sub2match where import_line_ref = :imp_ref ;


/* Create the link import_matching based on the given cleaned naml/cas/ec */
insert into import.sub2match(import_id,import_line_ref,cas,ec,naml)
select import_line.imp_id as import_id,import_line.imp_ref as import_line_ref,value11 as cas,value12 as ec,value35 as naml
from import.import_line
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is null		/* Substance Ecouid has not been provided*/
;

insert into import.import_matching (imp_id, entr_table, entr_tp, dst_table, entr_dst_id, msg)
select v_sub2match_score.import_id, 'entr_fiche_substances', 'SUBSTANCE_CONCERNED, BY_KE', 'substances', sub_id, '('||coalesce(actual_cas,'-')||', '||coalesce(actual_ec,'-')||', '||coalesce(actual_naml,'-')||', '||coalesce(score,'0000')||')' as msg
from import.v_sub2match_score
where import_line_ref = :imp_ref
and not exists(select 1 from import.import_matching where import_matching.imp_id = v_sub2match_score.import_id)
;

delete from import.sub2match where import_line_ref = :imp_ref ;

/* Records having a sub_id but no import_matching ⇒ Set error message. Works also for double match. */
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || format('The substance with EcoUID "%s" has not been found.', value_id6)
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is not null	/* id */
and 1 <> (select count(1) from import.import_matching MATCHING where MATCHING.imp_id = import_line.imp_id and  MATCHING.entr_tp = 'SUBSTANCE_CONCERNED, BY_ID')
;

/* Records having a naml/cas/ec but no import_matching ⇒ Set error message.*/
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || format('The substance (%s, %s, %s) not been found.', value10, value11, value12)
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is null	/* id */
and 0 = (select count(1) from import.import_matching MATCHING where MATCHING.imp_id = import_line.imp_id and  MATCHING.entr_tp = 'SUBSTANCE_CONCERNED, BY_KE')
;

/* Warn about many substances match */
update import.import_line
set imp_warn = true, imp_warn_msg = coalesce(imp_warn_msg || ' | ','') || 'The substance has been found ' || nb || ' times : ' || import_matching.msg
, imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'The substance has been found ' || nb || ' times : At this time, it is forbidden.'
from
(select imp_id, count(1) as nb, string_agg(msg, ', ') as msg  from import.import_matching where entr_tp = 'SUBSTANCE_CONCERNED' group by imp_id having count(1) > 1) as import_matching
where import_line.imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is null	/* id */
and import_matching.imp_id = import_line.imp_id ;
;

/* Report id found by KE in value_idN. */

/* Possible only if only one substance as matched. TODO : May change in the futur */
update import.import_line
set value_id6 = entr_dst_id
from import.import_matching MATCHING
where imp_ref = :imp_ref
and (coalesce(value8,'')!= '' OR coalesce(value9,'')!= '') /*Either perfume or impurity*/
and value_id6 is null	/* id */
and nullif(coalesce(value10,value11,value12),'') is not null  /* ke */
and not(coalesce(import_line.imp_err, false)) /* no err */
and MATCHING.imp_id = import_line.imp_id and  MATCHING.entr_tp = 'SUBSTANCE_CONCERNED, BY_KE' and entr_dst_id is not null
;