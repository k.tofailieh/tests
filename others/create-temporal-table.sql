CREATE TEMPORARY TABLE tmp
(
    name varchar NULL,
    data jsonb   NULL DEFAULT '[]'::jsonb
);

INSERT INTO tmp(name, data)
VALUES ('khaled', '[
  {
    "name": "Primary",
    "type": "primary",
    "values": [
      "h",
      "h23",
      "h2323"
    ]
  },
  {
    "name": "Secondary",
    "type": "secondary",
    "values": [
      "ghg"
    ]
  },
  {
    "name": "Tertiary",
    "type": "tertiary",
    "values": [
      ""
    ]
  },
  {
    "name": "Times",
    "type": "times",
    "values": [
      "T0",
      "t2",
      "t3"
    ]
  }
]');

SELECT *
FROM study
WHERE gridtest_metadata IS NOT NULL;


SELECT data[1]->>'name' from tmp;


DROP VIEW IF EXISTS ns_star.study;

CREATE OR REPLACE VIEW ns_star.study AS SELECT * FROM public.study WHERE ((study.ns IS NULL) OR ((study.ns)::text ~~ ANY ( SELECT ns_ok.ns_like FROM ns_ok))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
