create temporary table t1(entr_id, entr_tp, ordr) as
select entr_id, entr_tp, '1'
from entr_fiche_nomclit
where entr_id < 2000;


create temporary table t2(entr_id, entr_tp, ordr) as
select entr_id, entr_tp, '2'
from entr_nomclit_fiche
where entr_id < 2000;



create temporary view test_view as
select entr_id, entr_tp, '1' as ordr
from t1
union
select entr_id, entr_tp, '2' as ordr
from t2;


drop view if exists test_view;

select *
from test_view;

update test_view
set entr_id = 3
where entr_id = 2;


create or replace function f_update_view() returns trigger as
$$
begin
    if lower(substring(new.ordr from 1 for 1)) = '1' then
        update t1 set entr_id = new.entr_id where t1.entr_id = old.entr_id;
        return new;
    elsif lower(substring(new.ordr from 1 for 1)) = '2' then
        update t2 set entr_id = new.entr_id where t2.entr_id = old.entr_id;

        return new;
    else
        return new;
    end if;
end
$$
    language 'plpgsql';


create trigger t_update
    instead of update
    on test_view
    for each row
execute procedure f_update_view();

update test_view set entr_id = 2 where test_view.entr_id = 3;

select * from test_view;