WITH mat_risks AS
         (SELECT lfp.entr_src_id AS m_id, CAST('CLP' AS varchar) AS plctp, hstmt.plcsys_ke
          FROM entr_fiche_plc lfp
                   INNER JOIN entr_plc_plcsys lpp1
                              ON lpp1.entr_src_id = lfp.entr_dst_id AND lpp1.entr_tp = 'HAZARD_CLASS_CAT'
                   INNER JOIN entr_plcsys_plcsys lpp2 ON lpp2.entr_src_id = lpp1.entr_dst_id AND lpp2.entr_tp =
                                                                                                 CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                   INNER JOIN plcsys hstmt ON hstmt.plcsys_id = lpp2.entr_dst_id AND hstmt.name = 'GHS_H_STMT'
                   INNER JOIN fiche ON m_id = lfp.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
          WHERE lfp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
          UNION ALL
          SELECT lfp.entr_src_id AS m_id, CAST('DPD' AS varchar) AS plctp, hstmt.plcsys_ke
          FROM entr_fiche_plc lfp
                   INNER JOIN entr_plc_plcsys lpp1
                              ON lpp1.entr_src_id = lfp.entr_dst_id AND lpp1.entr_tp = 'RISK'
                   INNER JOIN plcsys hstmt ON hstmt.plcsys_id = lpp1.entr_dst_id AND hstmt.tp = 'RISK'
                   INNER JOIN fiche ON m_id = lfp.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
          WHERE lfp.entr_tp IN ('DPD_S')
            AND NOT exists(SELECT 1
                           FROM entr_fiche_plc lfclp
                                    INNER JOIN entr_plc_plcsys lclpplcsys ON lclpplcsys.entr_src_id = lfclp.entr_dst_id
                           WHERE lfclp.entr_src_id = lfp.entr_src_id
                             AND lfclp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL))
          ),
     mat_symboles AS
         (SELECT DISTINCT lfp.entr_src_id AS m_id, CAST('CLP' AS varchar) AS plctp, hstmt.plcsys_ke
                      FROM entr_fiche_plc lfp
                               INNER JOIN entr_plc_plcsys lpp1
                                          ON lpp1.entr_src_id = lfp.entr_dst_id AND lpp1.entr_tp = 'HAZARD_CLASS_CAT'
                               INNER JOIN entr_plcsys_plcsys lpp2 ON lpp2.entr_src_id = lpp1.entr_dst_id AND
                                                                     lpp2.entr_tp =
                                                                     CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                               INNER JOIN plcsys hstmt
                                          ON hstmt.plcsys_id = lpp2.entr_dst_id AND hstmt.name = 'GHS_PICTO'
                               INNER JOIN fiche ON m_id = lfp.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
                      WHERE lfp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                      UNION ALL
                      SELECT DISTINCT lfp.entr_src_id        AS m_id,
                                      CAST('DPD' AS varchar) AS plctp,
                                      CASE
                                          WHEN hstmt.plcsys_ke IN
                                               ('Carc. Cat. 1', 'Carc. Cat. 2', 'Muta. Cat. 1', 'Muta. Cat. 2',
                                                'Repr. Cat. 2', 'Repr. Cat. 2') THEN 'T'
                                          WHEN hstmt.plcsys_ke IN ('Carc. Cat. 3', 'Muta. Cat. 3', 'Repr. Cat. 3')
                                              THEN 'Xn'
                                          ELSE hstmt.plcsys_ke END
                      FROM entr_fiche_plc lfp
                               INNER JOIN entr_plc_plcsys lpp1
                                          ON lpp1.entr_src_id = lfp.entr_dst_id AND lpp1.entr_tp = 'RISK'
                               INNER JOIN entr_plcsys_plcsys lpp2
                                          ON lpp2.entr_src_id = lpp1.entr_dst_id AND lpp2.entr_tp = 'RISK_2_DANGER'
                               INNER JOIN plcsys hstmt ON hstmt.plcsys_id = lpp2.entr_dst_id
                               INNER JOIN fiche ON m_id = lfp.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
                      WHERE lfp.entr_tp IN ('DPD_S')
                        AND NOT exists(SELECT 1
                                       FROM entr_fiche_plc lfclp
                                                INNER JOIN entr_plc_plcsys lclpplcsys ON lclpplcsys.entr_src_id = lfclp.entr_dst_id
                                       WHERE lfclp.entr_src_id = lfp.entr_src_id
                                         AND lfclp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL))
        ),
     boilingpts AS
         (SELECT lfe.entr_src_id                        AS m_id,
                           CAST(NULLIF(endpt.val, '') AS DECIMAL) AS val,
                           endpt.value_max                        AS valmax,
                           NULLIF(endpt.value_operator, '')       AS valop,
                           NULLIF(endpt.value_unit, '')           AS valunit
                    FROM entr_fiche_endpt lfe
                             INNER JOIN endpt ON endpt_id = lfe.entr_dst_id
                             INNER JOIN der_endpt ON der_src_id = endpt_id AND der_tp = 'ENDPT_DESC'
                             INNER JOIN de ON de_id = der_dst_id AND de_ke = 'BOILPOINT'
                             INNER JOIN fiche ON m_id = lfe.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
                    WHERE lfe.entr_tp = 'MAT_ENDPT'
                      AND COALESCE(endpt.val ~ '^-?\d+\.?\d*$', FALSE) /* is numeric not null not empty*/
                      AND NOT exists( /* un plus petit ou pareil avec id > */ SELECT 1
                                                                              FROM entr_fiche_endpt lfeMin
                                                                                       INNER JOIN endpt endptMin ON endptMin.endpt_id = lfeMin.entr_dst_id
                                                                                       INNER JOIN der_endpt derMin
                                                                                                  ON derMin.der_src_id = endptMin.endpt_id AND derMin.der_tp = 'ENDPT_DESC'
                                                                              WHERE lfeMin.entr_src_id = lfe.entr_src_id
                                                                                AND derMin.der_dst_id = der_endpt.der_dst_id
                                                                                AND COALESCE(endptMin.val ~ '^-?\d+\.?\d*$', FALSE) /* is numeric not null not empty*/
                                                                                AND (CAST(NULLIF(endptMin.val, '') AS DECIMAL) <
                                                                                     CAST(NULLIF(endpt.val, '') AS DECIMAL) OR
                                                                                     (endptMin.val = endpt.val AND endptMin.endpt_id > endpt.endpt_id)))
                    ),
     vlesOfCompo AS
         (SELECT efs.entr_src_id AS m_id, ers.entr_float1 AS vleMg
                     FROM entr_reg_substances ers
                              INNER JOIN reg ON reg_id = ers.entr_src_id AND reg_ke = 'VLEP_FR'
                              INNER JOIN entr_fiche_substances efs ON efs.entr_dst_id = ers.entr_dst_id
                              INNER JOIN fiche ON m_id = efs.entr_src_id AND fiche.ns LIKE :user_organisation || '%'
                     WHERE ers.entr_tp = 'SUBSTANCES'
         )
SELECT intermediateSite.site_id                                                     AS site_id,
       intermediateSite.site_li                                                     AS site_name,
       fos.m_id                                                                     AS mat_id,
       fos.reference                                                                AS reference,
--        fos.numfiche                                                                                      AS numfiche,
       fos.marguage                                                                 AS marque,
       fos.m_ref                                                                    AS m_ref,
       (SELECT max(sdsdoc.date_of_revision)
        FROM entr_fiche_sdsdoc lfs
                 INNER JOIN sdsdoc ON sdsdoc_id = lfs.entr_dst_id
        WHERE lfs.entr_src_id = fos.m_id
          AND entr_tp = 'SDS')                                                      AS last_sds_dt,
       (SELECT max(mat_risks.plctp) FROM mat_risks WHERE mat_risks.m_id = fos.m_id) AS plctp,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 0)                                                           AS plctp0,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 1)                                                           AS plctp1,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 2)                                                           AS plctp2,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 3)                                                           AS plctp3,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 4)                                                           AS plctp4,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 5)                                                           AS plctp5,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 6)                                                           AS plctp6,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 7)                                                           AS plctp7,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 8)                                                           AS plctp8,
       (SELECT mat_risks.plcsys_ke
        FROM mat_risks
        WHERE mat_risks.m_id = fos.m_id
        ORDER BY mat_risks.plcsys_ke
        LIMIT 1 OFFSET 9)                                                           AS plctp9,
       (SELECT string_agg(DISTINCT mat_symboles.plcsys_ke, ', ' ORDER BY mat_symboles.plcsys_ke)
        FROM mat_symboles
        WHERE mat_symboles.m_id = fos.m_id
          AND (mat_symboles.plcsys_ke BETWEEN 'GHS05' AND 'GHS08' OR
               mat_symboles.plcsys_ke IN ('T+', 'T', 'C', 'Xn', 'Xi'))
        GROUP BY mat_symboles.m_id)                                                 AS symbole_sante,
       (SELECT string_agg(DISTINCT mat_symboles.plcsys_ke, ', ' ORDER BY mat_symboles.plcsys_ke)
        FROM mat_symboles
        WHERE mat_symboles.m_id = fos.m_id
          AND (mat_symboles.plcsys_ke BETWEEN 'GHS01' AND 'GHS04' OR mat_symboles.plcsys_ke IN ('F+', 'F', 'E', 'O'))
        GROUP BY mat_symboles.m_id)                                                 AS symbole_secu,
       (SELECT string_agg(DISTINCT mat_symboles.plcsys_ke, ', ' ORDER BY mat_symboles.plcsys_ke)
        FROM mat_symboles
        WHERE mat_symboles.m_id = fos.m_id
          AND (mat_symboles.plcsys_ke IN ('GHS09', 'N'))
        GROUP BY mat_symboles.m_id)                                                 AS symbole_env,
       (SELECT max(endpt.val)
        FROM entr_fiche_endpt lfe
                 INNER JOIN endpt ON endpt_id = lfe.entr_dst_id
                 INNER JOIN der_endpt ON der_src_id = endpt_id AND der_tp = 'ENDPT_DESC'
                 INNER JOIN de ON de_id = der_dst_id AND de_ke = 'PSTATE_USE'
        WHERE lfe.entr_src_id = fos.m_id
          AND lfe.entr_tp = 'MAT_ENDPT')                                            AS pstate_use,
       (SELECT val FROM boilingpts WHERE boilingpts.m_id = fos.m_id)                AS boilingpoint,
       (SELECT valmax FROM boilingpts WHERE boilingpts.m_id = fos.m_id)             AS boilingpointmax,
       (SELECT valop FROM boilingpts WHERE boilingpts.m_id = fos.m_id)              AS boilingpointop,
       (SELECT valunit FROM boilingpts WHERE boilingpts.m_id = fos.m_id)            AS boilingpointunit,
       (SELECT min(vleMg)
        FROM vlesOfCompo
        GROUP BY vlesOfCompo.m_id, fos.m_id
        HAVING vlesOfCompo.m_id = fos.m_id)                                         AS maxVleMg /* Presence info */ ,
       presence.presence_dt1                                                        AS startingDate,
       presence.presence_dt2                                                        AS endingDate,
       presence.presence_cmt                                                        AS commentMaterial /* Ext form */ ,
       extFormData.st                                                               AS stateOfMaterial /* Storage */ ,
       storageOfMaterial.val_num                                                    AS storageOfMaterial /* Qte */ ,
       CASE
           WHEN NULLIF(amountOfMaterial.val_num, 0) IS NULL THEN amountOfMaterial.val_float
           ELSE amountOfMaterial.val_num END                                        AS amountOfMaterial,
       amountOfMaterial_Unit.val_char || ' /'                                       AS amountOfMaterial_Unit,
       lower(amountOfMaterial_UnitPerFreq.val_char)                                 AS amountOfMaterial_UnitPerFreq /* Other Ext */ ,
       lower(useFreq.val_char)                                                      AS useFreq,
       useTemp.val_num                                                              AS useTemp,
       lower(useProcess.val_char)                                                   AS useProcess,
       lower(modedapplication.val_char)                                             AS modedapplication,
       lower(modedapplicationsiautres.val_char)                                     AS modedapplicationsiautres,
       lower(useCollectiveProtection.val_char)                                      AS useCollectiveProtection,
       lower(exposedSurface.val_char)                                               AS exposedSurface,
       lower(exposureFreq.val_char)                                                 AS exposureFreq,
       lower(fireSrcFreq.val_char)                                                  AS fireSrcFreq,
       lower(waterContactProbability.val_char)                                      AS waterContactProbability,
       lower(endPt_redondant.val_char)                                              AS endPt_EtatPhysique,
       lower(acidContactProbability.val_char)                                       AS acidContactProbability
FROM fiche fos
         INNER JOIN entr_presence_fiche pf ON pf.entr_dst_id = m_id AND pf.entr_tp IN ('MATERIAL', 'PRESENCE')
         INNER JOIN presence ON presence_id = pf.entr_src_id AND
                                coalesce(presence.presence_dt1, CURRENT_DATE) <= CURRENT_DATE AND
                                coalesce(presence.presence_dt2, CURRENT_DATE + 1) > CURRENT_DATE
         INNER JOIN entr_site_presence sp
                    ON (sp.entr_dst_id = pf.entr_src_id) AND (sp.entr_tp IN ('MATERIAL', 'PRESENCE'))
         INNER JOIN site intermediateSite ON intermediateSite.site_id = sp.entr_src_id
         INNER JOIN site ON site.tree_key = intermediateSite.tree_key AND
                            intermediateSite.site_lft BETWEEN site.site_lft AND site.site_rgt /* Ext form */
         LEFT OUTER JOIN (SELECT *
                          FROM ext
                                   INNER JOIN extr_presence ON extr_presence.extr_dst_id = ext_id AND
                                                               extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE ext.ns LIKE :user_organisation || '%') extFormData
                         ON extFormData.extr_src_id = presence_id /* Storage */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'storage'
                            AND extfld.ns LIKE :user_organisation || '%') storageOfMaterial
                         ON storageOfMaterial.extr_src_id = presence_id /* Qte */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'amountOfMaterial'
                            AND extfld.ns LIKE :user_organisation || '%') amountOfMaterial
                         ON amountOfMaterial.extr_src_id = presence_id
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'amountOfMaterial_Unit'
                            AND extfld.ns LIKE :user_organisation || '%') amountOfMaterial_Unit
                         ON amountOfMaterial_Unit.extr_src_id = presence_id
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'amountOfMaterial_UnitPerFreq'
                            AND extfld.ns LIKE :user_organisation || '%') amountOfMaterial_UnitPerFreq
                         ON amountOfMaterial_UnitPerFreq.extr_src_id = presence_id /* useFreq */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'useFreq'
                            AND extfld.ns LIKE :user_organisation || '%') useFreq
                         ON useFreq.extr_src_id = presence_id /* useTemp */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'useTemp'
                            AND extfld.ns LIKE :user_organisation || '%') useTemp
                         ON useTemp.extr_src_id = presence_id /* useProcess */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'useProcess'
                            AND extfld.ns LIKE :user_organisation || '%') useProcess
                         ON useProcess.extr_src_id = presence_id /* Modedapplication */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'Modedapplication'
                            AND extfld.ns LIKE :user_organisation || '%') modedapplication
                         ON modedapplication.extr_src_id = presence_id /* Modedapplicationsiautres */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'Modedapplicationsiautres'
                            AND extfld.ns LIKE :user_organisation || '%') modedapplicationsiautres
                         ON modedapplicationsiautres.extr_src_id = presence_id /* useCollectiveProtection */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'useCollectiveProtection'
                            AND extfld.ns LIKE :user_organisation || '%') useCollectiveProtection
                         ON useCollectiveProtection.extr_src_id = presence_id /* exposedSurface */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'exposedSurface'
                            AND extfld.ns LIKE :user_organisation || '%') exposedSurface
                         ON exposedSurface.extr_src_id = presence_id /* exposureFreq */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'exposureFreq'
                            AND extfld.ns LIKE :user_organisation || '%') exposureFreq
                         ON exposureFreq.extr_src_id = presence_id /* fireSrcFreq */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'fireSrcFreq'
                            AND extfld.ns LIKE :user_organisation || '%') fireSrcFreq
                         ON fireSrcFreq.extr_src_id = presence_id /* waterContactProbability */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'waterContactProbability'
                            AND extfld.ns LIKE :user_organisation || '%') waterContactProbability
                         ON waterContactProbability.extr_src_id = presence_id /* EndPt, redondant */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = '(EndPt, redondant)'
                            AND extfld.ns LIKE :user_organisation || '%') endPt_redondant
                         ON endPt_redondant.extr_src_id = presence_id /* acidContactProbability */
         LEFT OUTER JOIN (SELECT *
                          FROM extfld
                                   INNER JOIN entr_ext_extfld ON entr_ext_extfld.entr_dst_id = extfld_id AND
                                                                 entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
                                   INNER JOIN extr_presence
                                              ON extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id AND
                                                 extr_presence.extr_tp = 'FORM_INSTANCE'
                          WHERE extfld.itemid = 'acidContactProbability'
                            AND extfld.ns LIKE :user_organisation || '%') acidContactProbability
                         ON acidContactProbability.extr_src_id = presence_id /* where site.tree_key ilike 'V%' */ /*where m_id in (ids) *//* (32540,29986,29994, 30194,1,2,3) */
WHERE /*and*/ ((cast(:siteId AS bigint) <= 0) OR (site.site_id = cast(:siteId AS bigint)))
ORDER BY intermediateSite.site_li NULLS LAST, m_id;