select s.*
from anx.inci
     inner join anx.entr_substances_inci on inci_id = entr_substances_inci.entr_dst_id
     inner join substances s on entr_substances_inci.entr_src_id = s.sub_id
where inciname = 'CI 77000'



select naml, cas, ec, count(*)
from substances
where coalesce(cas, '') != ''
  and coalesce(ec, '') != ''
  and coalesce(naml, '') != ''
group by naml, cas, ec
having count(*) > 1
;

with substances_duplication as (select sub_id,
                                       naml,
                                       cas,
                                       ec,
                                       count(*) over (partition by naml, cas, ec) over_all,
                                       count(*) over (partition by cas, ec)       over_cas_ec
                                from substances
                                where coalesce(cas, '') != ''
                                  and coalesce(ec, '') != '')
select *
from substances_duplication
where over_all > 1
   or over_cas_ec > 1
;



with duplicated_incis as (select fiche.m_id,
                                 row_number() over (partition by inci_id, fiche.tp)      as rn,
                                 first_value(m_id) over (partition by inci_id, fiche.tp) as fv,
                                 inciname,
                                 inci.cas,
                                 fiche.tpsfrom                                           as ING_CR_DT,
                                 entr_fiche_inci.tpsfrom                                 as entr_fiche_inci_cr_dt,
                                 ns,
                                 count(*) over (partition by inci_id, fiche.tp)          as cnt
                          from fiche
                               inner join anx.entr_fiche_inci on entr_src_id = m_id
                               inner join anx.inci on inci_id = entr_dst_id

                          where split_part(ns, '/', 1) = 'OCCITANE'
                            and fiche.tp in ('INGREDIENT', 'ALLERGEN')
                            and anx_st = 'VALID'
                            and fiche.active in (1, 2)
                          order by inciname desc)
select *
from duplicated_incis
where cnt > 1



select F.m_id                                                       as entity_id
     , F.m_ref
     , F.tp
     , F.active
     , F.ec
     , s.naml
     , s.cas
     , s.sub_id
     , I.inci_id
     , I.tp                                                         as incitype
     , string_agg(distinct app.app_ke, ',')                         as ingredientfunction
     , nullif(concat_ws(',', string_agg(distinct eis.entr_tp, ','), string_agg(distinct eie.entr_tp, ',')),
              '')                                                   as incistudyendpttype
     , case when I.inciname is null then s.naml else I.inciname end as ingredientname
     , e1.endpt_xml
     , e1.val
     , s1.study_xml
     , s1.md_dt
     , d1.de_li
     , e2.endpt_xml                                                 as dap_endpt_xml
     , s2.study_xml                                                 as dap_study_xml
     , s2.md_dt                                                     as dap_md_dt
     , d2.de_li                                                     as dap_de_li
     , e2.val                                                       as dap_val
from fiche F
     inner join      entr_fiche_substances efs
                     on F.m_id = efs.entr_src_id and efs.entr_tp in ('INGREDIENT', 'PERFUME_COMPONENT')
     inner join      substances s on s.sub_id = efs.entr_dst_id

     left join       anx.entr_fiche_inci efi
                     on efi.entr_src_id = F.m_id and efi.entr_tp in ('INGREDIENT', 'PERFUME_COMPONENT')
     left join       anx.inci I on I.inci_id = efi.entr_dst_id


     left outer join anx.entr_inci_app eip on eip.entr_src_id = I.inci_id
     left outer join app on eip.entr_tp = 'COSMETIC_FUNCTION' and app.app_id = eip.entr_dst_id

     left outer join anx.entr_inci_study eis on eis.entr_src_id = I.inci_id and eis.entr_tp = 'STUDY_PRESELECTED'
     left outer join anx.entr_inci_endpt eie
                     on eie.entr_src_id = I.inci_id and eie.entr_tp in ('NOAEL_PRESELECTED', 'DAP_PRESELECTED')

                         -- select NOAEL
     left join       entr_fiche_endpt efe1
                     on efe1.entr_src_id = F.m_id and efe1.entr_tp in ('NOAEL_SELECTED', 'NOAEL_INSERTED')
     left join       endpt e1 on e1.endpt_id = efe1.entr_dst_id
     left join       entr_study_endpt ese1 on ese1.entr_dst_id = e1.endpt_id and ese1.entr_tp = 'ENDPT'
     left join       study s1 on s1.study_id = ese1.entr_src_id
     left join       der_study ds1 on ds1.der_src_id = s1.study_id and ds1.der_tp = 'STUDY_TP'
     left join       de d1 on d1.de_id = ds1.der_dst_id

                         -- select DAP SELECTED DAP INSERTED
     left join       entr_fiche_endpt efe2
                     on efe2.entr_src_id = F.m_id and efe2.entr_tp in ('DAP_SELECTED', 'DAP_INSERTED')
     left join       endpt e2 on e2.endpt_id = efe2.entr_dst_id
     left join       entr_study_endpt ese2 on ese2.entr_dst_id = e2.endpt_id and ese2.entr_tp = 'ENDPT'
     left join       study s2 on s2.study_id = ese2.entr_src_id
     left join       der_study ds2 on ds2.der_src_id = s2.study_id and ds2.der_tp = 'STUDY_TP'
     left join       de d2 on d2.de_id = ds2.der_dst_id

                         -- select DAP RAW
     left join       entr_fiche_endpt efe3
                     on efe3.entr_src_id = F.m_id and efe3.entr_tp in ('DAP_SELECTED', 'DAP_INSERTED')
     left join       endpt e3 on e3.endpt_id = efe3.entr_dst_id

where F.active <> 0
  and F.tp in ('INGREDIENT', 'PERFUME_COMPONENT')
  and upper(F.ns) like 'OCCI%'
group by F.m_id, entity_id, F.m_ref, F.tp, F.active, F.ec, s.naml, s.cas, s.sub_id, I.inci_id, I.tp, I.inciname,
         e1.endpt_xml, e1.val, s1.study_xml, s1.md_dt, d1.de_li, e2.endpt_xml, s2.study_xml, s2.md_dt, d2.de_li, e2.val
order by s.naml;
