SELECT n1.nomclit_id         nId1,
       n1.item_tp            nItemTp1,
       n1.entity_tp          nTp1,
       n1.conc_unit          nConUn1,
       n1.md_dt              nMd1,
       n1.alt_st             nAlt1,
       n1.conc_real_ubound   nConRU1,
       n1.conc_direct_ubound nConDU1,

       efn1.entr_id          efnId1,
       efn1.entr_tp          efnTp1,

       f1.m_id AS            fId1,

       df1.der_id            dfId1,
       df1.der_tp            dfTp1,

       de.de_id              deId,
       de.de_tp              deTp,

       enf1.entr_id          enfId1,
       enf1.entr_tp          enfTp1,

       f2.m_id               fId2,
       f2.m_ref              fMref2,
       f2.tp                 fTp2,

       efi1.entr_id          efiId1,
       efi1.entr_tp          efiTp1,
       i1.inci_id            inId1,
       i1.inciname           inTp1,

       enf2.entr_id          enfId2,
       enf2.entr_tp          enfTp2,

       n2.nomclit_id         nId2,

       efn2.entr_id          efnId2,
       efn2.entr_tp          efnTp2,

       f3.m_id,
       f3.m_ref,

       enr1.entr_id,
       enr1.entr_tp,

       r1.regentry_id,
       r1.tp,
       r1.conc_threshold_lbound,
       r1.conc_threshold_ubound,
       r1.conc_threshold_lbound_inc,
       r1.conc_threshold_ubound_inc,
       r1.assertion1,
       r1.assertion2,
       r1.assertion3,
       r1.assertion4,
       r1.conc_unit,
       r1.ref,
       r1.nt,

       err1.entr_id,
       err1.entr_tp,

       reg2.reg_id,
       reg2.name,
       reg2.reg_ke,

       dr1.der_id,
       dr1.der_tp,

       de1.de_id,
       de1.de_ke,
       de1.de_tp,

       era.entr_id,
       era.entr_tp,

       app.app_id,

       eri.entr_id,
       eri.entr_tp,

       inci3.inci_id,
       inci3.inciname,

       enreg1.entr_id,
       enreg1.entr_tp,

       reg.reg_id

FROM nomclit n1

         INNER JOIN entr_fiche_nomclit efn1
                    ON efn1.entr_dst_id = n1.nomclit_id AND efn1.entr_tp IN ('REAL_COMPO_FICHE_INGREDIENT')
         INNER JOIN fiche f1 ON f1.m_id = efn1.entr_src_id AND f1.m_id = 514343

         LEFT JOIN der_fiche df1 ON df1.der_src_id = f1.m_id AND df1.der_tp IN ('ACTIVATION', 'INHIBITION')
         LEFT JOIN de ON de.de_id = df1.der_dst_id AND de.de_tp = 'COSMETIC_REGULATORY_TYPE'

         INNER JOIN entr_nomclit_fiche enf1
                    ON enf1.entr_src_id = n1.nomclit_id AND enf1.entr_tp IN ('NOMENCLATURE_ITEM_FOR_FICHE_FORMULA')
         INNER JOIN fiche f2 ON f2.m_id = enf1.entr_dst_id AND f2.tp IN ('INGREDIENT', 'ALLERGEN')

         LEFT JOIN anx.entr_fiche_inci efi1 ON efi1.entr_src_id = f2.m_id AND efi1.entr_tp IN ('INGREDIENT', 'ALLERGEN')
         LEFT JOIN anx.inci i1 ON i1.inci_id = efi1.entr_dst_id

         LEFT JOIN entr_nomclit_fiche enf2 ON enf2.entr_dst_id = f2.m_id AND enf2.entr_tp = 'HAS'
         LEFT JOIN nomclit n2 ON n2.nomclit_id = enf2.entr_src_id

         LEFT JOIN entr_fiche_nomclit efn2 ON efn2.entr_dst_id = n2.nomclit_id AND efn2.entr_tp = 'DIRECT_COMPO'
         LEFT JOIN fiche f3 ON f3.m_id = efn2.entr_src_id

         INNER JOIN entr_nomclit_fiche enf3
                    ON enf3.entr_dst_id = f3.m_id AND enf3.entr_tp = 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA'
         INNER JOIN nomclit n3 ON n3.nomclit_id = enf3.entr_src_id
         INNER JOIN entr_fiche_nomclit efn3
                    ON efn3.entr_dst_id = n3.nomclit_id AND efn3.entr_tp = 'DIRECT_COMPO_FICHE_RM'
         INNER JOIN fiche f4 ON f4.m_id = efn3.entr_src_id AND f4.m_id = 514343


         LEFT JOIN entr_nomclit_regentry enr1
                   ON enr1.entr_src_id = n1.nomclit_id AND enr1.entr_tp IN ('AUTHORIZATION', 'INTERDICTION',
                                                                            'RESTRICTION')
         LEFT JOIN regentry r1 ON r1.regentry_id = enr1.entr_dst_id

         LEFT JOIN entr_reg_regentry err1 ON err1.entr_dst_id = r1.regentry_id AND err1.entr_tp = 'REGULATION_ENTRY'
         LEFT JOIN reg reg2 ON reg2.reg_id = err1.entr_src_id

         LEFT JOIN der_regentry dr1 ON dr1.der_src_id = r1.regentry_id AND dr1.der_tp IN ('ACTIVATION', 'INHIBITION')
         LEFT JOIN de de1 ON de1.de_id = dr1.der_dst_id

         LEFT JOIN entr_regentry_app era
                   ON era.entr_src_id = r1.regentry_id AND era.entr_tp IN ('ACTIVATION', 'INHIBITION')
         LEFT JOIN app ON app.app_id = era.entr_dst_id

         LEFT JOIN anx.entr_regentry_inci eri ON eri.entr_src_id = r1.regentry_id AND eri.entr_tp = 'INCI_CONCERNED'
         LEFT JOIN anx.inci inci3 ON inci3.inci_id = eri.entr_dst_id

         LEFT JOIN entr_nomclit_reg enreg1
                   ON enreg1.entr_src_id = n1.nomclit_id AND enreg1.entr_tp = 'COMPLIANT_OVERRIDE'
         LEFT JOIN reg ON reg.reg_id = enreg1.entr_dst_id


WHERE (n1.item_tp = 'MATERIAL' AND n1.entity_tp IN ('INGREDIENT', 'ALLERGEN'))
  AND (n1.alt_st IS NULL OR n1.alt_st != 'INVALID')
ORDER BY n1.nomclit_id DESC
;

SELECT de.*
FROM fiche
         INNER JOIN entr_fiche_sdsdoc efs ON fiche.m_id = efs.entr_src_id
         INNER JOIN docr_sds ON efs.entr_dst_id = docr_src_id
         INNER JOIN sdsdoc ON docr_sds.docr_src_id = sdsdoc.sdsdoc_id
         INNER JOIN der_sdsdoc ds ON efs.entr_dst_id = ds.der_src_id
         INNER JOIN de ON ds.der_dst_id = de.de_id
WHERE m_id = 43537;


SELECT der_tp, der_st, de.*
FROM fiche
         INNER JOIN docr_fiche df ON fiche.m_id = df.docr_src_id
         INNER JOIN der_doc ON der_src_id = docr_dst_id
         INNER JOIN de ON der_doc.der_dst_id = de.de_id


SELECT *
FROM substances
WHERE sub_id =


-----------------------------------------------


SELECT
    /* substances */
       substances.sub_id,
       naml,
       cas,

    /* entr_substances_plc */
       esp.entr_substances_plc_id,
       esp.entr_substances_plc_tp,
       esp.plc_id,
       esp.entr_plc_plcseys_id,
       esp.entr_plc_plcseys_tp,
       esp.plcsys_id,
       esp.plcsys_ke,
       esp.plcsys_tp,

    /* entr_substances_subset */
       ess.entr_subset_substances_id,
       ess.entr_subset_substances_tp,
       ess.subset_id,
       ess.entr_regentry_subset_id,
       ess.entr_regentry_subset_tp,
       ess.subset_regentry_id,
       ess.entr_reg_regentry_id,
       ess.entr_reg_regentry_tp,
       ess.subset_reg_id,
       ess.subset_reg_ke,

    /* entr_reg_substances */
       ers.entr_reg_substances_id,
       ers.entr_reg_substances_tp,
       ers.entr_reg_substances_co,
       ers.substance_reg_id,
       ers.substance_reg_desc,
       ers.substance_reg_info,
       ers.substance_reg_name,

    /* entr_regentry_substances */
       ers2.entr_regentry_substances_id,
       ers2.entr_regentry_substances_tp,
       ers2.entr_regentry_substances_co,
       ers2.substances_regentry_id,
       ers2.substances_regentry_ref,
       ers2.substances_regentry_co,
       ers2.substances_regentry_tp,
       ers2.entr_regentry_reg_id,
       ers2.entr_regentry_reg_tp,
       ers2.substance_reg_id,
       ers2.substance_reg_ke
FROM substances
         LEFT JOIN (SELECT esp.entr_id     AS entr_substances_plc_id,
                           esp.entr_src_id AS sub_id,
                           esp.entr_tp     AS entr_substances_plc_tp,
                           plc.plc_id      AS plc_id,
                           epp.entr_id     AS entr_plc_plcseys_id,
                           epp.entr_tp     AS entr_plc_plcseys_tp,
                           pls.plcsys_id,
                           pls.plcsys_ke,
                           pls.tp          AS plcsys_tp

                    FROM entr_substances_plc esp
                             INNER JOIN plc ON esp.entr_dst_id = plc.plc_id
                             INNER JOIN entr_plc_plcsys epp
                                        ON esp.entr_dst_id = epp.entr_src_id AND
                                           epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                             INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id) esp
                   ON substances.sub_id = esp.sub_id AND
                      esp.entr_substances_plc_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')

         LEFT JOIN (SELECT ess.entr_dst_id             AS sub_id,
                           ess.entr_id                 AS entr_subset_substances_id,
                           ess.entr_tp                 AS entr_subset_substances_tp,
                           subset.subset_id,
                           ers.entr_id                 AS entr_regentry_subset_id,
                           ers.entr_tp                 AS entr_regentry_subset_tp,
                           subset_regentry.regentry_id AS subset_regentry_id,
                           err.entr_id                 AS entr_reg_regentry_id,
                           err.entr_tp                 AS entr_reg_regentry_tp,
                           subset_reg.reg_id           AS subset_reg_id,
                           subset_reg.reg_ke           AS subset_reg_ke

                    FROM entr_subset_substances ess
                             INNER JOIN subset ON ess.entr_src_id = subset.subset_id
                             INNER JOIN entr_regentry_subset ers ON subset.subset_id = ers.entr_dst_id
                        AND ers.entr_tp = 'SUBSET_CONCERNED'
                             INNER JOIN regentry subset_regentry ON ers.entr_src_id = subset_regentry.regentry_id
                             INNER JOIN entr_reg_regentry err ON subset_regentry.regentry_id = err.entr_dst_id
                        AND err.entr_tp = 'REGULATION_ENTRY'
                             INNER JOIN reg subset_reg ON err.entr_src_id = subset_reg.reg_id) ess
                   ON substances.sub_id = ess.sub_id AND ess.entr_subset_substances_tp = 'IS_IN'
                          /* AND ess.subset_reg_id IN (841, 840, 1378, 842) */

         LEFT JOIN (SELECT ers.entr_id     AS entr_reg_substances_id,
                           ers.entr_src_id AS reg_id,
                           ers.entr_dst_id AS sub_id,
                           ers.entr_co     AS entr_reg_substances_co,
                           ers.entr_tp     AS entr_reg_substances_tp,
                           reg.name        AS substance_reg_name,
                           reg.reg_id      AS substance_reg_id,
                           reg.reg_desc    AS substance_reg_desc,
                           reg.info        AS substance_reg_info
                    FROM entr_reg_substances ers
                             INNER JOIN reg ON ers.entr_src_id = reg_id) ers
                   ON substances.sub_id = ers.sub_id AND ers.entr_reg_substances_tp = 'SUBSTANCES' AND
                      ers.reg_id IN (841, 840, 1378, 842)

         LEFT JOIN (SELECT ers.entr_id     AS entr_regentry_substances_id,
                           ers.entr_tp     AS entr_regentry_substances_tp,
                           ers.entr_dst_id AS sub_id,
                           ers.entr_co     AS entr_regentry_substances_co,
                           regentry_id     AS substances_regentry_id,
                           regentry.ref    AS substances_regentry_ref,
                           regentry.co     AS substances_regentry_co,
                           regentry.tp     AS substances_regentry_tp,
                           err.entr_id     AS entr_regentry_reg_id,
                           err.entr_tp     AS entr_regentry_reg_tp,
                           reg_id          AS substance_reg_id,
                           reg_ke          AS substance_reg_ke

                    FROM entr_regentry_substances ers
                             INNER JOIN regentry
                                        ON regentry_id = ers.entr_src_id
                             INNER JOIN entr_reg_regentry err ON err.entr_dst_id = regentry_id
                        AND err.entr_tp = 'REGULATION_ENTRY'
                             INNER JOIN reg ON reg_id = err.entr_src_id) ers2
                   ON substances.sub_id = ers2.sub_id AND ers2.entr_regentry_substances_tp = 'SUBSTANCE_CONCERNED'
                    /* and ers2.substance_reg_id IN (841, 840, 1378, 842) */

WHERE exists(
        SELECT 1
        FROM entr_subset_substances
                 INNER JOIN subset ON entr_src_id = subset.subset_id AND entr_tp = 'IS_IN'
            AND subset.key = 'Perfume+Component'
        WHERE entr_id = ess.entr_subset_substances_id
    )
   OR ess.entr_subset_substances_id IS NOT NULL
   OR ers2.entr_regentry_substances_id IS NOT NULL;




----------------------------------------------------------------------------------------------------------
select substances.sub_id, esp.entr_plc_plcseys_id
FROM substances
         LEFT JOIN (SELECT esp.entr_id     AS entr_substances_plc_id,
                           esp.entr_src_id AS sub_id,
                           esp.entr_tp     AS entr_substances_plc_tp,
                           plc.plc_id      AS plc_id,
                           epp.entr_id     AS entr_plc_plcseys_id,
                           epp.entr_tp     AS entr_plc_plcseys_tp,
                           pls.plcsys_id,
                           pls.plcsys_ke,
                           pls.tp          AS plcsys_tp

                    FROM entr_substances_plc esp
                             INNER JOIN plc ON esp.entr_dst_id = plc.plc_id
                             INNER JOIN entr_plc_plcsys epp
                                        ON esp.entr_dst_id = epp.entr_src_id AND
                                           epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                             INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id) esp
                   ON substances.sub_id = esp.sub_id AND
                      esp.entr_substances_plc_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')