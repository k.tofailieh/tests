
insert into de (de_id, de_tp, de_li, de_ke)
select nextval('seq_de_id'), 'ENDPT', 'Raw material incompatibility conditions', 'RAW_MATERIAL_INCOMPATIBILITY'
where not exists (select 1 from de where de_ke = 'RAW_MATERIAL_INCOMPATIBILITY');

--- Insert the distinct material_id with the Incompatibility into import.import_line with a new imp_ref ---
with distinct_raw_material as (select distinct internal_id, value20 from import.import_line
where imp_ref = 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and internal_id is not null
group by internal_id, value20)
insert into import.import_line (imp_id, internal_id, imp_ref, value20)
select nextval('import.import_line_imp_id'), internal_id, 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation', value20 from distinct_raw_material;

--- Select the link between fiche and endpoint ---
with rm_regulatory_restrictions as
(select distinct internal_id as rm_id,
    coalesce(endpt_id, -nextval('seq_endpt_id')) endpt_id,
    de_id
from import.import_line
inner join fiche f on f.m_id =  abs(internal_id)
left join entr_fiche_endpt efe on efe.entr_src_id = f.m_id and entr_tp = 'RAW_MATERIAL_INCOMPATIBILITY'
left join endpt e on e.endpt_id = efe.entr_dst_id and e.endpt_tp = 'RAW_MATERIAL_INCOMPATIBILITY'
left join der_endpt on der_endpt.der_src_id = e.endpt_id and der_tp = 'ENDPT_DESC'
left join de on de_tp = 'ENDPT' and de.de_ke = 'RAW_MATERIAL_INCOMPATIBILITY'
where imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and internal_id is not null
and and value20 is not null

)

--- update the import_line table with the rawmaterial incompatibility data ---
update import.import_line
set
 value_id1 = endpt_id, /* endpt_id in value_id1 */
  value_id2 = de_id /* de_id in value_id2 */
from rm_regulatory_restrictions
where imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and abs(internal_id) = abs(rm_id);

--- insert new endpoint containing the incompatibility if it doesn't already exist ---
insert into endpt (endpt_id, endpt_tp, val, endpt_co,ns)
select distinct abs(value_id1),
 'RAW_MATERIAL_INCOMPATIBILITY',/*fix bug column INCOMPATIBILITY*/
 /* if the formulation is too long for the val column, get the substring and stock the text in endpt_co */
  /* if the value can be filled in the val column, directly stock it in the val */
  CASE
    WHEN length(value20) > 125 THEN concat(substring(value20, 1, 125), ' ...')
    ELSE value20
  END,
  CASE
    WHEN length(value20) > 125 THEN value20
    ELSE null
  END,
  'Havea/2122581'
  from import.import_line
where imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and coalesce(value_id1, 0) < 0
and value20 is not null
;


--- insert new relation between the de and the endpoint if it doesn't already exist ---
insert into der_endpt (der_id, der_tp, der_src_id, der_dst_id)
select nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id1), value_id2 from import.import_line
where imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation'  and coalesce(value_id1, 0) < 0
group by value_id1, value_id2;

--- insert new relation between the de and the endpoint if it doesn't already exist ---
insert into entr_fiche_endpt (entr_id, entr_tp, entr_src_id, entr_dst_id)
select nextval('seq_entr_fiche_endpt_id'), 'RAW_MATERIAL_INCOMPATIBILITY', abs(internal_id), abs(value_id1) from import.import_line
where imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and coalesce(value_id1, 0) < 0
group by internal_id, value_id1;

/*update the existing endpoint linked to the RM */
update endpt e
set val = endptVal, endpt_co = endptCo
from (select
  value_id1,
  CASE
    WHEN length(value20) > 125 THEN concat(substring(value20, 1, 125), ' ...')
    ELSE value20
  END endptVal,
  CASE
    WHEN length(value20) > 125 THEN value20
    ELSE null
  END endptCo
  from import.import_line
where value20 is not null and imp_ref = 'INCOMPATIBILITY_REF' || 'ImportRawMaterial_9e20432e-1be5-4e40-8077-446d8127af54_OtherInformation' and coalesce(value_id1, 0) > 0) existing_rm
where e.endpt_id = existing_rm.value_id1

;