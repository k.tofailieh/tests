SELECT replace('khaled 23', replace(translate('khaled 23', '0123456789', '############'), '#', ''), '');

SELECT replace(translate('khaled 23', '0123456789', '############'), '#', '');

SELECT m_ref, cas, ec, marguage
FROM fiche
WHERE trim(marguage) LIKE 'F1%'
ORDER BY nullif(trim(lower(marguage)), '') NULLS LAST,
         CASE WHEN trim(marguage) LIKE 'F%' THEN trim(lower(m_ref)) ELSE cas END;
;


ORDER BY  NULLIF(marguage, '') NULLS LAST,

SELECT m_ref
FROM fiche
WHERE marguage IS NOT NULL;

SELECT c
FROM unnest(ARRAY ['GEL MAINS HYDROALCOOLIQUE - ARRETE DEROGATOIRE','BAIN DE BOUCHE PROPOLIS VERTE', 'essai 1 ','t']) chars(c)
ORDER BY lower(c);



WITH get_filters AS (SELECT DISTINCT 'tp' AS type, tp
                     FROM fiche
                     UNION ALL
                     SELECT DISTINCT 'ref' AS type, m_ref
                     FROM fiche)
SELECT *
FROM get_filters
ORDER BY get_filters.type DESC



SELECT *
FROM entr_fiche_nomclit;

SELECT translate('12121', '12', 'a')
           UPDATE import.import_line SET imp_err = TRUE, imp_err_msg = coalesce(imp_err_msg || ' | ', '') || 'cust_id  is required'
WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM'
  AND cust_id IS NULL;

WITH existingFormulas AS (SELECT m_id, m_ref, cust_id
                          FROM fiche
                          WHERE tp = 'RAW_MATERIAL'
                            AND split_part(ns, '/', 1) = 'EcoMundo'
                            AND active <> 0)
UPDATE import.import_line
SET internal_id = m_id
FROM existingFormulas
WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM'
  AND (import_line.cust_id = existingFormulas.cust_id)
  AND NOT (COALESCE(imp_err, FALSE));


WITH new_endpoints AS (SELECT internal_id, value_txt3, value_txt4, value_txt6, nextval('seq_endpt_id') new_id
                       FROM import.import_line
                       WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM'
                         AND coalesce(abs(internal_id), 0) > 0)
UPDATE import.import_line li
SET value_id5=new_endpoints.new_id /*new endpt*/
FROM new_endpoints
WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM'
  AND coalesce(imp_err, FALSE) = FALSE
  AND li.internal_id = new_endpoints.internal_id
  AND (li.value_txt3 = new_endpoints.value_txt3)
;
INSERT INTO endpt (endpt_id, val, value_max, value_unit, endpt_tp)
SELECT value_id5,
       CASE
           WHEN value_txt3 = 'WATER' THEN 'RAW_MATERIAL_WATER_SOLUBILITY'
           WHEN value_txt3 = 'ALCOHOL' THEN 'RAW_MATERIAL_ALCOHOL_SOLUBILITY'
           WHEN value_txt3 = 'OTHER' THEN 'RAW_MATERIAL_OTHER_SOLVENT_SOLUBILITY'
           END                            AS val
        ,
       cast(value_txt4 AS float),
       coalesce(value_txt6, '')           AS value_unit,
       'COSMETIC_RAW_MATERIAL_SOLUBILITY' AS endpt_tp
FROM import.import_line
WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM'
  AND coalesce(value_id5, 0) > 0
;


/*insert entr_fiche_endpt */
INSERT INTO entr_fiche_endpt(entr_id,
                             entr_src_id,
                             entr_dst_id,
                             entr_tp)
SELECT nextval('seq_entr_fiche_endpt_id'),
       internal_id,
       value_id5,
       'COSMETIC_RAW_MATERIAL_SOLUBILITY' entr_tp
FROM import.import_line
WHERE imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM' /* Current upload */
  AND coalesce(imp_err, FALSE) = FALSE /* No error */
  AND coalesce(abs(internal_id), 0) > 0
  AND coalesce(value_id5, 0) > 0
  AND NOT exists(SELECT 1
                 FROM entr_fiche_endpt efl
                 WHERE efl.entr_src_id = abs(import_line.internal_id)
                   AND efl.entr_tp = 'COSMETIC_RAW_MATERIAL_SOLUBILITY'
    )
GROUP BY internal_id, value_id5, value_txt3
;

UPDATE entr_fiche_endpt
SET entr_dst_id=abs(value_id5)
FROM import.import_line
WHERE exists(SELECT 1
             FROM entr_fiche_endpt efl
             WHERE efl.entr_src_id = abs(import_line.internal_id)
               AND entr_tp = 'COSMETIC_RAW_MATERIAL_SOLUBILITY'
    )

  AND imp_ref = 'ImportRawMaterial_f0754b27-3b8e-4c62-8225-ecc715068529_RM' /* Current upload */
  AND coalesce(imp_err, FALSE) = FALSE
  AND entr_tp = 'COSMETIC_RAW_MATERIAL_SOLUBILITY'
  AND entr_src_id = abs(import_line.internal_id)
;


/*get current ids for status*/
UPDATE import.import_line
SET value_id4=lov.lov_id
FROM lov
WHERE coalesce(value28, '') != ''
  AND coalesce(import_line.imp_err, FALSE) = FALSE
  AND lov.tp = 'RAW_MATERIAL_STATUS'
  AND trim(upper(value28)) = trim(upper(lov.ke))
  AND imp_ref = :imp_ref || '_STATUS'
;



SELECT internal_id, cust_id, value_id1, *
FROM import.import_line
WHERE cust_id = 'kha'
  AND imp_ref = :imp_ref;

ORDER BY imp_id DESC;










;




    --- Insert new De for RAW_MATERIAL_FORMULATION_CONDITIONS in the if it doesnt already exist ---
INSERT INTO de (de_id, de_tp, de_li, de_ke)
SELECT nextval('seq_de_id'), 'ENDPT', 'Raw material formulation conditions', 'RAW_MATERIAL_FORMULATION_CONDITIONS'
WHERE NOT exists(SELECT 1 FROM de WHERE de_ke = 'RAW_MATERIAL_FORMULATION_CONDITIONS');

--- Insert the distinct material_id with the Formulation into import.import_line with a new imp_ref ---
WITH distinct_raw_material AS (SELECT DISTINCT internal_id, value19
                               FROM import.import_line
                               WHERE imp_ref = :imp_ref
                                 AND internal_id IS NOT NULL
                               GROUP BY internal_id, value19)
INSERT
INTO import.import_line (imp_id, internal_id, imp_ref, value19)
SELECT nextval('import.import_line_imp_id'), internal_id, 'FORMULATION_REF' || :imp_ref, value19
FROM distinct_raw_material;

--- Select the link between fiche and endpoint ---
WITH rm_regulatory_restrictions AS
         (SELECT DISTINCT internal_id AS                               rm_id,
                          coalesce(endpt_id, -nextval('seq_endpt_id')) endpt_id,
                          de_id
          FROM import.import_line
                   INNER JOIN fiche f ON f.m_id = abs(internal_id)
                   LEFT JOIN entr_fiche_endpt efe
                             ON efe.entr_src_id = f.m_id AND entr_tp = 'RAW_MATERIAL_FORMULATION_CONDITIONS'
                   LEFT JOIN endpt e
                             ON e.endpt_id = efe.entr_dst_id AND e.endpt_tp = 'RAW_MATERIAL_FORMULATION_CONDITIONS'
                   LEFT JOIN der_endpt ON der_endpt.der_src_id = e.endpt_id AND der_tp = 'ENDPT_DESC'
                   LEFT JOIN de ON de_tp = 'ENDPT' AND de.de_ke = 'RAW_MATERIAL_FORMULATION_CONDITIONS'
          WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
            AND internal_id IS NOT NULL)

--- update the import_line table with the rawmaterial formulation data ---
UPDATE import.import_line
SET value_id1 = endpt_id, /* endpt_id in value_id1 */
    value_id2 = de_id /* de_id in value_id2 */
FROM rm_regulatory_restrictions
WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
  AND abs(internal_id) = abs(rm_id);

--- insert new endpoint containing the note if it doesn't already exist ---
INSERT INTO endpt (endpt_id, endpt_tp, val, endpt_co, ns)
SELECT DISTINCT abs(value_id1),
                'RAW_MATERIAL_FORMULATION_CONDITIONS',
    /* if the formulation is too long for the val column, get the substring and stock the text in endpt_co */
    /* if the value can be filled in the val column, directly stock it in the val */
                CASE
                    WHEN length(value19) > 125 THEN concat(substring(value19, 1, 125), ' ...')
                    ELSE value19
                    END,
                CASE
                    WHEN length(value19) > 125 THEN value19
                    ELSE NULL
                    END,
                :user_ns
FROM import.import_line
WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
  AND coalesce(value_id1, 0) < 0;

--- insert new relation between the de and the endpoint if it doesn't already exist ---
INSERT INTO der_endpt (der_id, der_tp, der_src_id, der_dst_id)
SELECT nextval('seq_der_endpt_id'), 'ENDPT_DESC', abs(value_id1), value_id2
FROM import.import_line
WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
  AND coalesce(value_id1, 0) < 0
GROUP BY value_id1, value_id2;

--- insert new relation between the de and the endpoint if it doesn't already exist ---
INSERT INTO entr_fiche_endpt (entr_id, entr_tp, entr_src_id, entr_dst_id)
SELECT nextval('seq_entr_fiche_endpt_id'), 'RAW_MATERIAL_FORMULATION_CONDITIONS', abs(internal_id), abs(value_id1)
FROM import.import_line
WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
  AND coalesce(value_id1, 0) < 0
GROUP BY internal_id, value_id1;

--- update the existing endpoint linked to the RM ---
UPDATE endpt e
SET val      = endptVal,
    endpt_co = endptCo
FROM (SELECT value_id1,
             CASE
                 WHEN length(value19) > 125 THEN concat(substring(value19, 1, 125), ' ...')
                 ELSE value19
                 END endptVal,
             CASE
                 WHEN length(value19) > 125 THEN value19
                 ELSE NULL
                 END endptCo
      FROM import.import_line
      WHERE imp_ref = 'FORMULATION_REF' || :imp_ref
        AND coalesce(value_id1, 0) > 0) existing_rm
WHERE e.endpt_id = existing_rm.value_id1;



SELECT DISTINCT entr_tp
FROM entr_inforeq_dir

SELECT DISTINCT *
FROM inforeqset


SELECT name_1, name_2
FROM part
WHERE name_2 IS NOT NULL;

SELECT entr_src_id,
       last_value(entr_id) OVER (PARTITION BY entr_src_id),
       first_value(entr_id) OVER (PARTITION BY entr_src_id)
FROM entr_fiche_nomclit;


SELECT 'khaled', substring('my_series, 01. del' FROM ',\ (.+). del');


SELECT df.der_char1,
       f.m_id,
       f.m_ref,
       f.active,
       f.ns,
       f.tp,
       df.der_src_id,
       df.der_tp
FROM fiche f
         LEFT JOIN der_fiche df
                   ON f.m_id = df.der_src_id AND df.der_tp = 'CURRENT_OFFICIAL_UFI'
         LEFT JOIN de d ON df.der_dst_id = d.de_id
WHERE der_char1 IS NOT NULL;

UPDATE der_fiche
SET der_char1 = trim(format('%s-%s-%s-%s', substring(der_char1 FROM 1 FOR 4), substring(der_char1 FROM 5 FOR 4),
                            substring(der_char1 FROM 9 FOR 4), substring(der_char1 FROM 13)))
WHERE der_tp = 'CURRENT_OFFICIAL_UFI'
  AND der_char1 SIMILAR TO '[A-Z0-9]{16}'
  AND der_char1 NOT SIMILAR TO '[A-Z]{16}|[0-9]{16}';