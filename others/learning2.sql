/* store new ids for formula*/
update import.import_line
set internal_id=abs(new_id)
where imp_ref=:imp_ref
and coalesce(new_id,0)<0
and internal_id is null
;


/*reset new_id in import_line for use it again */
update import.import_line
set new_id=null
where imp_ref=:imp_ref
and coalesce(new_id,0)<0
and coalesce(abs(internal_id),0)>0
;



/* create new process for Formula */
/*insert new ids*/
with distinct_process as (
    select value1 as formula_name ,nextval('seq_process_id') new_id
    from import.import_line
    where imp_ref = :imp_ref
    and value_id4 is null
    group by value1
)
update import.import_line
set new_id = distinct_process.new_id
from distinct_process
where imp_ref = :imp_ref
and value_id4 is null
and value1=formula_name
;
insert into process (
process_id,process_tp
)
select  new_id,'COSMETIC_MANUFACTURING_PROCESS'
from import.import_line
where imp_ref = :imp_ref
and value_id4 is null
and coalesce(abs(new_id),0)>0
group by new_id
on conflict do nothing
;


 /*add new relation between new process and formula */
insert into entr_fiche_process (
	entr_id,
	entr_src_id,
	entr_dst_id,
	entr_tp
)
SELECT
	nextval('seq_entr_fiche_process_id') as entr_id,
	abs(internal_id) ,
	abs(new_id),
	'COSMETIC_MANUFACTURING_PROCESS' as entr_tp
from import.import_line
where imp_ref = :imp_ref						/* Current upload */
and coalesce(imp_err,false) = false				/* No error */
and coalesce(abs(internal_id),0) > 0
and coalesce(abs(new_id),0) > 0
group by internal_id, new_id
on conflict do nothing
;

select DISTINCT value1,internal_id,new_id  from import.import_line
where imp_ref='ImportFormula_d6f21e03-28fb-41e0-b43b-f6f05150d5a5_Formula'
-- and coalesce(new_id,value_id4)='577907'



select value1 as name_formula,value2 as fromulaCode ,-nextval('seq_fiche_id') as newId  from import.import_line
where internal_id is null and imp_ref = :imp_ref AND NOT(COALESCE(imp_err, FALSE))
group by value1,value2



with
	new_Formula_with_new_ids as (select value1 as name_formula,value2 as fromulaCode ,-nextval('seq_fiche_id') as newId  from import.import_line
where internal_id is null and imp_ref = :imp_ref AND NOT(COALESCE(imp_err, FALSE))
group by value1,value2
)
update import.import_line
set new_Id = newId
from new_Formula_with_new_ids
where imp_ref = :imp_ref
and  import_line.value1=new_Formula_with_new_ids.name_formula
and coalesce(import_line.value2,'-')=coalesce(new_Formula_with_new_ids.fromulaCode,'-')
;


select DISTINCT imp_ref,imp_err_msg,internal_id,* from import.import_line
where cust_id='MP01475'
and value21='test storage'
order by imp_id DESC
;


delete from import.import_line
where cust_id='MP01475'
 ;
select m_ref,marguage,* from fiche where cust_id='MP01475'


;

select internal_id,* from import.import_line
where imp_ref='code8';

insert into import.import_line(imp_ref,value13)
values('code8','Abricot bio (noyaux d'' huile , vierge)');



select  * from lov where tp='RAW_MATERIAL_CERTIFICATION'

and ke='BIO'
;

;

select * from nomclit where nomclit_id=5723659;
WITH existingRAW_MATERIAL AS (SELECT m_id, m_ref, cust_id, marguage as code
                              FROM fiche
                              WHERE tp = 'RAW_MATERIAL'
                                AND split_part(ns, '/', 1) = :user_organisation
                                AND active <> 0)
UPDATE import.import_line
SET internal_id = m_id
from existingRAW_MATERIAL
WHERE imp_ref = :imp_ref
  and (import_line.cust_id = existingRAW_MATERIAL.cust_id or
       (coalesce(import_line.value_txt10, '') != '' and import_line.value_txt10 = code and import_line.value13 = m_ref) or
       (coalesce(import_line.value_txt10, '') = '' and coalesce(code, '') = '' and import_line.value13 = m_ref))
  AND NOT (COALESCE(imp_err, FALSE));