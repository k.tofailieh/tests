select
  study_id as id,
  study_name as name,
  study_xml, gridtest_metadata
  from study where test_type
                       ='TEST'
  and study_xml ilike '%"template":"true"%'


update study set gridtest_metadata = '[{"name":"Temperature","type":"primary","values":["10C","20C"]},{"name":"Other","type":"secondary","values":["UV","Noir"]},{"name":"Tertiary","type":"tertiary","values":["Ph","Color"]},{"name":"Times","type":"times","values":["t0","t0+30min"]}]'
where study_id in (228362
,229917
,229918
,230467
,230468
,228363
)


update doc set st = 'VALID' where doc_id = 804310;


select * from fiche where ns ilike '%granado%'