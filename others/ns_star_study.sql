DROP VIEW IF EXISTS ns_star.entr_study_study;
DROP VIEW IF EXISTS ns_star.entr_study_lov;
DROP VIEW IF EXISTS ns_star.entr_study_substances;
DROP VIEW IF EXISTS ns_star.entr_study_inci;
DROP VIEW IF EXISTS ns_star.entr_study_dir;
DROP VIEW IF EXISTS ns_star.entr_profile_study;
DROP VIEW IF EXISTS ns_star.entr_ns_study;
DROP VIEW IF EXISTS ns_star.entr_inci_study;
DROP VIEW IF EXISTS ns_star.entr_fiche_study;
DROP VIEW IF EXISTS ns_star.docr_study;
DROP VIEW IF EXISTS ns_star.der_study;
DROP VIEW IF EXISTS ns_star.entr_study_endpt;
DROP VIEW IF EXISTS ns_star.entr_study_part;

DROP VIEW IF EXISTS ns_star.study;


ALTER TABLE study ALTER COLUMN gridtest_data TYPE jsonb USING gridtest_data::jsonb;
ALTER TABLE study alter column gridtest_metadata TYPE jsonb USING gridtest_data::jsonb;


CREATE OR REPLACE VIEW ns_star.study AS SELECT * FROM public.study WHERE ((study.ns IS NULL) OR ((study.ns)::text ~~ ANY ( SELECT ns_star.ns_ok.ns_like FROM ns_star.ns_ok))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.der_study AS SELECT * FROM public.der_study WHERE ((der_study.der_src_id IN ( SELECT study.study_id FROM study)) AND (der_study.der_dst_id IN ( SELECT de.de_id FROM de))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.docr_study AS SELECT * FROM public.docr_study WHERE (docr_study.docr_src_id IN ( SELECT study.study_id FROM study)) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_fiche_study AS SELECT * FROM public.entr_fiche_study WHERE ((entr_fiche_study.entr_src_id IN ( SELECT fiche.m_id FROM fiche)) AND (entr_fiche_study.entr_dst_id IN ( SELECT study.study_id FROM study))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_inci_study AS SELECT * FROM anx.entr_inci_study WHERE ((entr_inci_study.entr_src_id IN ( SELECT anx.inci.inci_id FROM anx.inci)) AND (entr_inci_study.entr_dst_id IN ( SELECT study.study_id FROM study))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_ns_study AS SELECT * FROM public.entr_ns_study WHERE ((entr_ns_study.entr_src_id IN ( SELECT ns.ns_id FROM ns)) AND (entr_ns_study.entr_dst_id IN ( SELECT study.study_id FROM study))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_profile_study AS SELECT * FROM public.entr_profile_study WHERE (entr_profile_study.entr_src_id IN ( SELECT profile.profile_id FROM profile)) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_dir AS SELECT * FROM public.entr_study_dir WHERE ((entr_study_dir.entr_src_id IN ( SELECT study.study_id FROM study)) AND (entr_study_dir.entr_dst_id IN ( SELECT dir.dir_id FROM dir))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_inci AS SELECT * FROM anx.entr_study_inci WHERE ((entr_study_inci.entr_src_id IN ( SELECT study.study_id FROM study)) AND (entr_study_inci.entr_dst_id IN ( SELECT anx.inci.inci_id FROM anx.inci))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_substances AS SELECT * FROM public.entr_study_substances WHERE ((entr_study_substances.entr_src_id IN ( SELECT study.study_id FROM study)) AND (entr_study_substances.entr_dst_id IN ( SELECT substances.sub_id FROM substances))) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_endpt			AS SELECT * FROM entr_study_endpt		WHERE (entr_src_id	IN (SELECT study_id		FROM ns_star.study		)) AND (entr_dst_id	IN (SELECT endpt_id		FROM ns_star.endpt	))	AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_lov AS SELECT * FROM entr_study_lov WHERE (entr_study_lov.entr_src_id IN (SELECT study.study_id FROM ns_star.study)) AND (entr_study_lov.entr_dst_id IN (SELECT lov.lov_id FROM ns_star.lov)) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());
CREATE OR REPLACE VIEW ns_star.entr_study_study AS SELECT * FROM entr_study_study WHERE (entr_study_study.entr_src_id IN (SELECT study.study_id FROM ns_star.study)) AND (entr_study_study.entr_dst_id IN (SELECT study.study_id FROM ns_star.study)) AND (tpsfrom,tpstill) OVERLAPS (ns_star.getDbTransfertAsOfOverlapsLBound(),ns_star.getDbTransfertAsOfOverlapsUBound());


CREATE OR REPLACE view ns_star.entr_study_part(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co, entr_nt, entr_st, entr_num1, entr_num2, entr_char1, entr_char2, entr_float1, entr_float2, entr_xml, entr_dt1, entr_dt2, tpsuid, tpsfrom, tpstill, tps_md_ns, tps_md_app) as
	SELECT entr_study_part.entr_id,
    entr_study_part.entr_src_id,
    entr_study_part.entr_dst_id,
    entr_study_part.entr_tp,
    entr_study_part.entr_co,
    entr_study_part.entr_nt,
    entr_study_part.entr_st,
    entr_study_part.entr_num1,
    entr_study_part.entr_num2,
    entr_study_part.entr_char1,
    entr_study_part.entr_char2,
    entr_study_part.entr_float1,
    entr_study_part.entr_float2,
    entr_study_part.entr_xml,
    entr_study_part.entr_dt1,
    entr_study_part.entr_dt2,
    entr_study_part.tpsuid,
    entr_study_part.tpsfrom,
    entr_study_part.tpstill,
    entr_study_part.tps_md_ns,
    entr_study_part.tps_md_app
   FROM entr_study_part
  WHERE (entr_study_part.entr_src_id IN ( SELECT study.study_id
           FROM ns_star.study)) AND (entr_study_part.entr_dst_id IN ( SELECT part.part_id
           FROM ns_star.part)) AND "overlaps"(entr_study_part.tpsfrom, entr_study_part.tpstill, ns_star.getdbtransfertasofoverlapslbound(), ns_star.getdbtransfertasofoverlapsubound());


SELECT dir_id,  de.*, dd.* from dir inner join der_dir dd ON dir.dir_id = dd.der_src_id
                    inner join de on dd.der_dst_id = de.de_id
                    where dir.dir_id = 2130721;

DELETE from dir where dir_id = '2136310';

insert into dir (dir_id, dn, cn, displayname, givenname, surname, email) VALUES (nextval('seq_dir_id'),'cn=k.hennara,o=EcoMundo,dc=ecodis,dc=org', 'k.hennara', 'Khalil Hennara', 'Khalil', 'Hennara', 'k.hennara@workwithnerds.ca')


select * from study;