--INSERT INTO entr_part_nomclit(entr_id, entr_tp, entr_src_id, entr_dst_id) values (nextval('public.seq_entr_part_nomclit_id'),'NOMENCLATURE_ITEM_FOR_PART_IS',348075, 760179) RETURNING entr_id;
-- entr_id 3129399 is to delete on tadmor.

WITH RECURSIVE
    fiche_root AS
        (SELECT efn.entr_src_id AS root_id, efn.entr_src_id AS parent_id, 1 AS deep, nomclit_id, fiche.m_id, m_ref, tp
         FROM entr_fiche_nomclit efn
                  INNER JOIN nomclit ON efn.entr_dst_id = nomclit.nomclit_id
                  INNER JOIN entr_nomclit_fiche enf ON nomclit.nomclit_id = enf.entr_src_id
                  INNER JOIN fiche ON m_id = enf.entr_dst_id
         WHERE efn.entr_src_id = 44075

         UNION ALL
         SELECT fiche_root.root_id,
                efn.entr_src_id,
                fiche_root.deep + 1,
                n.nomclit_id,
                child.m_id,
                child.m_ref,
                child.tp
         FROM fiche_root
                  INNER JOIN entr_fiche_nomclit efn ON entr_src_id = fiche_root.m_id
                  INNER JOIN nomclit n ON n.nomclit_id = efn.entr_dst_id
                  INNER JOIN entr_nomclit_fiche enf ON enf.entr_src_id = n.nomclit_id
                  INNER JOIN fiche child ON enf.entr_dst_id = child.m_id),

    parent AS (SELECT epn.entr_src_id   AS bill_id,
                      cast(0 AS bigint) AS parent_id,
                      epn.entr_tp,
                      epn.entr_dst_id   AS nomclit_id,
                      enp.entr_tp,
                      enf.entr_tp,
                      1                 AS deep,
                      enf.fiche_id,
                      part.*
               FROM entr_part_nomclit epn
                        LEFT JOIN entr_nomclit_part enp ON epn.entr_dst_id = enp.entr_src_id

                        LEFT JOIN part ON enp.entr_dst_id = part.part_id
                        LEFT JOIN (SELECT enf.entr_dst_id AS fiche_id, enf.entr_src_id AS nomclit_id, enf.entr_tp
                                   FROM entr_nomclit_fiche enf) enf ON epn.entr_dst_id = enf.nomclit_id
               WHERE epn.entr_src_id = 449958 and epn.entr_tp IN ('DIRECT_COMPO_PART', 'DIRECT_COMPO_FICHE')


               UNION
               SELECT parent.bill_id,
                      parent.part_id  AS parent_id,
                      epn.entr_tp,
                      epn.entr_dst_id AS nomclit_id,
                      enp.entr_tp,
                      enf.entr_tp,
                      parent.deep + 1 AS deep,
                      enf.fiche_id,
                      child.*
               FROM parent
                        INNER JOIN entr_part_nomclit epn ON parent.part_id = epn.entr_src_id AND
                                                            epn.entr_tp IN ('DIRECT_COMPO_PART', 'DIRECT_COMPO_FICHE')
                        LEFT JOIN entr_nomclit_part enp ON epn.entr_dst_id = enp.entr_src_id
                        LEFT JOIN (SELECT enf.entr_dst_id AS fiche_id, enf.entr_src_id AS nomclit_id, enf.entr_tp
                                   FROM entr_nomclit_fiche enf) enf ON epn.entr_dst_id = enf.nomclit_id

                        LEFT JOIN part child ON child.part_id = enp.entr_dst_id),
    get_parts AS (SELECT *, count(coalesce(fiche_id, part_id)) OVER (PARTITION BY nomclit_id, bill_id) AS children_cnt
                  FROM parent)
SELECT  *
FROM get_parts
ORDER BY bill_id
;



WITH RECURSIVE fiche_root AS
                   (SELECT efn.entr_src_id AS fiche_id,
                           efn.entr_src_id AS parent_id,
                           1               AS deep,
                           nomclit_id,
                           fiche.m_id,
                           m_ref,
                           tp
                    FROM entr_fiche_nomclit efn
                             INNER JOIN nomclit ON efn.entr_dst_id = nomclit.nomclit_id
                             INNER JOIN entr_nomclit_fiche enf ON nomclit.nomclit_id = enf.entr_src_id
                             INNER JOIN fiche ON m_id = enf.entr_dst_id
                    WHERE efn.entr_src_id = 202262
                    UNION ALL
                    SELECT fiche_root.fiche_id,
                           efn.entr_src_id,
                           fiche_root.deep + 1,
                           n.nomclit_id,
                           child.m_id,
                           child.m_ref,
                           child.tp
                    FROM fiche_root
                             INNER JOIN entr_fiche_nomclit efn ON entr_src_id = fiche_root.m_id
                             INNER JOIN nomclit n ON n.nomclit_id = efn.entr_dst_id
                             INNER JOIN entr_nomclit_fiche enf ON enf.entr_src_id = n.nomclit_id
                             INNER JOIN fiche child ON enf.entr_dst_id = child.m_id)
SELECT max(deep)
FROM fiche_root;


epn.entr_src_id, enp.entr_src_id, enf.*
FROM entr_part_nomclit epn
         LEFT JOIN entr_nomclit_part enp ON epn.entr_dst_id = enp.entr_src_id

         LEFT JOIN part ON enp.entr_dst_id = part.part_id
         LEFT JOIN (SELECT enf.entr_src_id AS nomclit_id_1,
                           fiche_root.*
                    FROM entr_nomclit_fiche enf
                             LEFT JOIN fiche_root ON enf.entr_dst_id = fiche_root.fiche_id) enf
                   ON epn.entr_dst_id = enf.nomclit_id_1
WHERE fiche_id IS NOT NULL;



SELECT *
FROM entr_fiche_nomclit efn
         INNER JOIN nomclit ON efn.entr_dst_id = nomclit.nomclit_id
         INNER JOIN entr_nomclit_fiche enf ON nomclit.nomclit_id = enf.entr_src_id
         INNER JOIN fiche ON m_id = enf.entr_dst_id


SELECT *
FROM entr_part_nomclit epn
         INNER JOIN entr_nomclit_fiche enf ON epn.entr_dst_id = enf.entr_src_id;


SELECT name_1
FROM part

SELECT column_name
FROM information_schema.columns
WHERE table_name = 'part'











