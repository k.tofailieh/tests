--INSERT INTO entr_part_nomclit(entr_id, entr_tp, entr_src_id, entr_dst_id) values (nextval('public.seq_entr_part_nomclit_id'),'NOMENCLATURE_ITEM_FOR_PART_IS',348075, 760179) RETURNING entr_id;
-- entr_id 3129399 is to delete on tadmor.

with recursive parent as (
    select epn.entr_src_id as bill_id, cast(0 as bigint) as parent_id, epn.entr_dst_id as nomclit_id, 1 as deep, part.*
    from entr_part_nomclit epn
             join entr_nomclit_part enp on epn.entr_dst_id = enp.entr_src_id
             join part on enp.entr_dst_id = part.part_id
    where epn.entr_src_id = 349171
    union all
    select parent.bill_id, parent.part_id as parent_id, epn.entr_dst_id as nomclit_id, parent.deep + 1 as deep, child.*
    from parent
             join entr_part_nomclit epn on parent.part_id = epn.entr_src_id
             join entr_nomclit_part enp on epn.entr_dst_id = enp.entr_src_id
             join part child on child.part_id = enp.entr_dst_id
)
SELECT inforeqset_id,
       inforeqset_st,
       inforeqset_cmt,
       parent.*
FROM inforeqset
         INNER JOIN entr_inforeqset_inforeq eii ON inforeqset.inforeqset_id = eii.entr_src_id
         INNER JOIN entr_inforeq_part eip ON eii.entr_dst_id = eip.entr_src_id
         INNER JOIN parent ON parent.part_id = eip.entr_dst_id
where inforeqset_id = 25
order by bill_id, parent.part_id;


with recursive parent as
    (
        select efn.entr_src_id as bill_id, efn.entr_src_id as parent_id, 1 as deep,  nomclit_id, fiche.m_id, m_ref, tp
            from  entr_fiche_nomclit efn
               inner join nomclit on efn.entr_dst_id = nomclit.nomclit_id
               inner join entr_nomclit_fiche enf on nomclit.nomclit_id = enf.entr_src_id
               inner join fiche on m_id = enf.entr_dst_id
            where efn.entr_src_id = 44075

        union all
        select parent.bill_id, efn.entr_src_id, parent.deep +1, n.nomclit_id, child.m_id, child.m_ref, child.tp from parent
               inner join entr_fiche_nomclit efn on entr_src_id = parent.m_id
               inner join nomclit n on n.nomclit_id = efn.entr_dst_id
               inner join entr_nomclit_fiche enf on enf.entr_src_id = n.nomclit_id
               inner join fiche child on enf.entr_dst_id = child.m_id
    )
select max(deep) from parent;


select * from
             entr_fiche_nomclit efn
               inner join nomclit on efn.entr_dst_id = nomclit.nomclit_id
               inner join entr_nomclit_fiche enf on nomclit.nomclit_id = enf.entr_src_id
               inner join fiche on m_id = enf.entr_dst_id

