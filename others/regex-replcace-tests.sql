/* Clean INCI-Names and but the cleaned values in value35 */
/* Delete All Words Between Parthness And Manage The Double Spaces. */
update import.import_line
set value35 = trim(upper(regexp_replace(regexp_replace(value10, '\(([^)(]*)\)', '', 'g'), '(\s+)', ' ', 'g')))
where imp_ref = :imp_ref
  and not coalesce(imp_err, false)
  and coalesce(value10, '') != ''
;

SELECT regexp_replace('(){}[]<>,.!?/\*+-=:;@#$&%~', '__________________________', '_')

select translate(' _?>?>', '(){}[]<>,.!?/\*+-=:;@#$&%~', '__________________________')


select * from anx.inci where upper(inciname) like 'CI 77266%'