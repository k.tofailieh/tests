SELECT
    /* substances */
    substances.sub_id,
    naml,
    cas,

    /* entr_substances_plc */
    esp.entr_substances_plc_id,
    esp.entr_substances_plc_tp,
    esp.plc_id,
    esp.entr_plc_plcseys_id,
    esp.entr_plc_plcseys_tp,
    esp.plcsys_id,
    esp.plcsys_ke,
    esp.plcsys_tp,

    /* entr_substances_subset */
    ess.entr_subset_substances_id,
    ess.entr_subset_substances_tp,
    ess.subset_id,
    ess.entr_regentry_subset_id,
    ess.entr_regentry_subset_tp,
    ess.subset_regentry_id,
    ess.entr_reg_regentry_id,
    ess.entr_reg_regentry_tp,
    ess.subset_reg_id,
    ess.subset_reg_ke,

    /* entr_reg_substances */
    ers.entr_reg_substances_id,
    ers.entr_reg_substances_tp,
    ers.entr_reg_substances_co,
    ers.substance_reg_id,
    ers.substance_reg_desc,
    ers.substance_reg_info,
    ers.substance_reg_name,

    /* entr_regentry_substances */
    ers2.entr_regentry_substances_id,
    ers2.entr_regentry_substances_tp,
    ers2.entr_regentry_substances_co,
    ers2.substances_regentry_id,
    ers2.substances_regentry_ref,
    ers2.substances_regentry_co,
    ers2.substances_regentry_tp,
    ers2.entr_regentry_reg_id,
    ers2.entr_regentry_reg_tp,
    ers2.substance_reg_id,
    ers2.substance_reg_ke
FROM substances
         LEFT JOIN (SELECT esp.entr_id     AS entr_substances_plc_id,
                           esp.entr_src_id AS sub_id,
                           esp.entr_tp     AS entr_substances_plc_tp,
                           plc.plc_id      AS plc_id,
                           epp.entr_id     AS entr_plc_plcseys_id,
                           epp.entr_tp     AS entr_plc_plcseys_tp,
                           pls.plcsys_id,
                           pls.plcsys_ke,
                           pls.tp          AS plcsys_tp

                    FROM entr_substances_plc esp
                             INNER JOIN plc ON esp.entr_dst_id = plc.plc_id
                             INNER JOIN entr_plc_plcsys epp
                                        ON esp.entr_dst_id = epp.entr_src_id AND
                                           epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                             INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id) esp
                   ON substances.sub_id = esp.sub_id AND
                      esp.entr_substances_plc_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')

         LEFT JOIN (SELECT ess.entr_dst_id             AS sub_id,
                           ess.entr_id                 AS entr_subset_substances_id,
                           ess.entr_tp                 AS entr_subset_substances_tp,
                           subset.subset_id,
                           ers.entr_id                 AS entr_regentry_subset_id,
                           ers.entr_tp                 AS entr_regentry_subset_tp,
                           subset_regentry.regentry_id AS subset_regentry_id,
                           err.entr_id                 AS entr_reg_regentry_id,
                           err.entr_tp                 AS entr_reg_regentry_tp,
                           subset_reg.reg_id           AS subset_reg_id,
                           subset_reg.reg_ke           AS subset_reg_ke

                    FROM entr_subset_substances ess
                             INNER JOIN subset ON ess.entr_src_id = subset.subset_id
                             INNER JOIN entr_regentry_subset ers ON subset.subset_id = ers.entr_dst_id
                        AND ers.entr_tp = 'SUBSET_CONCERNED'
                             INNER JOIN regentry subset_regentry ON ers.entr_src_id = subset_regentry.regentry_id
                             INNER JOIN entr_reg_regentry err ON subset_regentry.regentry_id = err.entr_dst_id
                        AND err.entr_tp = 'REGULATION_ENTRY'
                             INNER JOIN reg subset_reg ON err.entr_src_id = subset_reg.reg_id) ess
                   ON substances.sub_id = ess.sub_id AND ess.entr_subset_substances_tp = 'IS_IN'
    /* AND ess.subset_reg_id IN (841, 840, 1378, 842) */

         LEFT JOIN (SELECT ers.entr_id     AS entr_reg_substances_id,
                           ers.entr_src_id AS reg_id,
                           ers.entr_dst_id AS sub_id,
                           ers.entr_co     AS entr_reg_substances_co,
                           ers.entr_tp     AS entr_reg_substances_tp,
                           reg.name        AS substance_reg_name,
                           reg.reg_id      AS substance_reg_id,
                           reg.reg_desc    AS substance_reg_desc,
                           reg.info        AS substance_reg_info
                    FROM entr_reg_substances ers
                             INNER JOIN reg ON ers.entr_src_id = reg_id) ers
                   ON substances.sub_id = ers.sub_id AND ers.entr_reg_substances_tp = 'SUBSTANCES' AND
                      ers.reg_id IN (841, 840, 1378, 842)

         LEFT JOIN (SELECT ers.entr_id     AS entr_regentry_substances_id,
                           ers.entr_tp     AS entr_regentry_substances_tp,
                           ers.entr_dst_id AS sub_id,
                           ers.entr_co     AS entr_regentry_substances_co,
                           regentry_id     AS substances_regentry_id,
                           regentry.ref    AS substances_regentry_ref,
                           regentry.co     AS substances_regentry_co,
                           regentry.tp     AS substances_regentry_tp,
                           err.entr_id     AS entr_regentry_reg_id,
                           err.entr_tp     AS entr_regentry_reg_tp,
                           reg_id          AS substance_reg_id,
                           reg_ke          AS substance_reg_ke

                    FROM entr_regentry_substances ers
                             INNER JOIN regentry
                                        ON regentry_id = ers.entr_src_id
                             INNER JOIN entr_reg_regentry err ON err.entr_dst_id = regentry_id
                        AND err.entr_tp = 'REGULATION_ENTRY'
                             INNER JOIN reg ON reg_id = err.entr_src_id) ers2
                   ON substances.sub_id = ers2.sub_id AND ers2.entr_regentry_substances_tp = 'SUBSTANCE_CONCERNED'
    /* and ers2.substance_reg_id IN (841, 840, 1378, 842) */

WHERE exists(
        SELECT 1
        FROM entr_subset_substances
                 INNER JOIN subset ON entr_src_id = subset.subset_id AND entr_tp = 'IS_IN'
            AND subset.key = 'Perfume+Component'
        WHERE entr_id = ess.entr_subset_substances_id
    )
   OR ess.entr_subset_substances_id IS NOT NULL
   OR ers2.entr_regentry_substances_id IS NOT NULL;


------------------------------------------------------------------------------------------------------------------------

WITH original_query AS (SELECT
                            /* substances */
                            substances.sub_id,
                            naml,
                            cas,

                            /* entr_substances_plc */
                            esp.entr_substances_plc_id,
                            esp.entr_substances_plc_tp,
                            esp.plc_id,
                            esp.entr_plc_plcseys_id,
                            esp.entr_plc_plcseys_tp,
                            esp.plcsys_id,
                            esp.plcsys_ke,
                            esp.plcsys_tp,
                            string_agg()

                            /* entr_substances_subset */
                            ess.entr_subset_substances_id, ess.entr_subset_substances_tp,
                            ess.subset_id,
                            ess.entr_regentry_subset_id,
                            ess.entr_regentry_subset_tp,
                            ess.subset_regentry_id,
                            ess.entr_reg_regentry_id,
                            ess.entr_reg_regentry_tp,
                            ess.subset_reg_id,
                            ess.subset_reg_ke,

                            /* entr_reg_substances */
                            ers.entr_reg_substances_id,
                            ers.entr_reg_substances_tp,
                            ers.entr_reg_substances_co,
                            ers.substance_reg_id,
                            ers.substance_reg_desc,
                            ers.substance_reg_info,
                            ers.substance_reg_name,

                            /* entr_regentry_substances */
                            ers2.entr_regentry_substances_id,
                            ers2.entr_regentry_substances_tp,
                            ers2.entr_regentry_substances_co,
                            ers2.substances_regentry_id,
                            ers2.substances_regentry_ref,
                            ers2.substances_regentry_co,
                            ers2.substances_regentry_tp,
                            ers2.entr_regentry_reg_id,
                            ers2.entr_regentry_reg_tp,
                            ers2.substance_reg_id,
                            ers2.substance_reg_ke,

                        FROM substances
                                 LEFT JOIN (SELECT esp.entr_id     AS entr_substances_plc_id,
                                                   esp.entr_src_id AS sub_id,
                                                   esp.entr_tp     AS entr_substances_plc_tp,
                                                   plc.plc_id      AS plc_id,
                                                   epp.entr_id     AS entr_plc_plcseys_id,
                                                   epp.entr_tp     AS entr_plc_plcseys_tp,
                                                   pls.plcsys_id,
                                                   pls.plcsys_ke,
                                                   pls.tp          AS plcsys_tp

                                            FROM entr_substances_plc esp
                                                     INNER JOIN plc ON esp.entr_dst_id = plc.plc_id
                                                     INNER JOIN entr_plc_plcsys epp
                                                                ON esp.entr_dst_id = epp.entr_src_id AND
                                                                   epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                                                     INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id) esp
                                           ON substances.sub_id = esp.sub_id AND
                                              esp.entr_substances_plc_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')

                                 LEFT JOIN (SELECT ess.entr_dst_id             AS sub_id,
                                                   ess.entr_id                 AS entr_subset_substances_id,
                                                   ess.entr_tp                 AS entr_subset_substances_tp,
                                                   subset.subset_id,
                                                   ers.entr_id                 AS entr_regentry_subset_id,
                                                   ers.entr_tp                 AS entr_regentry_subset_tp,
                                                   subset_regentry.regentry_id AS subset_regentry_id,
                                                   err.entr_id                 AS entr_reg_regentry_id,
                                                   err.entr_tp                 AS entr_reg_regentry_tp,
                                                   subset_reg.reg_id           AS subset_reg_id,
                                                   subset_reg.reg_ke           AS subset_reg_ke

                                            FROM entr_subset_substances ess
                                                     INNER JOIN subset ON ess.entr_src_id = subset.subset_id
                                                     INNER JOIN entr_regentry_subset ers
                                                                ON subset.subset_id = ers.entr_dst_id
                                                                    AND ers.entr_tp = 'SUBSET_CONCERNED'
                                                     INNER JOIN regentry subset_regentry
                                                                ON ers.entr_src_id = subset_regentry.regentry_id
                                                     INNER JOIN entr_reg_regentry err
                                                                ON subset_regentry.regentry_id = err.entr_dst_id
                                                                    AND err.entr_tp = 'REGULATION_ENTRY'
                                                     INNER JOIN reg subset_reg ON err.entr_src_id = subset_reg.reg_id) ess
                                           ON substances.sub_id = ess.sub_id AND ess.entr_subset_substances_tp = 'IS_IN'
                            /* AND ess.subset_reg_id IN (841, 840, 1378, 842) */

                                 LEFT JOIN (SELECT ers.entr_id     AS entr_reg_substances_id,
                                                   ers.entr_src_id AS reg_id,
                                                   ers.entr_dst_id AS sub_id,
                                                   ers.entr_co     AS entr_reg_substances_co,
                                                   ers.entr_tp     AS entr_reg_substances_tp,
                                                   reg.name        AS substance_reg_name,
                                                   reg.reg_id      AS substance_reg_id,
                                                   reg.reg_desc    AS substance_reg_desc,
                                                   reg.info        AS substance_reg_info
                                            FROM entr_reg_substances ers
                                                     INNER JOIN reg ON ers.entr_src_id = reg_id) ers
                                           ON substances.sub_id = ers.sub_id AND
                                              ers.entr_reg_substances_tp = 'SUBSTANCES' AND
                                              ers.reg_id IN (841, 840, 1378, 842)

                                 LEFT JOIN (SELECT ers.entr_id     AS entr_regentry_substances_id,
                                                   ers.entr_tp     AS entr_regentry_substances_tp,
                                                   ers.entr_dst_id AS sub_id,
                                                   ers.entr_co     AS entr_regentry_substances_co,
                                                   regentry_id     AS substances_regentry_id,
                                                   regentry.ref    AS substances_regentry_ref,
                                                   regentry.co     AS substances_regentry_co,
                                                   regentry.tp     AS substances_regentry_tp,
                                                   err.entr_id     AS entr_regentry_reg_id,
                                                   err.entr_tp     AS entr_regentry_reg_tp,
                                                   reg_id          AS substance_reg_id,
                                                   reg_ke          AS substance_reg_ke

                                            FROM entr_regentry_substances ers
                                                     INNER JOIN regentry
                                                                ON regentry_id = ers.entr_src_id
                                                     INNER JOIN entr_reg_regentry err ON err.entr_dst_id = regentry_id
                                                AND err.entr_tp = 'REGULATION_ENTRY'
                                                     INNER JOIN reg ON reg_id = err.entr_src_id) ers2
                                           ON substances.sub_id = ers2.sub_id AND
                                              ers2.entr_regentry_substances_tp = 'SUBSTANCE_CONCERNED'
                            /* and ers2.substance_reg_id IN (841, 840, 1378, 842) */

                        WHERE exists(
                                SELECT 1
                                FROM entr_subset_substances
                                         INNER JOIN subset ON entr_src_id = subset.subset_id AND entr_tp = 'IS_IN'
                                    AND subset.key = 'Perfume+Component'
                                WHERE entr_id = ess.entr_subset_substances_id
                            )
                           OR ess.entr_subset_substances_id IS NOT NULL
                           OR ers2.entr_regentry_substances_id IS NOT NULL)
SELECT *
FROM original_query;


with original_result as (
SELECT esp.entr_id     AS entr_substance_plc_id,
       esp.entr_tp     AS entr_substances_plc_tp,
       esp.entr_dst_id AS plc_id,
       esp.entr_src_id AS sub_id,
       epp.*
FROM entr_substances_plc esp

         INNER JOIN (SELECT epp.entr_src_id AS plc_id,
                            epp.entr_id     AS entr_plc_plcsys_id,
                            epp.entr_tp     AS entr_plc_plcsys_tp,
                            pls.plcsys_id   AS src_plcsys_id,
                            pls.tp          AS src_plcsys_tp,
                            pls.plcsys_ke   AS src_plcsys_ke,

                            epp1.*
                     FROM entr_plc_plcsys epp
                              INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id
                              LEFT JOIN (SELECT entr_id        entr_plcsys_plcsys_id,
                                                entr_tp        entr_plcsys_plcsys_tp,
                                                plcsys_id      dst_plcsys_id,
                                                plcsys_ke      dst_plcsys_ke,
                                                tp             dst_plcsys_tp
                                         FROM entr_plcsys_plcsys
                                                  INNER JOIN plcsys ON entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id) epp1
                                        ON epp1.src_plcsys_id = pls.plcsys_id
                     WHERE epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')) epp
                    ON esp.entr_dst_id = epp.plc_id
WHERE esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H'))
    select string_agg() over (PARTITION BY src_plcsys_id)
from original_result