-- entr_substances_plc
SELECT substances.sub_id, esp.*
FROM substances
         LEFT JOIN (SELECT string_agg('{entr_id: ' || esp.entr_id || ', entr_tp: ' ||
                                      coalesce(esp.entr_tp, '')
                                          || ', dst: {' || 'plc_id: ' || esp.entr_dst_id || ', entr_plc_plcsys:[' ||
                                      epp.entr_plc_plcsys_oblject || ']}'
                                          || '}', ',') AS entr_substances_plc,
                           esp.entr_src_id             AS sub_id
                    FROM entr_substances_plc esp
                             INNER JOIN (SELECT string_agg('{entr_id:' || epp.entr_id || ', entr_tp:' ||
                                                           coalesce(epp.entr_tp, '') || ', dst: {' || 'plcsys_id:' ||
                                                           pls.plcsys_id ||
                                                           ', tp: ' || pls.tp || ',plcsys_ke:' || pls.plcsys_ke ||
                                                           ',entr_plcsys_plcsys:[' || epp1.entr_plcsys_plcsys_object ||
                                                           ']}}',
                                                           ', ')   entr_plc_plcsys_oblject,
                                                epp.entr_src_id AS plc_id
                                         FROM entr_plc_plcsys epp
                                                  INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id
                                                  LEFT JOIN (SELECT string_agg('{entr_id:' || entr_id ||
                                                                               ', entr_tp:' || entr_tp || ', dst:{' ||
                                                                               'plcsys_id:' || plcsys_id ||
                                                                               ', plcsys_ke:' || plcsys_ke || ', tp:' ||
                                                                               tp || '}}',
                                                                               ', ') AS entr_plcsys_plcsys_object,
                                                                    entr_src_id      AS plcsys_id
                                                             FROM entr_plcsys_plcsys
                                                                      INNER JOIN plcsys ON entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id
                                                             GROUP BY entr_src_id) epp1
                                                            ON epp1.plcsys_id = pls.plcsys_id
                                         WHERE epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                                         GROUP BY epp.entr_src_id) epp
                                        ON esp.entr_dst_id = epp.plc_id
                    WHERE esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                    GROUP BY esp.entr_tp, esp.entr_src_id, esp.entr_id) esp
                   ON substances.sub_id = esp.sub_id


-------------------------------------------------------------------------------------

SELECT substances.sub_id, ess.entr_subset_substances_object
FROM substances
         LEFT JOIN (SELECT ess.entr_dst_id AS sub_id,
                           ess.entr_src_id,
                           string_agg('{entr_id:' || ess.entr_id || ', entr_tp:' || ess.entr_tp || ', dst: {' ||
                                      'subset_id:' || ess.entr_src_id || '}}', ', ') as entr_subset_substances_object,
                            ers.entr_regentry_subset_object,
                            err.entr_reg_regentry_object
                    FROM entr_subset_substances ess
                             INNER JOIN (SELECT entr_dst_id,
                                                ers.entr_src_id,
                                                string_agg('{entr_id:' || ers.entr_id ||
                                                           ', entr_tp:' || ers.entr_tp || ', dst: {' ||
                                                           'regentry_id:' || ers.entr_src_id || '}}', ', ') as entr_regentry_subset_object
                                         FROM entr_regentry_subset ers
                                         WHERE ers.entr_tp = 'SUBSET_CONCERNED'
                                         GROUP BY entr_dst_id, ers.entr_src_id) ers
                                        ON ess.entr_src_id = ers.entr_dst_id

                             INNER JOIN (SELECT err.entr_dst_id,
                                                string_agg('{entr_id:' || err.entr_id || ', entr_tp:' || err.entr_tp ||
                                                           ', dst:{' || 'reg_id:' || reg_id || ', reg_ke:' || reg_ke ||
                                                           '}}', ', ') as entr_reg_regentry_object
                                         FROM entr_reg_regentry err
                                                  INNER JOIN reg
                                                             ON err.entr_src_id = reg_id
                                         WHERE err.entr_tp = 'REGULATION_ENTRY'
                                         GROUP BY entr_dst_id) err
                                        ON ers.entr_src_id = err.entr_dst_id
                    WHERE ess.entr_tp = 'IS_IN'
                    GROUP BY ess.entr_dst_id, ess.entr_src_id, ers.entr_regentry_subset_object, err.entr_reg_regentry_object) ess
                   ON substances.sub_id = ess.sub_id
/* AND ess.subset_reg_id IN (841, 840, 1378, 842) */


-- ers.entr_id                 AS entr_regentry_subset_id,
-- ers.entr_tp                 AS entr_regentry_subset_tp,
-- subset_regentry.regentry_id AS subset_regentry_id,
-- err.entr_id                 AS entr_reg_regentry_id,
-- err.entr_tp                 AS entr_reg_regentry_tp,
-- subset_reg.reg_id           AS subset_reg_id,
-- subset_reg.reg_ke           AS subset_reg_ke



-- entr_subset_substances: [{entr_id:22973, entr_tp:IS_IN, dst: {subset_id:3833}}],
-- entr_subset_substances: [{entr_id:22973, entr_tp:IS_IN, dst: {subset_id:3833}}],
-- entr_subset_substances: [{entr_id:22973, entr_tp:IS_IN, dst: {subset_id:3833}}],
-- entr_subset_substances: [{entr_id:22973, entr_tp:IS_IN, dst: {subset_id:3833}}]


select * from entr_subset_substances where entr_id = 22973


select * from import.import_line order by imp_id DESC


select entr_tp, dir.* from entr_fiche_dir
inner join dir on entr_fiche_dir.entr_dst_id = dir.dir_id
where entr_src_id = 598173

select * from society where displayname = 'KhaledIIIII'


select entr_tp, lov.* from entr_fiche_lov
               inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id where entr_src_id = 246763;


with brand_to_be_inserted as (select lov_id as brand_id from import.import_line left join lov on  trim(upper(value2)) = trim(upper(ke)) where imp_ref = :imp_ref and not coalesce(imp_err, false));