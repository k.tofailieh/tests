-- entr_substances_plc
SELECT substances.sub_id, esp.*
FROM substances
         LEFT JOIN (SELECT string_agg('{entr_id: ' || esp.entr_id || ', entr_tp: ' ||
                                      coalesce(esp.entr_tp, '')
                                          || ', dst: {' || 'plc_id: ' || esp.entr_dst_id || ', entr_plc_plcsys:[' ||
                                      epp.entr_plc_plcsys_oblject || ']}'
                                          || '}', ',') AS entr_substances_plc,
                           esp.entr_src_id             AS sub_id
                    FROM entr_substances_plc esp
                             INNER JOIN (SELECT string_agg('{entr_id:' || epp.entr_id || ', entr_tp:' ||
                                                           coalesce(epp.entr_tp, '') || ', dst: {' || 'plcsys_id:' ||
                                                           pls.plcsys_id ||
                                                           ', tp: ' || pls.tp || ',plcsys_ke:' || pls.plcsys_ke ||
                                                           ',entr_plcsys_plcsys:[' || epp1.entr_plcsys_plcsys_object ||
                                                           ']}}',
                                                           ', ')   entr_plc_plcsys_oblject,
                                                epp.entr_src_id AS plc_id
                                         FROM entr_plc_plcsys epp
                                                  INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id
                                                  LEFT JOIN (SELECT string_agg('{entr_id:' || entr_id ||
                                                                               ', entr_tp:' || entr_tp || ', dst:{' ||
                                                                               'plcsys_id:' || plcsys_id ||
                                                                               ', plcsys_ke:' || plcsys_ke || ', tp:' ||
                                                                               tp || '}}',
                                                                               ', ') AS entr_plcsys_plcsys_object,
                                                                    entr_src_id      AS plcsys_id
                                                             FROM entr_plcsys_plcsys
                                                                      INNER JOIN plcsys ON entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id
                                                             GROUP BY entr_src_id) epp1
                                                            ON epp1.plcsys_id = pls.plcsys_id
                                         WHERE epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                                         GROUP BY epp.entr_src_id) epp
                                        ON esp.entr_dst_id = epp.plc_id
                    WHERE esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                    GROUP BY esp.entr_tp, esp.entr_src_id) esp
                   ON substances.sub_id = esp.sub_id


-------------------------------------------------------------------------------------

SELECT substances.sub_id, ess.entr_subset_substances_object
FROM substances
         LEFT JOIN (SELECT ess.entr_dst_id                                           AS sub_id,
                           string_agg('{entr_id:' || ess.entr_id || ', entr_tp:' || ess.entr_tp || ', dst: {' ||
                                      'subset_id:' || ess.entr_src_id || '}' || ', entr_regentry_subset:[' ||
                                      ers.entr_regentry_subset_object || ']}', ', ') AS entr_subset_substances_object
                    FROM entr_subset_substances ess
                             INNER JOIN (SELECT ers.entr_dst_id,
                                                string_agg('{entr_id:' || ers.entr_id ||
                                                           ', entr_tp:' || ers.entr_tp || ', dst: {' ||
                                                           'regentry_id:' || ers.entr_src_id || '}' ||
                                                           ', entr_reg_regentry:[' || err.entr_reg_regentry_object ||
                                                           ']}',
                                                           ', ') AS entr_regentry_subset_object
                                         FROM entr_regentry_subset ers
                                                  INNER JOIN (SELECT err.entr_dst_id,
                                                                     string_agg('{entr_id:' || err.entr_id ||
                                                                                ', entr_tp:' || err.entr_tp ||
                                                                                ', dst:{' || 'reg_id:' || reg_id ||
                                                                                ', reg_ke:' || reg_ke ||
                                                                                '}}', ', ') AS entr_reg_regentry_object
                                                              FROM entr_reg_regentry err
                                                                       INNER JOIN reg
                                                                                  ON err.entr_src_id = reg_id
                                                              WHERE err.entr_tp = 'REGULATION_ENTRY'
                                                              GROUP BY entr_dst_id) err
                                                             ON ers.entr_src_id = err.entr_dst_id
                                         WHERE ers.entr_tp = 'SUBSET_CONCERNED'
                                         GROUP BY ers.entr_dst_id) ers
                                        ON ess.entr_src_id = ers.entr_dst_id
                    WHERE ess.entr_tp = 'IS_IN'
                    GROUP BY ess.entr_dst_id) ess
                   ON substances.sub_id = ess.sub_id;


---------------------------------------------------------------------

SELECT substances.sub_id, ers.*
FROM substances
         LEFT JOIN (SELECT ers.entr_dst_id AS sub_id,
                           string_agg('{entr_id:' || ers.entr_id || ', entr_tp:' || ers.entr_tp || ', entr_co:' ||
                                      ers.entr_co || ', src:{' || 'reg_id:' || reg_id || ', reg_desc:' ||
                                      reg.reg_desc || ', info: ' || reg.info || ', name:' || reg.name || '}}', ', ')
                    FROM entr_reg_substances ers
                             INNER JOIN reg ON ers.entr_src_id = reg_id
                    WHERE ers.entr_tp = 'SUBSTANCES'
                      AND ers.entr_src_id IN (841, 840, 1378, 842)
                    GROUP BY ers.entr_dst_id) ers
                   ON substances.sub_id = ers.sub_id;


-----------------------------------------------------


SELECT substances.sub_id, ers2.*
FROM substances
         LEFT JOIN (SELECT ers.entr_dst_id AS sub_id,
                           string_agg('{entr_id:' || ers.entr_id || ', entr_tp:' || ers.entr_tp || ', src:' ||
                                      '{regentry_id:' || regentry.regentry_id || ', regentry_tp:' || regentry.tp ||
                                      ', entr_reg_regentry:[' || coalesce(err.entr_reg_regentry_object, '') || ']}}',
                                      ', ')
                    FROM entr_regentry_substances ers
                             INNER JOIN regentry
                                        ON regentry_id = ers.entr_src_id
                             LEFT JOIN (SELECT err.entr_dst_id  AS regentry_id,
                                               string_agg('{entr_id:' || err.entr_id || ', entr_tp:' || err.entr_tp ||
                                                          ', entr_co:' || err.entr_co || ', src:' || '{reg_id:' ||
                                                          reg.reg_id || ', reg_ke:' || reg.reg_ke || '}}',
                                                          ', ') AS entr_reg_regentry_object
                                        FROM entr_reg_regentry err
                                                 INNER JOIN reg
                                                            ON reg_id = err.entr_src_id
                                        WHERE err.entr_tp = 'REGULATION_ENTRY'
                                        GROUP BY err.entr_dst_id) err
                                       ON err.regentry_id = regentry.regentry_id
                    WHERE ers.entr_tp = 'SUBSTANCE_CONCERNED'
                    GROUP BY ers.entr_dst_id) ers2
                   ON substances.sub_id = ers2.sub_id;


SELECT
    /* substances */
    substances.sub_id,
    naml,
    cas,
    --esp.entr_substances_plc_object,
    ess.entr_subset_substances_object,
    ers.entr_reg_substances_object,
    ers2.entr_regentry_substances_object
FROM substances
         -- entr_substances_plc
         /* LEFT JOIN (SELECT string_agg('{entr_id: ' || esp.entr_id || ', entr_tp: ' ||
                                       coalesce(esp.entr_tp, '')
                                           || ', dst: {' || 'plc_id: ' || esp.entr_dst_id || ', entr_plc_plcsys:[' ||
                                       epp.entr_plc_plcsys_oblject || ']}'
                                           || '}', ',') AS entr_substances_plc_object,
                            esp.entr_src_id             AS sub_id
                     FROM entr_substances_plc esp
                              INNER JOIN (SELECT first.plc_id,
                                                 string_agg(first.entr_plc_plcsys_oblject || ',entr_plcsys_plcsys:[' ||
                                                            second.entr_plcsys_plcsys_object ||
                                                            ']}}', ', ') AS entr_plc_plcsys_oblject

                                          FROM (SELECT string_agg('{entr_id:' || epp.entr_id || ', entr_tp:' ||
                                                                  coalesce(epp.entr_tp, '') || ', dst: {' ||
                                                                  'plcsys_id:' ||
                                                                  pls.plcsys_id ||
                                                                  ', tp: ' || pls.tp || ',plcsys_ke:' || pls.plcsys_ke,
                                                                  ', ') as    entr_plc_plcsys_oblject,
                                                       epp.entr_src_id AS plc_id,
                                                       pls.plcsys_id   AS plcsys_id
                                                FROM entr_plc_plcsys epp
                                                         INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id AND
                                                                                  epp.entr_tp IN
                                                                                  ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                                                GROUP BY epp.entr_src_id, plcsys_id) first
                                                   LEFT JOIN (SELECT string_agg('{entr_id:' || entr_id ||
                                                                                ', entr_tp:' || coalesce(entr_tp, '') || ', dst:{' ||
                                                                                'plcsys_id:' || plcsys_id ||
                                                                                ', plcsys_ke:' || coalesce(plcsys_ke, '') || ', tp:' ||
                                                                                coalesce(tp, '') || '}}',
                                                                                ', ') AS entr_plcsys_plcsys_object,
                                                                     entr_src_id      AS plcsys_id
                                                              FROM entr_plcsys_plcsys
                                                                       INNER JOIN plcsys ON entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id
                                                              GROUP BY entr_src_id) second
                                                             USING (plcsys_id)
                                          GROUP BY first.plc_id) epp
                                         ON esp.entr_dst_id = epp.plc_id AND
                                            esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                     WHERE esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                     GROUP BY esp.entr_tp, esp.entr_src_id) esp
                    USING (sub_id) */
-- entr_subset_substances
         LEFT JOIN (SELECT ess.entr_dst_id                                           AS sub_id,
                           string_agg('{entr_id:' || ess.entr_id || ', entr_tp:' || ess.entr_tp || ', dst: {' ||
                                      'subset_id:' || ess.entr_src_id || '}' || ', entr_regentry_subset:[' ||
                                      ers.entr_regentry_subset_object || ']}', ', ') AS entr_subset_substances_object
                    FROM entr_subset_substances ess
                             INNER JOIN (SELECT ers.entr_dst_id,
                                                string_agg('{entr_id:' || ers.entr_id ||
                                                           ', entr_tp:' || ers.entr_tp || ', dst: {' ||
                                                           'regentry_id:' || ers.entr_src_id || '}' ||
                                                           ', entr_reg_regentry:[' || err.entr_reg_regentry_object ||
                                                           ']}',
                                                           ', ') AS entr_regentry_subset_object
                                         FROM entr_regentry_subset ers
                                                  INNER JOIN (SELECT err.entr_dst_id,
                                                                     string_agg('{entr_id:' || err.entr_id ||
                                                                                ', entr_tp:' || err.entr_tp ||
                                                                                ', dst:{' || 'reg_id:' || reg_id ||
                                                                                ', reg_ke:' || reg_ke ||
                                                                                '}}', ', ') AS entr_reg_regentry_object
                                                              FROM entr_reg_regentry err
                                                                       INNER JOIN reg
                                                                                  ON err.entr_src_id = reg_id
                                                              WHERE err.entr_tp = 'REGULATION_ENTRY'
                                                              GROUP BY entr_dst_id) err
                                                             ON ers.entr_src_id = err.entr_dst_id
                                         WHERE ers.entr_tp = 'SUBSET_CONCERNED'
                                         GROUP BY ers.entr_dst_id) ers
                                        ON ess.entr_src_id = ers.entr_dst_id
                    WHERE ess.entr_tp = 'IS_IN'
                    GROUP BY ess.entr_dst_id) ess
                   ON substances.sub_id = ess.sub_id
-- entr_substances_reg
         LEFT JOIN (SELECT ers.entr_dst_id  AS sub_id,
                           string_agg('{entr_id:' || ers.entr_id || ', entr_tp:' || ers.entr_tp || ', entr_co:' ||
                                      ers.entr_co || ', src:{' || 'reg_id:' || reg_id || ', reg_desc:' ||
                                      reg.reg_desc || ', info: ' || reg.info || ', name:' || reg.name || '}}',
                                      ', ') AS entr_reg_substances_object
                    FROM entr_reg_substances ers
                             INNER JOIN reg ON ers.entr_src_id = reg_id
                    WHERE ers.entr_tp = 'SUBSTANCES'
                      AND ers.entr_src_id IN (841, 840, 1378, 842)
                    GROUP BY ers.entr_dst_id) ers
                   ON substances.sub_id = ers.sub_id
-- entr_substances_regentry
         LEFT JOIN (SELECT ers.entr_dst_id  AS sub_id,
                           string_agg('{entr_id:' || ers.entr_id || ', entr_tp:' || ers.entr_tp || ', src:' ||
                                      '{regentry_id:' || regentry.regentry_id || ', regentry_tp:' || regentry.tp ||
                                      ', entr_reg_regentry:[' || COALESCE(err.entr_reg_regentry_object, '') || ']}}',
                                      ', ') AS entr_regentry_substances_object
                    FROM entr_regentry_substances ers
                             INNER JOIN regentry
                                        ON regentry_id = ers.entr_src_id
                             LEFT JOIN (SELECT err.entr_dst_id  AS regentry_id,
                                               string_agg('{entr_id:' || err.entr_id || ', entr_tp:' || err.entr_tp ||
                                                          ', entr_co:' || err.entr_co || ', src:' || '{reg_id:' ||
                                                          reg.reg_id || ', reg_ke:' || reg.reg_ke || '}}',
                                                          ', ') AS entr_reg_regentry_object
                                        FROM entr_reg_regentry err
                                                 INNER JOIN reg
                                                            ON reg_id = err.entr_src_id
                                        WHERE err.entr_tp = 'REGULATION_ENTRY'
                                        GROUP BY err.entr_dst_id) err
                                       ON err.regentry_id = regentry.regentry_id
                    WHERE ers.entr_tp = 'SUBSTANCE_CONCERNED'
                    GROUP BY ers.entr_dst_id) ers2
                   ON substances.sub_id = ers2.sub_id
WHERE EXISTS(
        SELECT 1
        FROM entr_subset_substances
                 INNER JOIN subset ON entr_src_id = subset.subset_id AND entr_tp = 'IS_IN'
            AND subset.key = 'Perfume+Component'
        WHERE entr_dst_id = ess.sub_id
    )
   OR ess.sub_id IS NOT NULL
   OR ers2.sub_id IS NOT NULL;


SELECT *
FROM plcsys
         INNER JOIN entr_plcsys_plcsys epp ON plcsys.plcsys_id = epp.entr_src_id
         INNER JOIN plcsys p2 ON entr_dst_id = p2.plcsys_id;


(SELECT epp.entr_src_id AS plc_id,
        string_agg('{entr_id:' || cast(epp.entr_id AS text) || ', entr_tp:' ||
                   epp.entr_tp || ', dst: {' || 'plcsys_id:' ||
                   cast(pls.plcsys_id AS text) ||
                   ', tp: ' || coalesce(pls.tp, '') || ',plcsys_ke:' || coalesce(cast(pls.plcsys_ke AS text), '') ||
                   ',entr_plcsys_plcsys:[' || ''/*epp1.entr_plcsys_plcsys_object*/ ||
                   ']}}',
                   ', ')   entr_plc_plcsys_oblject

 FROM entr_plc_plcsys epp
          INNER JOIN plcsys pls
                     ON epp.entr_dst_id = pls.plcsys_id AND epp.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
 GROUP BY epp.entr_src_id);



SELECT entr_src_id,
       string_agg('{entr_id:' || cast(epp.entr_id AS text) || ', entr_tp:' ||
                  epp.entr_tp || ', dst: {' || 'plcsys_id:' ||
                  cast(pls.plcsys_id AS text) ||
                  ', tp: ' || coalesce(pls.tp, '') || ',plcsys_ke:' || coalesce(cast(pls.plcsys_ke AS text), '') ||
                  ',entr_plcsys_plcsys:[' || ''/*epp1.entr_plcsys_plcsys_object*/ ||
                  ']}}', ',')
FROM entr_plc_plcsys epp
         INNER JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id AND entr_tp IN ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
GROUP BY epp.entr_src_id