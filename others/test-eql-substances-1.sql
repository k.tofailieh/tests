WITH plcsys_plcsys AS (SELECT string_agg('{entr_id:' || entr_id ||
                                         ', entr_tp:' || coalesce(entr_tp, '') || ', dst:{' ||
                                         'plcsys_id:' || plcsys_id ||
                                         ', plcsys_ke:' || coalesce(plcsys_ke, '') || ', tp:' ||
                                         coalesce(tp, '') || '}}',
                                         ', ') AS entr_plcsys_plcsys_object,
                              entr_src_id      AS plcsys_id
                       FROM entr_plcsys_plcsys
                                INNER JOIN plcsys ON entr_plcsys_plcsys.entr_dst_id = plcsys.plcsys_id
                       GROUP BY entr_src_id),
     plc_plcsys AS (SELECT string_agg('{entr_id:' || entr_id || ', entr_tp:' ||
                                      coalesce(entr_tp, '') || ', dst: {' ||
                                      'plcsys_id:' ||
                                      pls.plcsys_id ||
                                      ', tp: ' || pls.tp || ',plcsys_ke:' || pls.plcsys_ke,
                                      ', ') AS entr_plc_plcsys_oblject,
                           entr_src_id      AS plc_id,
                           pls.plcsys_id    AS plcsys_id
                    FROM entr_plc_plcsys
                             INNER JOIN plcsys pls ON entr_dst_id = pls.plcsys_id AND
                                                      entr_tp IN
                                                      ('HAZARD_CLASS_CAT', 'RISK', 'SAFETY')
                             LEFT JOIN plcsys_plcsys USING (plcsys_id)
                    GROUP BY entr_src_id, plcsys_id),
     plc_plcsys_plcsys AS (SELECT plc_plcsys.plc_id,
                                  string_agg(plc_plcsys.entr_plc_plcsys_oblject || ',entr_plcsys_plcsys:[' ||
                                             coalesce(plcsys_plcsys.entr_plcsys_plcsys_object, '') ||
                                             ']}}', ', ') AS entr_plc_plcsys_oblject
                           FROM plc_plcsys
                                    LEFT JOIN plcsys_plcsys USING (plcsys_id)
                           GROUP BY plc_id),
     substance_plc AS (SELECT string_agg('{entr_id: ' || esp.entr_id || ', entr_tp: ' ||
                                         COALESCE(esp.entr_tp, '')
                                             || ', dst: {' || 'plc_id: ' || esp.entr_dst_id || ', entr_plc_plcsys:[' ||
                                         entr_plc_plcsys_oblject || ']}'
                                             || '}', ',') AS entr_substances_plc_object,
                              esp.entr_src_id             AS sub_id
                       FROM entr_substances_plc esp
                                INNER JOIN plc_plcsys_plcsys ON esp.entr_dst_id = plc_plcsys_plcsys.plc_id AND
                                                                esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                       WHERE esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
                       GROUP BY esp.entr_src_id)
SELECT *
FROM substance_plc
;





