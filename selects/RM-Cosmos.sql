SELECT endpt.*
FROM fiche
         INNER JOIN entr_fiche_endpt ON fiche.m_id = entr_fiche_endpt.entr_src_id AND entr_tp = 'COSMOS_CONTENT'
         INNER JOIN endpt ON entr_fiche_endpt.entr_dst_id = endpt.endpt_id
         INNER JOIN der_endpt der ON entr_fiche_endpt.entr_dst_id = der.der_src_id
         INNER JOIN de ON der.der_dst_id = de.de_id
    AND de.de_tp = 'ENDPT_CAT' AND de_ke = 'ORG_PPAI'
WHERE m_id = 128915;

SELECT *
FROM de
WHERE de_tp = 'ENDPT_CAT'


WITH CosmosCatDescription AS (SELECT efe.entr_src_id,
                                     efe.entr_tp,
                                     endpt_id,
                                     de_id
                              FROM entr_fiche_endpt efe
                                       INNER JOIN endpt e
                                                  ON efe.entr_dst_id = e.endpt_id
                                       INNER JOIN der_endpt ON der_endpt.der_src_id = e.endpt_id AND der_tp = 'ENDPT_DESC'
                                       INNER JOIN de ON der_endpt.der_dst_id = de.de_id AND de_tp = 'ENDPT_CAT'

                              WHERE de.de_ke IN
                                    ('SyMo', 'SyIng', 'ORG_CPAI', 'CPAI', 'ORG_PPAI', 'PPAI')),
     dede AS (SELECT de_ke, de_id
              FROM de
              WHERE de.de_ke IN
                    ('SyMo', 'SyIng', 'ORG_CPAI', 'CPAI', 'ORG_PPAI', 'PPAI')),
     ss AS (SELECT m_id, CosmosCatDescription.*, dede.*
            FROM fiche f
                     LEFT JOIN CosmosCatDescription ON CosmosCatDescription.entr_src_id = f.m_id AND
                                                       CosmosCatDescription.entr_tp = 'COSMOS_CONTENT'
                     RIGHT JOIN dede ON dede.de_id = CosmosCatDescription.de_id OR CosmosCatDescription.de_id IS NULL)
SELECT ss.*,
       CASE
           WHEN ss.de_ke = 'PPAI' THEN value1
           WHEN ss.de_ke = 'ORG_PPAI' THEN value2
           WHEN ss.de_ke = 'CPAI' THEN value3
           WHEN ss.de_ke = 'ORG_CPAI' THEN value4
           WHEN ss.de_ke = 'SyIng' THEN value5
           WHEN ss.de_ke = 'SyMo' THEN value6 END as cosmos_value

FROM import.import_line
         INNER JOIN
     ss ON ss.m_id = internal_id
WHERE m_id = 158493
  AND imp_ref = :imp_ref
ORDER BY m_id;


SELECT internal_id,
       imp_ref,
       value1,
       value2,
       value3,
       value4,
       value5,
       value6
FROM import.import_line
WHERE imp_ref = :imp_ref;


INSERT INTO import.import_line(internal_id, imp_ref, value1, value2, value3, value4, value5, value6)
VALUES (158493, 'import-cosmos-test', 1.0, 2.0, 3.0, 4.0, 5.0, 6.0)