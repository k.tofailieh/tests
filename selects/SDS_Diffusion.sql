	with data_table as (
			select
				doc.doc_id,
				doc.doc_tl,
				doc.doc_version,
				doc.doc_encoding,
				doc.lg,
				dir.displayname as supplier,
				dir.dir_id as dir_id,
				lower(LG.de_ke) as language_ke,
				lower(COUNTRY.de_ke) as country_ke,
				doc_src.doc_file_name,
				doc_src.doc_id as logo_id,
				case
					when docr_sds.docr_xml = '' then '{"custId":""}'
					when docr_sds.docr_xml is null then '{"custId":""}'
					else docr_sds.docr_xml
				end as docr_xml
			from
				doc
			inner join docr_sds on
				docr_sds.docr_dst_id = doc.doc_id
				and docr_sds.docr_tp = 'SDSDIFFUSION_PARAMS'
			inner join sdsdoc on
				docr_sds.docr_src_id = sdsdoc.sdsdoc_id
				and sdsdoc.sdsdoc_id = :sdsdocId

			left join entr_doc_dir edd on
				edd.entr_src_id = doc.doc_id
				and edd.entr_tp = 'SDSDIFFUSION_PARAMS_SUPPLIER'
			left join dir on
				edd.entr_dst_id = dir.dir_id

			left join der_doc dd1 on dd1.der_src_id = doc.doc_id and dd1.der_tp = 'AVAILABLE_LANGUAGE'
			left join de LG on dd1.der_dst_id = LG.de_id

			left join der_doc dd2 on dd2.der_src_id = doc.doc_id and dd2.der_tp = 'AVAILABLE_COUNTRIES'
			left join de COUNTRY on dd2.der_dst_id = COUNTRY.de_id


			left join docr_doc on
			docr_doc.docr_src_id = doc.doc_id
			and docr_doc.docr_tp = 'SDSDIFFUSION_PARAMS_LOGO'
			left join doc doc_src on
			docr_doc.docr_dst_id = doc_src.doc_id )
			select
				doc_id,
				doc_tl,
				doc_version,
				doc_encoding,
				lg,
				supplier,
				dir_id,
				language_ke,
				country_ke,
				doc_file_name,
				logo_id,
				jsondata.value as cust_id
			from
				data_table,
				pg_catalog.jsonb_each_text(docr_xml::jsonb) jsondata;



select * from docr_sds where docr_sds.docr_tp = 'SDSDIFFUSION_PARAMS';


 -- entr_doc_dir: 'SDSDIFFUSION_PARAMS_SUPPLIER'
 -- docr_doc: 'SDSDIFFUSION_PARAMS_LOGO'
 -- der_doc : 'AVAILABLE_LANGUAGE', 'AVAILABLE_COUNTRIES'

select duplicate_sdsdoc(100058209);

SELECT * from docr_sds where docr_src_id = 100173751;

select * from entr_sdsdoc_sdschap where entr_src_id = 100173751;

select * from sdsdoc where sdsdoc_id = 100173751;

select DISTINCT docr_tp from docr_sds

select doc.* from sdsdoc
         inner join docr_sds on sdsdoc.sdsdoc_id = docr_sds.docr_src_id
         inner join doc on docr_sds.docr_dst_id = doc.doc_id
         inner join entr_doc_dir edd ON doc.doc_id = edd.entr_src_id
         where sdsdoc_id = 100173753

select public.duplicate_doc(758584)

select * from doc where doc_id = 758585