/*Product/Formule ingredients*/
select split_part(prod.ns, '/', 1) as org,
       prod.marguage as product_code,
       prod.m_ref as product_name,
       prodnomenclature.rawmaterial_concentration,
       prodnomenclature.m_ref as rawmaterial_name,
       prodnomenclature.marguage as rawmaterial_code,
       prodnomenclature.ingredient_concentration,
       prodnomenclature.inciname,
       prodnomenclature.cas,
       ingredientrealcompo.prod_conc as ingredient_total_product_concentration,
       prodnomenclature.ingredient_type,
       prodnomenclature.origin,
       ingredientfunction.cosmetic_ingredient_function_eu,
       ingredientfunction.cosmetic_ingredient_function_usa,
       ingredientfunction.cosmetic_ingredient_function_canada,
       brand.ke
from fiche prod
/* Get down the nomenclature from prod to INCI*/
     left outer join (select distinct rawmatarial.marguage,
                                      max(case
                                              when entr_fiche_nomclit_direct_compo_rm.entr_tp = 'DIRECT_COMPO'
                                                  then nc.conc_real_ubound
                                              else nc.conc_direct_ubound
                                          end) as rawmaterial_concentration,
                                      entr_fiche_nomclit_direct_compo_rm.entr_src_id,
                                      rawmatarial.m_ref,
                                      rawmatarial.m_id,
                                      rawmatnomenclature.conc_real_ubound as ingredient_concentration,
                                      inci.inci_id,
                                      inci.inciname,
                                      s.cas,
                                      ingredient.tp as ingredient_type,
                                      entr_fiche_nomclit_direct_compo.entr_char1 as origin
                      from nomclit nc
                           inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo_rm
                                          on nc.nomclit_id = entr_fiche_nomclit_direct_compo_rm.entr_dst_id
                                          and entr_fiche_nomclit_direct_compo_rm.entr_tp in
                                              ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has_rm
                                          on entr_nomclit_fiche_has_rm.entr_src_id = nc.nomclit_id and
                                             entr_nomclit_fiche_has_rm.entr_tp in
                                             ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA',
                                              'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                           inner join fiche rawmatarial
                                          on rawmatarial.m_id = entr_nomclit_fiche_has_rm.entr_dst_id
                           inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo
                                          on entr_fiche_nomclit_direct_compo.entr_src_id = rawmatarial.m_id and
                                             entr_fiche_nomclit_direct_compo.entr_tp = 'DIRECT_COMPO'
                           inner join nomclit rawmatnomenclature on rawmatnomenclature.nomclit_id =
                                                                    entr_fiche_nomclit_direct_compo.entr_dst_id
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has
                                          on entr_nomclit_fiche_has.entr_src_id = rawmatnomenclature.nomclit_id and
                                             entr_nomclit_fiche_has.entr_tp = 'HAS'
                           inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has.entr_dst_id
                           left join  anx.entr_fiche_inci entr_fiche_inci
                                          on entr_fiche_inci.entr_src_id = ingredient.m_id and
                                             entr_fiche_inci.entr_tp in () 'INGREDIENT'
                           left join  anx.inci inci on inci.inci_id = entr_fiche_inci.entr_dst_id


                           left join  entr_fiche_substances efs
                                          on ingredient.m_id = efs.entr_src_id and efs.entr_tp = 'INGREDIENT'
                           left join  substances s on efs.entr_dst_id = s.sub_id

                      group by rawmatarial.m_ref, rawmatarial.marguage,
                               entr_fiche_nomclit_direct_compo_rm.entr_src_id, rawmatarial.m_id,
                               ingredient_concentration, inci.inci_id, inci.inciname, inci.cas, ingredient_type,
                               origin, s.cas

                      order by rawmaterial_concentration) as prodnomenclature
                         on prodnomenclature.entr_src_id = prod.m_id
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
     left outer join
                     (select distinct entr_fiche_inci.entr_dst_id as fiche_inci_id,
                                      max(prodingredient.conc_real_ubound) as prod_conc,
                                      rc.entr_src_id,
                                      rc.entr_tp
                      from entr_fiche_nomclit rc
                           inner join nomclit prodingredient on prodingredient.nomclit_id = rc.entr_dst_id
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has_real
                                          on entr_nomclit_fiche_has_real.entr_src_id = prodingredient.nomclit_id and
                                             entr_nomclit_fiche_has_real.entr_tp in
                                             ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                           inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has_real.entr_dst_id
                           inner join anx.entr_fiche_inci 
                                          on entr_fiche_inci.entr_src_id = ingredient.m_id and
                                             entr_fiche_inci.entr_tp = 'INGREDIENT'
                      group by fiche_inci_id, rc.entr_src_id, rc.entr_tp
                      order by prod_conc desc) as ingredientrealcompo
                         on ingredientrealcompo.entr_src_id = prod.m_id and
                            ingredientrealcompo.fiche_inci_id = prodnomenclature.inci_id and
                            ingredientrealcompo.entr_tp in ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
     left outer join
                     (select string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') as cosmetic_ingredient_function_eu,
                             string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') as cosmetic_ingredient_function_usa,
                             string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_CANADA') as cosmetic_ingredient_function_canada,
                             inci_app.entr_src_id
                      from anx.entr_inci_app inci_app
                           inner join app on app.app_id = inci_app.entr_dst_id
                      group by inci_app.entr_src_id) as ingredientfunction
                         on ingredientfunction.entr_src_id = prodnomenclature.inci_id
     left outer join
                     (select entr_src_id as m_id,
                             lov.ke
                      from entr_fiche_lov
                           inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id
                          and coalesce(lov.tp, 'PRODUCT_BRAND') = 'PRODUCT_BRAND'
                      where entr_tp = 'COSMETIC_PRODUCT_BRAND'
                        and split_part(lov.ns, '/', 1) = :user_organisation) brand on prod.m_id = brand.m_id
where split_part(prod.ns, '/', 1) = :user_organisation
  and prod.m_id = cast(:MId as bigint)
order by product_code, rawmaterial_concentration desc, ingredient_concentration desc;



select inciname, cas
from anx.inci
where inciname ilike 'Water';



select *
from (select entr_src_id as m_id,
             lov.ke
      from entr_fiche_lov
           inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id and
                             coalesce(lov.tp, 'PROJECT_BRAND') = 'PROJECT_BRAND'
      where entr_tp = 'COSMETIC_PRODUCT_BRAND'
        and split_part(lov.ns, '/', 1) = :user_organisation) brand


---------------------------------------------


select nomclit.conc_real_ubound, substances.cas, substances.naml, substances.ec, fiche.m_ref as product_name,
       fiche.marguage as product_code
from fiche
     inner join entr_fiche_nomclit efn on fiche.m_id = efn.entr_src_id and efn.entr_tp = 'REAL_COMPO'
     inner join nomclit on efn.entr_dst_id = nomclit.nomclit_id
     inner join entr_nomclit_fiche on entr_nomclit_fiche.entr_src_id = nomclit.nomclit_id
     inner join fiche allergen
                    on allergen.m_id = entr_nomclit_fiche.entr_dst_id and entr_nomclit_fiche.entr_tp = 'HAS' and
                       allergen.tp = 'ALLERGEN'
     inner join entr_fiche_substances efs on allergen.m_id = efs.entr_src_id
     inner join substances on substances.sub_id = efs.entr_dst_id and efs.entr_tp = 'ALLERGEN'
where split_part(fiche.ns, '/', 1) = :user_organisation
  and fiche.m_id = cast(:MId as bigint)
group by fiche.m_ref, fiche.marguage, substances.cas, substances.naml, substances.ec, nomclit.conc_real_ubound;

select *
from import.import_line
order by imp_id desc
------------------------------------------------------------------------------------------------------------------------

/*Product/Formule ingredients*/
select distinct split_part(prod.ns, '/', 1) as org, prod.marguage as product_code, prod.m_ref as product_name,
                prodnomenclature.inciname, coalesce(nullif(prodnomenclature.cas, ''), '-') as cas,
                coalesce(nullif(prodnomenclature.ec, ''), '-') as ec,
                ingredientrealcompo.prod_conc as ingredient_total_product_concentration,
                prodnomenclature.ingredient_type, prodnomenclature.origin,
                replace(regexp_replace(ingredientfunction.cosmetic_ingredient_function_eu, '_', ' ', 'g'), ',',
                        ', ') as cif_eu, ingredientfunction.cosmetic_ingredient_function_usa,
                ingredientfunction.cosmetic_ingredient_function_canada
        , brand.ke as brand
from fiche prod
/* Get down the nomenclature from prod to INCI*/
     left outer join (select distinct rawmatarial.marguage,
                                      max(case
                                              when entr_fiche_nomclit_direct_compo_rm.entr_tp in ('DIRECT_COMPO', '')
                                                  then nc.conc_real_ubound
                                              else nc.conc_direct_ubound
                                          end) as rawmaterial_concentration,
                                      entr_fiche_nomclit_direct_compo_rm.entr_src_id,
                                      rawmatarial.m_ref, rawmatarial.m_id,
                                      rawmatnomenclature.conc_real_ubound as ingredient_concentration,
                                      inci.inci_id, inci.inciname, substances.cas, substances.ec,
                                      ingredient.tp as ingredient_type,
                                      entr_fiche_nomclit_direct_compo.entr_char1 as origin
                      from nomclit nc
                           inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo_rm
                                          on nc.nomclit_id = entr_fiche_nomclit_direct_compo_rm.entr_dst_id
                                          and entr_fiche_nomclit_direct_compo_rm.entr_tp in
                                              ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has_rm
                                          on entr_nomclit_fiche_has_rm.entr_src_id = nc.nomclit_id and
                                             entr_nomclit_fiche_has_rm.entr_tp in
                                             ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA',
                                              'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                           inner join fiche rawmatarial on rawmatarial.m_id = entr_nomclit_fiche_has_rm.entr_dst_id
                           inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo
                                          on entr_fiche_nomclit_direct_compo.entr_src_id = rawmatarial.m_id and
                                             entr_fiche_nomclit_direct_compo.entr_tp = 'DIRECT_COMPO'
                           inner join nomclit rawmatnomenclature
                                          on rawmatnomenclature.nomclit_id = entr_fiche_nomclit_direct_compo.entr_dst_id
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has
                                          on entr_nomclit_fiche_has.entr_src_id = rawmatnomenclature.nomclit_id and
                                             entr_nomclit_fiche_has.entr_tp = 'HAS'
                           inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has.entr_dst_id
                           inner join entr_fiche_substances on ingredient.m_id = entr_fiche_substances.entr_src_id
                           inner join substances on substances.sub_id = entr_fiche_substances.entr_dst_id
                           inner join anx.entr_fiche_inci entr_fiche_inci
                                          on entr_fiche_inci.entr_src_id = ingredient.m_id and
                                             entr_fiche_inci.entr_tp = 'INGREDIENT'
                           inner join anx.inci inci on inci.inci_id = entr_fiche_inci.entr_dst_id
                      group by rawmatarial.m_ref, rawmatarial.marguage, entr_fiche_nomclit_direct_compo_rm.entr_src_id,
                               rawmatarial.m_id,
                               ingredient_concentration, inci.inci_id, inci.inciname, substances.cas, ingredient_type,
                               origin, substances.ec
                      order by rawmaterial_concentration) as prodnomenclature
                         on prodnomenclature.entr_src_id = prod.m_id
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
     left outer join
                     (select distinct entr_fiche_inci.entr_dst_id as fiche_inci_id,
                                      max(prodingredient.conc_real_ubound) as prod_conc, rc.entr_src_id, rc.entr_tp
                      from entr_fiche_nomclit rc
                           inner join nomclit prodingredient on prodingredient.nomclit_id = rc.entr_dst_id
                           inner join entr_nomclit_fiche entr_nomclit_fiche_has_real
                                          on entr_nomclit_fiche_has_real.entr_src_id = prodingredient.nomclit_id and
                                             entr_nomclit_fiche_has_real.entr_tp in
                                             ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                           inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has_real.entr_dst_id
                           inner join anx.entr_fiche_inci entr_fiche_inci
                                          on entr_fiche_inci.entr_src_id = ingredient.m_id and
                                             entr_fiche_inci.entr_tp = 'INGREDIENT'
                      group by fiche_inci_id, rc.entr_src_id, rc.entr_tp
                      order by prod_conc desc) as ingredientrealcompo on ingredientrealcompo.entr_src_id = prod.m_id and
                                                                         ingredientrealcompo.fiche_inci_id =
                                                                         prodnomenclature.inci_id and
                                                                         ingredientrealcompo.entr_tp in
                                                                         ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
     left outer join
                     (select string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') as cosmetic_ingredient_function_eu,
                             string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') as cosmetic_ingredient_function_usa,
                             string_agg(app.app_ke, ',')
                             filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_CANADA') as cosmetic_ingredient_function_canada,
                             inci_app.entr_src_id
                      from anx.entr_inci_app inci_app
                           inner join app on app.app_id = inci_app.entr_dst_id
                      group by inci_app.entr_src_id) as ingredientfunction
                         on ingredientfunction.entr_src_id = prodnomenclature.inci_id
     left outer join
                     (select entr_src_id as m_id,
                             lov.ke
                      from entr_fiche_lov
                           inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id
                          and coalesce(lov.tp, 'PRODUCT_BRAND') = 'PRODUCT_BRAND'
                      where entr_tp = 'COSMETIC_PRODUCT_BRAND'
                        and split_part(lov.ns, '/', 1) = :user_organisation) brand on prod.m_id = brand.m_id
where split_part(prod.ns, '/', 1) = :user_organisation and prod.m_id = cast(:MId as bigint)
order by ingredient_total_product_concentration desc;
