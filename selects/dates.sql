select min(case dw when '2' then dm end) as MO,
       min(case dw when '3' then dm end) as TU,
       min(case dw when '4' then dm end) as WE,
       min(case dw when '5' then dm end) as TH,
       min(case dw when '6' then dm end) as FR,
       min(case dw when '7' then dm end) as SA,
       min(case dw when '1' then dm end) as SU,
       ww
from (select id,
             to_char(cast(date_trunc('month', current_date) as date) + id, 'iw')   ww,
             to_char(cast(date_trunc('month', current_date) as date) + id, 'dd')   dm,
             to_char(cast(date_trunc('month', current_date) as date) + id, 'd') as dw,
             to_char(cast(date_trunc('month', current_date) as date) + id, 'mm')   current_mnth,
             to_char(current_date, 'mm')                                        as mth
      from generate_series(0, 31) id) x
where current_mnth = mth
group by ww


select x, ((row_number() over () - 1) / 7) + 1
from generate_series(0, 30) x



with recursive x (start_date, end_date)
                   as
                   (select cast(date_trunc('day', to_date('15,1,2014', 'dd mm yyyy')) -
                                (cast(extract(day from date_trunc('day', to_date('15,1,2014', 'dd mm yyyy')))
                                     as integer) - 1) as date)
                         , '15-12-2015'
                    union all
                    select cast(start_date + interval '1 month' as date)
                         , end_date
                    from x
                    where start_date < end_date)
select x.start_date
from x


select extract(day from date_trunc('day', to_date('15,1,2014', 'dd mm yyyy')))


select dir.cn, dir.displayname, d1.displayname, d1.cn
from dir
         inner join entr_dir_dir edd on dir.dir_id = edd.entr_dst_id and entr_tp = 'MEMBER'
         inner join dir d1 on entr_src_id = d1.dir_id
where d1.displayname ilike 'rfgi%'
order by d1.cn


select to_char(current_date, 'day')

select *
from dir
where cn ilike '%rfgi%'


select imp_ref
from import.import_line
order by imp_id desc


select distinct imp_err_msg, imp_ref
from import.import_line
where value30 = 'SUBSTANCE'


select gridtest_data, gridtest_metadata
from study
where gridtest_data is not null
   or gridtest_metadata is not null;


