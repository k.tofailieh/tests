
------ Check if the internal_id or cust_id is both null ------
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'Action column not valid.'
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and lower(coalesce(value30,'')) not in ('create', 'modify')
;

------ Check if the internal_id or cust_id is both null ------
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'Both Material EcoUID and CustomId are null.'
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and coalesce(cust_id,'') = ''
and coalesce(value1, '') = '' and coalesce(value2, '') = '' /* should provide name or code or cust_id. */
and lower(coalesce(value30,'')) = 'create'
;

-- Check if EcoUID is provided with "create" action
update import.import_line set imp_err = true, imp_err_msg = coalesce(imp_err_msg|| ' | ', '') || E'Shouldn\'t fill EcoUID with Create Action.'
where internal_id is not null
and imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
;

------ Check double of the cust_id in the import file ------
update import.import_line imp1
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'There exists a same Material CustomId in this import file.'
where imp1.imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and coalesce(imp1.cust_id,'') != ''
and exists (
	select 1 from import.import_line imp2
	where imp2.imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
	and imp2.imp_id != imp1.imp_id
	and coalesce(imp2.cust_id,'') = imp1.cust_id
)
;

------ Check if the matching criteria (EcoUID, cust_id and name) are empty with "modify" action. ------
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'The Material name is empty.'
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and coalesce(value2,'') = ''
and (internal_id is null and coalesce(cust_id, '') = '')
and lower(value30) = 'modify'
;

------ Error if not exist the product when modify and warning if exists a product with the same name and same code when create ------
with allproducts as (
	select m_id, cust_id, marguage, m_ref from fiche
	where (ns = 'EcoMundo' or ns like 'EcoMundo' || '/%')
	and active in (1,2) and m_attribute = '0'
)
update import.import_line
set imp_warn = true, imp_warn_msg = coalesce(imp_warn_msg || ' | ','') || 'There exists a Material with the same name and code in the database.'
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and exists (
	select 1 from allproducts
	where internal_id = m_id or import_line.cust_id = allproducts.cust_id or (coalesce(marguage,'') = coalesce(value1,'') and coalesce(m_ref,'') = coalesce(value2,''))
	    )
and lower(coalesce(value30,'')) = 'create'
;


-- Check if the product exists in DB using EcoUID
with allproducts as (
	select m_id, cust_id, marguage, m_ref from fiche
	where (ns = 'EcoMundo' or ns like 'EcoMundo' || '/%')
	and active in (1,2) and m_attribute = '0'
        )
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || format('The material with "%s" EcoUID does not exist in the database to modify.', internal_id)
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
  and internal_id is not null
  and coalesce(lower(value30), '') = 'modify'
  and not exists (
      select 1 from allproducts where import_line.internal_id = m_id
    )
;

-- get product m_id if it matched using cust_id or combination (name and code)
with allproducts as (
	select m_id, cust_id, marguage, m_ref from fiche
	where (ns = 'EcoMundo' or ns like 'EcoMundo' || '/%')
	and active in (1,2) and m_attribute = '0'
        )
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || format('The Material with "%s" cust_id and "%s" name is not exists in database or founded many times.', cust_id, value2)
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
    and internal_id is null
    and 1 <> (
        select count(1) from allproducts
        where (coalesce(import_line.cust_id, '') = allproducts.cust_id) or
              (coalesce(import_line.cust_id, '') = '' and coalesce(upper(trim(marguage)),'') = coalesce(upper(trim(value1)),'') and coalesce(upper(trim(m_ref)),'') = coalesce(upper(trim(value2)),''))
        )
    and lower(coalesce(value30,'')) = 'modify'
;

--------- Check validity of Material type --------
update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg || ' | ','') || 'The Material type is not valid. Please choose from the selectable combo box.'
where imp_ref = 'ImportSdsMaterial_f675115e-1e25-41dd-954a-dca3cfa559b9_Material'
and coalesce(value25,'') not in ('Product','Commercial substance')
;
