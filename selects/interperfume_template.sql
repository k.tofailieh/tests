/* interperfume -- submission code. */
select distinct product.m_id as p_id, product.m_ref as p_name,
                raw_material.m_id as rm_id, raw_material.m_ref as rm_name,
                studies.purpose_flag as country, studies.conclusion as submision_code, studies.study_id,
                nom_ing.conc_real_ubound as concentration,
                inci.inciname as inciname, inci.cas as cas,
                coalesce(supplier.displayname, soc.displayname) as supplier_name,
                app.app_ke as function,
                nom_rm.conc_real_ubound as added, product.marguage as pr_code, ingredient.tp,
                sum_allergens.concentration as sum_allerg
from fiche product
     inner join entr_fiche_nomclit efn_pr on product.m_id = efn_pr.entr_src_id and efn_pr.entr_tp in ('DIRECT_COMPO')
     inner join nomclit nom_rm on efn_pr.entr_dst_id = nom_rm.nomclit_id
     inner join entr_nomclit_fiche enf1 on nom_rm.nomclit_id = enf1.entr_src_id and enf1.entr_tp = 'HAS'
     inner join fiche raw_material on raw_material.m_id = enf1.entr_dst_id and raw_material.tp = 'RAW_MATERIAL'
     inner join entr_fiche_nomclit efn_rm on raw_material.m_id = efn_rm.entr_src_id
     inner join nomclit nom_ing on efn_rm.entr_dst_id = nom_ing.nomclit_id
     inner join entr_nomclit_fiche enf2 on nom_ing.nomclit_id = enf2.entr_src_id and enf2.entr_tp = 'HAS'
     inner join fiche ingredient on ingredient.m_id = enf2.entr_dst_id and ingredient.tp in ('INGREDIENT', 'ALLERGEN')

     inner join anx.entr_fiche_inci efi on ingredient.m_id = efi.entr_src_id
     inner join anx.inci inci on efi.entr_dst_id = inci.inci_id
     left join  (select raw_material.m_id as rm_id, raw_material.m_ref as rm_name,
                        sum(nom_ing.conc_real_ubound) as concentration

                 from fiche raw_material
                      inner join entr_fiche_nomclit efn_rm on raw_material.m_id = efn_rm.entr_src_id
                      inner join nomclit nom_ing on efn_rm.entr_dst_id = nom_ing.nomclit_id
                      inner join entr_nomclit_fiche enf2
                                     on nom_ing.nomclit_id = enf2.entr_src_id and enf2.entr_tp = 'HAS'
                      inner join fiche ingredient
                                     on ingredient.m_id = enf2.entr_dst_id and ingredient.tp in ('ALLERGEN')
                 where split_part(raw_material.ns, '/', 1) = :user_organisation
                   and raw_material.active <> 0
                   and raw_material.tp = 'RAW_MATERIAL'
                 group by rm_id, rm_name) sum_allergens on rm_id = raw_material.m_id
     left join
                (select study.category, conclusion, purpose_flag, study_id, study_dt,
                        max(study_dt) over (partition by purpose_flag,efp.entr_src_id) as max_dt,
                        efp.entr_src_id as m_id
                 from entr_fiche_profile efp
                      inner join profile on efp.entr_dst_id = profile.profile_id and profile.tptp = 'Registration'
                      inner join entr_profile_study eps
                                     on profile.profile_id = eps.entr_src_id and eps.entr_tp = 'REGULATORY_PROFILE_DATA'
                      inner join study on eps.entr_dst_id = study.study_id and study.category = 'Registration'
                 where study.purpose_flag = 'CN' and efp.entr_tp = 'REGULATORY_PROFILE') studies
                    on raw_material.m_id = studies.m_id and studies.max_dt = studies.study_dt
     left join  entr_fiche_dir efd on efd.entr_src_id = raw_material.m_id and efd.entr_tp = 'SUPPLIER'
     left join  dir supplier on supplier.dir_id = efd.entr_dst_id
     left outer join
                (select esd.entr_dst_id, society_id, displayname
                 from entr_society_dir esd
                      inner join society on esd.entr_src_id = society_id
                      inner join entr_dir_dir edd on edd.entr_src_id = esd.entr_dst_id and
                                                     edd.entr_tp in ('SUPPLIER', 'COSMETIC_RAWMATERIAL_SUPPLIER')
                     and edd.entr_st in ('VALID', 'ACTIVE')
                 where esd.entr_tp = 'NATIF_RECORD'
                   and edd.entr_src_id = 543) soc
                    on soc.entr_dst_id = supplier.dir_id

     left join
                (select epn.entr_dst_id, epn.entr_tp, app.app_ke, app_tp
                 from entr_profile_nomclit epn
                      inner join entr_profile_app epa
                                     on epa.entr_src_id = epn.entr_src_id and epa.entr_tp = 'COSMETIC_FUNCTION_IDENTITY'
                      inner join app on epa.entr_dst_id = app.app_id and app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA'
                      inner join entr_nomclit_profile enp on enp.entr_dst_id = epa.entr_src_id and
                                                             enp.entr_tp = 'COSMETIC_FUNCTION_APPLIANCE') app
                    on nom_ing.nomclit_id = app.entr_dst_id and app.entr_tp = 'COSMETIC_FUNCTION_SOURCE'

where split_part(product.ns, '/', 1) = :user_organisation
  and raw_material.active <> 0
  and product.m_id = cast(:MId as bigint);


------------------------------------------------------------------------------------------------------------------------

select split_part(prod.ns, '/', 1) as org,
       prod.marguage as product_code,
       prod.m_ref as product_name,
       prodnomenclature.rawmaterial_concentration,
       prodnomenclature.m_ref as rawmaterial_name,
       prodnomenclature.marguage as rawmaterial_code,
       prodnomenclature.ingredient_concentration,
       prodnomenclature.inciname,
       coalesce(nullif(prodnomenclature.cas, ''), '-') as cas,
       ingredientrealcompo.prod_conc as ingredient_total_product_concentration,
       prodnomenclature.ingredient_type,
       prodnomenclature.origin,
       rawmaterialsupplier.supplier_name,
       ingredientfunction.cosmetic_ingredient_function_eu,
       ingredientfunction.cosmetic_ingredient_function_usa,
       ingredientfunction.cosmetic_ingredient_function_canada,
       brand.ke as brand, ingredient_concentration * rawmaterial_concentration / 100 as finalconc,
       row_number()
       over (partition by prodnomenclature.m_ref,prodnomenclature.marguage order by prodnomenclature.m_ref) as rank,
       studies.conclusion as submision_code
from fiche prod
/* Get down the nomenclature from prod to INCI*/
     left join (select distinct rawmatarial.m_id as rawmaterial_id, rawmatarial.marguage,
                                max(case
                                        when entr_fiche_nomclit_direct_compo_rm.entr_tp in
                                             ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                                            then nc.conc_real_ubound
                                        else nc.conc_direct_ubound end) as rawmaterial_concentration,
                                entr_fiche_nomclit_direct_compo_rm.entr_src_id, rawmatarial.m_ref, rawmatarial.m_id,
                                rawmatnomenclature.conc_real_ubound as ingredient_concentration, inci.inci_id,
                                inci.inciname, s.cas, ingredient.tp as ingredient_type,
                                entr_fiche_nomclit_direct_compo.entr_char1 as origin, rawmatnomenclature.nomclit_id
                from nomclit nc
                     inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo_rm
                                    on nc.nomclit_id = entr_fiche_nomclit_direct_compo_rm.entr_dst_id
                                    and entr_fiche_nomclit_direct_compo_rm.entr_tp in
                                        ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has_rm
                                    on entr_nomclit_fiche_has_rm.entr_src_id = nc.nomclit_id and
                                       entr_nomclit_fiche_has_rm.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA',
                                        'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST',
                                        'NOMENCLATURE_ITEM_FOR_FICHE_PRODUCT')
                     inner join fiche rawmatarial
                                    on rawmatarial.m_id = entr_nomclit_fiche_has_rm.entr_dst_id
                     inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo
                                    on entr_fiche_nomclit_direct_compo.entr_src_id = rawmatarial.m_id and
                                       entr_fiche_nomclit_direct_compo.entr_tp in
                                       ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                     inner join nomclit rawmatnomenclature on rawmatnomenclature.nomclit_id =
                                                              entr_fiche_nomclit_direct_compo.entr_dst_id
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has
                                    on entr_nomclit_fiche_has.entr_src_id = rawmatnomenclature.nomclit_id and
                                       entr_nomclit_fiche_has.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_RAW_MATERIAL')
                     inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has.entr_dst_id and
                                                    ingredient.tp in ('INGREDIENT', 'ALLERGEN')
                     left join  anx.entr_fiche_inci entr_fiche_inci_inci
                                    on entr_fiche_inci_inci.entr_src_id = ingredient.m_id and
                                       entr_fiche_inci_inci.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  anx.inci inci on inci.inci_id = entr_fiche_inci_inci.entr_dst_id


                     left join  entr_fiche_substances efs
                                    on ingredient.m_id = efs.entr_src_id and
                                       efs.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  substances s on efs.entr_dst_id = s.sub_id

                group by rawmatarial.m_ref, rawmatarial.marguage, rawmatnomenclature.nomclit_id,
                         entr_fiche_nomclit_direct_compo_rm.entr_src_id, rawmatarial.m_id,
                         ingredient_concentration, inci.inci_id, inci.inciname, inci.cas, ingredient_type,
                         origin, s.cas
                order by rawmaterial_concentration) as prodnomenclature
                   on prodnomenclature.entr_src_id = prod.m_id
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
     left join (select distinct entr_fiche_inci_inci.entr_dst_id as fiche_inci_id,
                                max(prodingredient.conc_real_ubound) as prod_conc, rc.entr_src_id, rc.entr_tp
                from entr_fiche_nomclit rc
                     inner join nomclit prodingredient on prodingredient.nomclit_id = rc.entr_dst_id
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has_real
                                    on entr_nomclit_fiche_has_real.entr_src_id = prodingredient.nomclit_id and
                                       entr_nomclit_fiche_has_real.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                     inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has_real.entr_dst_id
                     inner join anx.entr_fiche_inci entr_fiche_inci_inci
                                    on entr_fiche_inci_inci.entr_src_id = ingredient.m_id and
                                       entr_fiche_inci_inci.entr_tp in ('INGREDIENT', 'ALLERGEN')
                group by fiche_inci_id, rc.entr_src_id, rc.entr_tp
                order by prod_conc desc) as ingredientrealcompo
                   on ingredientrealcompo.entr_src_id = prod.m_id and
                      ingredientrealcompo.fiche_inci_id = prodnomenclature.inci_id and
                      ingredientrealcompo.entr_tp in ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
/* Get ingredient functions. */
     left join (select epn.entr_dst_id as nomclit_id,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') as cosmetic_ingredient_function_eu,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') as cosmetic_ingredient_function_usa,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_CANADA') as cosmetic_ingredient_function_canada
                from entr_profile_nomclit epn
                     inner join entr_profile_app epa on epa.entr_src_id = epn.entr_src_id and
                                                        epa.entr_tp = 'COSMETIC_FUNCTION_IDENTITY'
                     inner join app on epa.entr_dst_id = app.app_id and app.app_tp in
                                                                        ('COSMETIC_INGREDIENT_FUNCTION_USA',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_EU',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_CANADA')
                     inner join entr_nomclit_profile enp on enp.entr_dst_id = epa.entr_src_id and
                                                            enp.entr_tp = 'COSMETIC_FUNCTION_APPLIANCE'
                where epn.entr_tp = 'COSMETIC_FUNCTION_SOURCE'
                group by epn.entr_dst_id) ingredientfunction using (nomclit_id)
/* Get brand. */
     left join (select entr_src_id as m_id, lov.ke
                from entr_fiche_lov
                     inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id and
                                       coalesce(lov.tp, 'PRODUCT_BRAND') = 'PRODUCT_BRAND'
                where entr_tp = 'COSMETIC_PRODUCT_BRAND' and
                      split_part(lov.ns, '/', 1) = :user_organisation) brand on prod.m_id = brand.m_id
/* Get supplier. */
     left join (select efd.entr_src_id as rawmaterial_id,
                       string_agg(coalesce(society.displayname, dir.displayname, '-'), ' / ') as supplier_name
                from entr_fiche_dir efd
                     inner join dir on efd.entr_dst_id = dir.dir_id
                     left join  entr_society_dir esd on dir.dir_id = esd.entr_dst_id and esd.entr_tp = 'NATIF_RECORD'
                     left join  society on esd.entr_src_id = society.society_id
                where efd.entr_tp = 'SUPPLIER'
                group by rawmaterial_id) rawmaterialsupplier using (rawmaterial_id)
/* Get submission code. */
     left join (select string_agg(conclusion, ' / ') as conclusion,
                       efp.entr_src_id as rawmaterial_id
                from entr_fiche_profile efp
                     inner join profile on efp.entr_dst_id = profile.profile_id and profile.tptp = 'Registration'
                     inner join entr_profile_study eps
                                    on profile.profile_id = eps.entr_src_id and eps.entr_tp = 'REGULATORY_PROFILE_DATA'
                     inner join study on eps.entr_dst_id = study.study_id and study.category = 'Registration'
                where efp.entr_tp = 'REGULATORY_PROFILE'
                group by rawmaterial_id) studies
                   using (rawmaterial_id)
where split_part(prod.ns, '/', 1) = :user_organisation
  and prod.m_id = cast(:MId as bigint)
order by rawmaterial_concentration desc, finalconc desc, prodnomenclature.m_ref desc, prodnomenclature.marguage desc;


------------------------------------------------------------------------------------------------------------------------

select split_part(prod.ns, '/', 1) as org,
       prod.marguage as product_code,
       prod.m_ref as product_name,
       prodnomenclature.rawmaterial_concentration,
       prodnomenclature.m_ref as rawmaterial_name,
       prodnomenclature.marguage as rawmaterial_code,
       prodnomenclature.ingredient_concentration,
       prodnomenclature.inciname,
       coalesce(nullif(prodnomenclature.cas, ''), '-') as cas,
       ingredientrealcompo.prod_conc as ingredient_total_product_concentration,
       prodnomenclature.ingredient_type,
       prodnomenclature.origin,
       rawmaterialsupplier.supplier_name,
       ingredientfunction.cosmetic_ingredient_function_eu,
       ingredientfunction.cosmetic_ingredient_function_usa,
       ingredientfunction.cosmetic_ingredient_function_canada,
       brand.ke as brand, ingredient_concentration * rawmaterial_concentration / 100 as finalconc,
       row_number()
       over (partition by prodnomenclature.m_ref,prodnomenclature.marguage order by prodnomenclature.m_ref) as rank,
       studies.conclusion as submision_code
from fiche prod
/* Get down the nomenclature from prod to INCI*/
     left join (select distinct rawmatarial.m_id as rawmaterial_id, rawmatarial.marguage,
                                max(case
                                        when entr_fiche_nomclit_direct_compo_rm.entr_tp in ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                                            then nc.conc_real_ubound
                                        else nc.conc_direct_ubound end) as rawmaterial_concentration,
                                entr_fiche_nomclit_direct_compo_rm.entr_src_id, rawmatarial.m_ref, rawmatarial.m_id,
                                rawmatnomenclature.conc_real_ubound as ingredient_concentration, inci.inci_id,
                                inci.inciname, s.cas, ingredient.tp as ingredient_type,
                                entr_fiche_nomclit_direct_compo.entr_char1 as origin, rawmatnomenclature.nomclit_id
                from nomclit nc
                     inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo_rm
                                    on nc.nomclit_id = entr_fiche_nomclit_direct_compo_rm.entr_dst_id
                                    and entr_fiche_nomclit_direct_compo_rm.entr_tp in
                                        ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has_rm
                                    on entr_nomclit_fiche_has_rm.entr_src_id = nc.nomclit_id and
                                       entr_nomclit_fiche_has_rm.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA',
                                        'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST', 'NOMENCLATURE_ITEM_FOR_FICHE_PRODUCT')
                     inner join fiche rawmatarial
                                    on rawmatarial.m_id = entr_nomclit_fiche_has_rm.entr_dst_id
                     inner join entr_fiche_nomclit entr_fiche_nomclit_direct_compo
                                    on entr_fiche_nomclit_direct_compo.entr_src_id = rawmatarial.m_id and
                                       entr_fiche_nomclit_direct_compo.entr_tp in ('DIRECT_COMPO', 'DIRECT_COMPO_FICHE_RM')
                     inner join nomclit rawmatnomenclature on rawmatnomenclature.nomclit_id =
                                                              entr_fiche_nomclit_direct_compo.entr_dst_id
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has
                                    on entr_nomclit_fiche_has.entr_src_id = rawmatnomenclature.nomclit_id and
                                       entr_nomclit_fiche_has.entr_tp in( 'HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_RAW_MATERIAL')
                     inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has.entr_dst_id and
                                                    ingredient.tp in ('INGREDIENT', 'ALLERGEN')
                     left join  anx.entr_fiche_inci entr_fiche_inci_inci
                                    on entr_fiche_inci_inci.entr_src_id = ingredient.m_id and
                                       entr_fiche_inci_inci.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  anx.inci inci on inci.inci_id = entr_fiche_inci_inci.entr_dst_id


                     left join  entr_fiche_substances efs
                                    on ingredient.m_id = efs.entr_src_id and
                                       efs.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  substances s on efs.entr_dst_id = s.sub_id

                group by rawmatarial.m_ref, rawmatarial.marguage, rawmatnomenclature.nomclit_id,
                         entr_fiche_nomclit_direct_compo_rm.entr_src_id, rawmatarial.m_id,
                         ingredient_concentration, inci.inci_id, inci.inciname, inci.cas, ingredient_type,
                         origin, s.cas
                order by rawmaterial_concentration) as prodnomenclature
                   on prodnomenclature.entr_src_id = prod.m_id
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
     left join (select distinct entr_fiche_inci_inci.entr_dst_id as fiche_inci_id,
                                max(prodingredient.conc_real_ubound) as prod_conc, rc.entr_src_id, rc.entr_tp
                from entr_fiche_nomclit rc
                     inner join nomclit prodingredient on prodingredient.nomclit_id = rc.entr_dst_id
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has_real
                                    on entr_nomclit_fiche_has_real.entr_src_id = prodingredient.nomclit_id and
                                       entr_nomclit_fiche_has_real.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                     inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has_real.entr_dst_id
                     inner join anx.entr_fiche_inci entr_fiche_inci_inci
                                    on entr_fiche_inci_inci.entr_src_id = ingredient.m_id and
                                       entr_fiche_inci_inci.entr_tp in ('INGREDIENT', 'ALLERGEN')
                group by fiche_inci_id, rc.entr_src_id, rc.entr_tp
                order by prod_conc desc) as ingredientrealcompo
                   on ingredientrealcompo.entr_src_id = prod.m_id and
                      ingredientrealcompo.fiche_inci_id = prodnomenclature.inci_id and
                      ingredientrealcompo.entr_tp = 'REAL_COMPO'
/* Get ingredient functions. */
     left join (select epn.entr_dst_id as nomclit_id,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') as cosmetic_ingredient_function_eu,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') as cosmetic_ingredient_function_usa,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_CANADA') as cosmetic_ingredient_function_canada
                from entr_profile_nomclit epn
                     inner join entr_profile_app epa on epa.entr_src_id = epn.entr_src_id and
                                                        epa.entr_tp = 'COSMETIC_FUNCTION_IDENTITY'
                     inner join app on epa.entr_dst_id = app.app_id and app.app_tp in
                                                                        ('COSMETIC_INGREDIENT_FUNCTION_USA',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_EU',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_CANADA')
                     inner join entr_nomclit_profile enp on enp.entr_dst_id = epa.entr_src_id and
                                                            enp.entr_tp = 'COSMETIC_FUNCTION_APPLIANCE'
                where epn.entr_tp = 'COSMETIC_FUNCTION_SOURCE'
                group by epn.entr_dst_id) ingredientfunction using (nomclit_id)
/* Get brand. */
     left join (select entr_src_id as m_id, lov.ke
                from entr_fiche_lov
                     inner join lov on entr_fiche_lov.entr_dst_id = lov.lov_id and
                                       coalesce(lov.tp, 'PRODUCT_BRAND') = 'PRODUCT_BRAND'
                where entr_tp = 'COSMETIC_PRODUCT_BRAND' and
                      split_part(lov.ns, '/', 1) = :user_organisation) brand on prod.m_id = brand.m_id
/* Get supplier. */
     left join (select efd.entr_src_id as rawmaterial_id,
                       string_agg(coalesce(society.displayname, dir.displayname, '-'), ' / ') as supplier_name
                from entr_fiche_dir efd
                     inner join dir on efd.entr_dst_id = dir.dir_id
                     left join  entr_society_dir esd on dir.dir_id = esd.entr_dst_id and esd.entr_tp = 'NATIF_RECORD'
                     left join  society on esd.entr_src_id = society.society_id
                where efd.entr_tp = 'SUPPLIER'
                group by rawmaterial_id ) rawmaterialsupplier using (rawmaterial_id)
/* Get submission code. */
     left join (select string_agg(conclusion, ' / ') as conclusion,
                       efp.entr_src_id as rawmaterial_id
                from entr_fiche_profile efp
                     inner join profile on efp.entr_dst_id = profile.profile_id and profile.tptp = 'Registration'
                     inner join entr_profile_study eps
                                    on profile.profile_id = eps.entr_src_id and eps.entr_tp = 'REGULATORY_PROFILE_DATA'
                     inner join study on eps.entr_dst_id = study.study_id and study.category = 'Registration'
                where efp.entr_tp = 'REGULATORY_PROFILE'
                group by rawmaterial_id) studies
                   using(rawmaterial_id)
where split_part(prod.ns, '/', 1) = :user_organisation
  and prod.m_id = cast(:MId as bigint)
order by rawmaterial_concentration desc, finalconc desc, prodnomenclature.m_ref desc, prodnomenclature.marguage desc;



select distinct split_part(prod.ns, '/', 1) as org,
                prod.marguage as product_code,
                prod.m_ref as product_name,
                ingredientrealcompo.inciname,
                coalesce(nullif(ingredientrealcompo.cas, ''), '-') as cas,
                ingredientrealcompo.ingredient_concentration,
                ingredientrealcompo.ingredient_type,
                ingredientfunction.cosmetic_ingredient_function_eu,
                ingredientfunction.cosmetic_ingredient_function_usa,
                ingredientfunction.cosmetic_ingredient_function_canada
from fiche prod
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
     left join (select distinct efn.entr_src_id as m_id, inci.inciname, s.cas, nomclit_id,
                                ingredient.tp as ingredient_type,
                                max(prodingredient.conc_real_ubound) as ingredient_concentration
                from entr_fiche_nomclit efn
                     inner join nomclit prodingredient on prodingredient.nomclit_id = efn.entr_dst_id
                     inner join entr_nomclit_fiche entr_nomclit_fiche_has_real
                                    on entr_nomclit_fiche_has_real.entr_src_id = prodingredient.nomclit_id and
                                       entr_nomclit_fiche_has_real.entr_tp in
                                       ('HAS', 'NOMENCLATURE_ITEM_FOR_FICHE_FORMULA_TEST')
                     inner join fiche ingredient on ingredient.m_id = entr_nomclit_fiche_has_real.entr_dst_id

                     left join  anx.entr_fiche_inci
                                    on entr_fiche_inci.entr_src_id = ingredient.m_id and
                                       entr_fiche_inci.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  anx.inci inci on inci.inci_id = entr_fiche_inci.entr_dst_id

                     left join  entr_fiche_substances efs
                                    on ingredient.m_id = efs.entr_src_id and
                                       efs.entr_tp in ('INGREDIENT', 'ALLERGEN')
                     left join  substances s on efs.entr_dst_id = s.sub_id
                where efn.entr_tp in ('REAL_COMPO', 'REAL_COMPO_FICHE_INGREDIENT')
                group by efn.entr_src_id, nomclit_id,ingredient_type, inciname, s.cas
                order by ingredient_concentration desc) as ingredientrealcompo using (m_id)
/* Get ingredient functions. */
     left join (select epn.entr_dst_id as nomclit_id,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') as cosmetic_ingredient_function_eu,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') as cosmetic_ingredient_function_usa,
                       string_agg(distinct app.app_ke, '/ ')
                       filter (where app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_CANADA') as cosmetic_ingredient_function_canada
                from entr_profile_nomclit epn
                     inner join entr_profile_app epa on epa.entr_src_id = epn.entr_src_id and
                                                        epa.entr_tp = 'COSMETIC_FUNCTION_IDENTITY'
                     inner join app on epa.entr_dst_id = app.app_id and app.app_tp in
                                                                        ('COSMETIC_INGREDIENT_FUNCTION_USA',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_EU',
                                                                         'COSMETIC_INGREDIENT_FUNCTION_CANADA')
                     inner join entr_nomclit_profile enp on enp.entr_dst_id = epa.entr_src_id and
                                                            enp.entr_tp = 'COSMETIC_FUNCTION_APPLIANCE'
                where epn.entr_tp = 'COSMETIC_FUNCTION_SOURCE'
                group by epn.entr_dst_id) ingredientfunction using (nomclit_id)
where split_part(prod.ns, '/', 1) = :user_organisation
  and prod.m_id = cast(:MId as bigint)
order by ingredientrealcompo.ingredient_concentration desc ;


