select
  dependantObject.m_id as entity_id,
  'fiche' as entity_tp,
  dependantObject.tp as tp,
  dependantObject.m_ref as name,
  dependantObject.marguage as code,
  dependantObject.version_label,
  efd.entr_dt1 as creation_date
from
  fiche dependantObject
  left join entr_fiche_dir efd on efd.entr_src_id = dependantObject.m_id
  and efd.entr_tp = 'CREATOR'
  inner join entr_fiche_nomclit efn on efn.entr_src_id = dependantObject.m_id
  /* No filter on entr_tp because all src of nomclit is a dependant objet of the dst of nomclit */
  inner join nomclit n on n.nomclit_id = efn.entr_dst_id
  inner join entr_nomclit_fiche enf on enf.entr_src_id = n.nomclit_id
  /* No filter on entr_tp because all src of nomclit is a dependant objet of the dst of nomclit */
  and enf.entr_dst_id = :entityId
union all
select
  dependantObject.part_id as entity_id,
  'part' as entity_tp,
  dependantObject.tp as tp,
  dependantObject.name_1 as name,
  dependantObject.reference_1 as code,
  /**Vérifier si c'est name_1 ou name_2 et idem pour reference */
  '' as version_label,
  efd.entr_dt1 as creation_date
from
  part dependantObject
  /**Vérifier si ça existe*/
  left join entr_part_dir efd on efd.entr_src_id = dependantObject.part_id
  and efd.entr_tp = 'CREATOR'
  inner join entr_part_nomclit efn on efn.entr_src_id = dependantObject.part_id
  /* No filter on entr_tp because all src of nomclit is a dependant objet of the dst of nomclit */
  inner join nomclit n on n.nomclit_id = efn.entr_dst_id
  inner join entr_nomclit_fiche enf on enf.entr_src_id = n.nomclit_id
  /* No filter on entr_tp because all src of nomclit is a dependant objet of the dst of nomclit */
  and enf.entr_dst_id = :entityId
order by
  tp,
  name,
  code


select DISTINCT 'fiche' as "table", tp from fiche
union all
select DISTINCT 'part' as "table", tp from part


select translate('\nmmm\n', '\n', '')
