SELECT sub_id,
       naml,
       substances.idx,
       ec,
       cas,
       esp.entr_id,
       esp.entr_tp,
       plc.plc_id,
       epp.entr_id,
       epp.entr_tp,
       pls.plcsys_id,
       pls.plcsys_ke,
       pls.tp,

       ess.entr_id,
       ess.entr_tp,
       subset.subset_id,

       ers.entr_id,
       ers.entr_tp,
       subset_regentry.regentry_id,

       err.entr_id,
       err.entr_tp,
       subset_reg.reg_id,
       subset_reg.reg_ke,


       ers1.entr_id,
       ers1.entr_tp,
       ers1.entr_co,
       substance_reg.reg_id,
       substance_reg.reg_desc,
       substance_reg.info,
       substance_reg.name,

       ers2.entr_id,
       ers2.entr_tp,
       ers2.entr_co,

       substances_regentry.regentry_id,
       substances_regentry.ref,
       substances_regentry.co,
       substances_regentry.tp,
       err2.entr_id,
       err2.entr_tp,
       substance_reg1.reg_id,
       substance_reg1.reg_ke


FROM substances
         LEFT JOIN entr_substances_plc esp
                   ON substances.sub_id = esp.entr_src_id AND esp.entr_tp IN ('CLP-H', 'CLP_S', 'DSD_S', 'DSD_H')
         LEFT JOIN plc ON esp.entr_dst_id = plc.plc_id
         LEFT JOIN entr_plc_plcsys epp ON esp.entr_dst_id = epp.entr_src_id
         LEFT JOIN plcsys pls ON epp.entr_dst_id = pls.plcsys_id
         LEFT JOIN entr_subset_substances ess ON substances.sub_id = ess.entr_dst_id AND ess.entr_tp = 'IS_IN'
         LEFT JOIN subset ON ess.entr_src_id = subset.subset_id
         LEFT JOIN entr_regentry_subset ers ON subset.subset_id = ers.entr_dst_id AND ers.entr_tp = 'SUBSET_CONCERNED'
         LEFT JOIN regentry subset_regentry ON ers.entr_src_id = subset_regentry.regentry_id
         LEFT JOIN entr_reg_regentry err
                   ON subset_regentry.regentry_id = err.entr_dst_id AND err.entr_tp = 'REGULATION_ENTRY'
         LEFT JOIN reg subset_reg ON err.entr_src_id = subset_reg.reg_id


         LEFT JOIN entr_reg_substances ers1 ON substances.sub_id = ers1.entr_dst_id AND ers1.entr_tp = 'SUBSTANCES' AND
                                               ers1.entr_src_id IN (841, 840, 1378, 842)
         LEFT JOIN reg substance_reg ON ers1.entr_src_id = substance_reg.reg_id

         LEFT JOIN entr_regentry_substances ers2
                   ON substances.sub_id = ers2.entr_dst_id AND ers2.entr_tp = 'SUBSTANCE_CONCERNED'
         LEFT JOIN regentry substances_regentry ON substances_regentry.regentry_id = ers2.entr_src_id

         LEFT JOIN entr_reg_regentry err2
                   ON err2.entr_dst_id = substances_regentry.regentry_id AND err2.entr_tp = 'REGULATION_ENTRY' AND
                      epp.entr_src_id IN (841, 840, 1378, 842)
         LEFT JOIN reg substance_reg1 ON substance_reg1.reg_id = err2.entr_src_id
order by naml
offset :off limit 50;





