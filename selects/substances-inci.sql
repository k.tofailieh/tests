SELECT * FROM substances s
INNER JOIN anx.entr_substances_inci esi ON s.sub_id = esi.entr_src_id
INNER JOIN anx.inci inci ON inci.inci_id = esi.entr_dst_id
WHERE inci.cas = '7732-18-5';

SELECT * FROM fiche f
INNER JOIN anx.entr_fiche_inci efi ON efi.entr_src_id = m_id
INNER JOIN anx.inci i ON i.inci_id = efi.entr_dst_id
WHERE i.cas = '7732-18-5';

SELECT * FROM anx.inci i
INNER JOIN anx.entr_inci_incitranslation eii ON i.inci_id = eii.entr_src_id
INNER JOIN anx.incitranslation i2 ON i2.incitranslation_id = eii.entr_dst_id
WHERE i.inci_id  = 69688;

SELECT * FROM substances s
INNER JOIN anx.entr_substances_inci esi ON esi.entr_src_id = s.sub_id
INNER JOIN anx.inci ON inci_id = esi.entr_dst_id
WHERE s.cas = '7732-18-5';


SELECT f.m_id ,i.inci_id , i.inciname  FROM fiche f
INNER JOIN anx.entr_fiche_inci efi ON efi.entr_src_id = m_id
INNER JOIN anx.inci i ON i.inci_id = efi.entr_dst_id
WHERE f.tp = 'INGREDIENT'
AND i.inciname  ILIKE ANY (VALUES ('water%'),('aqua%'));