SELECT columns.table_schema, columns.table_name, ist_pk.constraint_schema, ist_pk.constraint_name, ist_pk.constraint_type, ist_pk_col.column_name
  FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
  INNER JOIN INFORMATION_SCHEMA.TABLES /* base tables  (that are temporal (or ft tables) ) */
    ON (TABLES.table_schema, TABLES.table_name) = (columns.table_schema, columns.table_name)
    AND TABLES.table_type = 'BASE TABLE'
  INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ist_pk /* Constraints (except temporal) */
    ON ist_pk.table_catalog = TABLES.table_catalog
    AND ist_pk.table_schema = TABLES.table_schema
    AND ist_pk.table_name = TABLES.table_name
  	--AND ist_pk.constraint_type in ('PRIMARY KEY','FOREIGN KEY','UNIQUE') /* Very few unique because we use "unique index" instead (but is is not a good practice, unique constraint is better) */
  	--AND ist_pk.constraint_name not like 'uc_%_tpsuid'
  	--AND ist_pk.constraint_name not like 'uc_%_id_tpsfrom'
  INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ist_pk_col /* The contraint column */
    ON ist_pk_col.table_catalog = ist_pk.table_catalog
    AND ist_pk_col.table_schema = ist_pk.table_schema
    AND ist_pk_col.table_name = ist_pk.table_name
    AND ist_pk_col.column_name = COLUMNS.column_name
    AND ist_pk_col.constraint_name = ist_pk.constraint_name
  WHERE columns.column_name in ('ns') /* For tables with tps decoration (or ft tables) */
  AND columns.table_schema <> 'tps'
  ORDER BY ist_pk.constraint_type;
  
  


  
SELECT * FROM pg_indexes 
WHERE schemaname IN ('public', 'anx' )
AND NOT indexdef LIKE ('%UNIQUE%') 
AND split_part(indexdef, '(', 2 ) LIKE ANY (VALUES ('%entr_tp,%'),('%,entr_tp%'),('%der_tp)%'));


SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
  INNER JOIN INFORMATION_SCHEMA.TABLES 
ON (TABLES.table_schema, TABLES.table_name) = (columns.table_schema, columns.table_name)
AND TABLES.table_type = 'BASE TABLE'
AND COLUMNS.column_name = 'ns';



 SELECT TABLES.table_schema, TABLES.table_name, COLUMNS.column_name
   						FROM information_schema.columns /* Columns tpsuid of tables that are temporal (or ft tables)*/
 				 		INNER JOIN INFORMATION_SCHEMA.TABLES ON (TABLES.table_schema, TABLES.table_name) = (columns.table_schema, columns.table_name)
															 AND TABLES.table_type = 'BASE TABLE'
															 AND COLUMNS.column_name = 'ns'
															 AND TABLES.table_name LIKE COALESCE(NULLIF('', ''), '%')


select * from de;



select st,* from doc where doc_id = 773704;


update doc set st = 'VALID' where doc_id in (179239,454796, 182972, 182866);